//
//  ExpenseCell.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 12/05/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

class ExpenseCell: UITableViewCell {

    @IBOutlet weak var lbl_date_time : UILabel!
    @IBOutlet weak var lbl_expense_name : UILabel!
    @IBOutlet weak var lbl_cost : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
