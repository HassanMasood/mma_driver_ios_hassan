//
//  QueueStatusCell.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 08/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

class QueueStatusCell: UITableViewCell {

    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var driver_name : UILabel!
    @IBOutlet weak var waiting_since : UILabel!
    @IBOutlet weak var cons_clock_width : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
