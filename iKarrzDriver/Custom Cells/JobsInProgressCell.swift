//
//  JobsInProgressCell.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 25/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

class JobsInProgressCell: UITableViewCell {

    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var viewStatus : UIView!
    
    @IBOutlet weak var lblCustName : UILabel!
    @IBOutlet weak var lblJobId : UILabel!
    @IBOutlet weak var lblPassenger : UILabel!
    @IBOutlet weak var lblFare : UILabel!
    @IBOutlet weak var lblMobile : UILabel!
    @IBOutlet weak var lblJobStatus : UILabel!
    
    @IBOutlet weak var lblVia1 : UILabel!
    @IBOutlet weak var lblVia2 : UILabel!
    @IBOutlet weak var lblVia3 : UILabel!
    
    @IBOutlet weak var stackVia1 : UIStackView!
    @IBOutlet weak var stackVia2 : UIStackView!
    @IBOutlet weak var stackVia3 : UIStackView!
    
    @IBOutlet weak var lbl_pickup : UILabel!
    @IBOutlet weak var lbl_destination : UILabel!
    @IBOutlet weak var lbl_date : UILabel!
    //@IBOutlet weak var lbl_time : UILabel!
    @IBOutlet weak var lbl_payment : UILabel!
    @IBOutlet weak var stack_vehicle : UIStackView!
    @IBOutlet weak var lbl_VehicleType: UILabel!
    @IBOutlet weak var lbl_fare : UILabel!
    @IBOutlet weak var info_stack : UIStackView!
    @IBOutlet weak var stack_flight : UIStackView!
    @IBOutlet weak var lbl_flightNo : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
