//
//  PickUpCell.swift
//  TBMSDriver
//
//  Created by Bhupat Bhuva on 01/02/20.
//  Copyright © 2020 Piyush Agrawal. All rights reserved.
//

import UIKit

class PickUpCell: UITableViewCell {

    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var btnNav : CustomButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
