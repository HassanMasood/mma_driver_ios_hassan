//
//  CompletedJobsCell.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 19/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

class CompletedJobsCell: UITableViewCell {

    @IBOutlet weak var lbl_from : UILabel!
    @IBOutlet weak var lbl_to : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
