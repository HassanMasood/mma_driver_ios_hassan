//
//  BiddingCell.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 20/02/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

class BiddingCell: UITableViewCell {

    @IBOutlet weak var background_img : UIImageView!
    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var img_width : NSLayoutConstraint!
    @IBOutlet weak var const_Stack_Height: NSLayoutConstraint!
    @IBOutlet weak var lbl_id : UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lbl_date : UILabel!
    @IBOutlet weak var lbl_pickup : UILabel!
    @IBOutlet weak var lbl_destination : UILabel!
    @IBOutlet weak var lblFlightNo : UILabel!
    @IBOutlet weak var lblVehicleType : UILabel!
    @IBOutlet weak var imgFlight : UIImageView!
    @IBOutlet weak var imgVehicle : UIImageView!
    @IBOutlet weak var lblComment : UILabel!
    @IBOutlet weak var stackFlight : UIStackView!

    
    //@IBOutlet weak var lbl_min_bid_price : UILabel!
    //@IBOutlet weak var lbl_highest_bid : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        background_img.layer.shadowColor = UIColor.black.cgColor
//        background_img.layer.shadowOpacity = 1
//        background_img.layer.shadowOffset = CGSize.zero
//        background_img.layer.shadowRadius = 2
//        //background_img.layer.shadowPath = UIBezierPath(rect: background_img.bounds).cgPath
//        background_img.layer.shouldRasterize = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
