//
//  MessageCell.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 14/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var lblIncomingMessage : UILabel!
    @IBOutlet weak var lblOutgoingMessage : UILabel!
    
    @IBOutlet weak var viewIn : UIView!
    @IBOutlet weak var viewOut : UIView!
    
    @IBOutlet weak var imgView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblIncomingMessage.layer.cornerRadius = 10
        self.lblIncomingMessage.layer.masksToBounds = true
        
        self.lblOutgoingMessage.layer.cornerRadius = 10
        self.lblOutgoingMessage.layer.masksToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
