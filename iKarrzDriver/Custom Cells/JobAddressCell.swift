//
//  JobAddressCell.swift
//  TBMSDriver
//
//  Created by Bhupat Bhuva on 04/11/19.
//  Copyright © 2019 Piyush Agrawal. All rights reserved.
//

import UIKit

class JobAddressCell: UITableViewCell {
    
    @IBOutlet weak var stackView1 : UIStackView!
    @IBOutlet weak var stackView2 : UIStackView!
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblPassenger : UILabel!
    @IBOutlet weak var btnNav : CustomButton!
    @IBOutlet weak var btnMobile : UIButton!
    @IBOutlet weak var btnArrived : UIButton!
    @IBOutlet weak var btnPickedUp : UIButton!
    @IBOutlet weak var btnNoShow : UIButton!
    @IBOutlet weak var stackStatus : UIStackView!
    
    @IBOutlet weak var const_lblPass_Top : NSLayoutConstraint!
    @IBOutlet weak var const_lblPass_Bottom : NSLayoutConstraint!
    @IBOutlet weak var const_lblPass_Height : NSLayoutConstraint!
    
    @IBOutlet weak var const_Stack_Height : NSLayoutConstraint!
    @IBOutlet weak var const_btnMobile_Height : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnArrived.layer.cornerRadius = 5
        self.btnArrived.layer.borderWidth = 1
        self.btnArrived.layer.borderColor = UIColor.black.cgColor        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
