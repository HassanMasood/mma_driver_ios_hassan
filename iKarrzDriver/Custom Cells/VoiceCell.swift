//
//  VoiceCell.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 16/03/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

class VoiceCell: UITableViewCell {
    
    @IBOutlet weak var imag : UIImageView!
    @IBOutlet weak var lbl_time : UILabel!
    @IBOutlet weak var img_play : UIImageView!
    
    @IBOutlet weak var cons_lead_imag : NSLayoutConstraint!
    @IBOutlet weak var cons_trail_imag : NSLayoutConstraint!
    
    @IBOutlet weak var cons_lead_content : NSLayoutConstraint!
    @IBOutlet weak var cons_trail_content : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
