//
//  CommonVC.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 08/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit



import ChameleonFramework

class CommonVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         //self.view.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: [UIColor(hexString: "F2C94C")!,UIColor(hexString: "F2994A")!])
        //self.view.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: [UIColor(hexString: "F2C94C")!,UIColor(hexString: "FFD200")!,UIColor(hexString: "F2994A")!])
        //self.view.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: [UIColor(hexString: "F2C94C")!,UIColor(hexString: "FFD200")!,UIColor(hexString: "FFB75E")!])
        //self.view.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: [UIColor(hexString: "ffb347")!,UIColor(hexString: "F2994A")!])
        if self.isNeedToChangeBack() {
            self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }

//        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "ffb347", withAlpha: 0.5)
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "8CC641")

        self.navigationController?.navigationBar.tintColor = UIColor.white

        
//        Green 8CC641
//        Blue  2EACE2
    }

    //----------------------------------------
    
    func setupButtonUI(for button: UIButton) {
        
        button.layer.cornerRadius = button.frame.size.height / 2
        
        //        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowColor = button.backgroundColor?.cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
    }
    
    //----------------------------------------
    
    func setupView(viewT: UIView) {
        viewT.layer.cornerRadius = 20
        viewT.layer.borderColor = UIColor.black.cgColor
//          viewT.layer.borderColor = UIColor.green.cgColor
        viewT.layer.borderWidth = 1.5
    }
    
    //----------------------------------------
    
    func setupUI(for viewContent: UIView) {
        
        viewContent.layer.cornerRadius = viewContent.frame.size.height / 2
        
        //        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        viewContent.layer.shadowColor = viewContent.backgroundColor?.cgColor
        viewContent.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        viewContent.layer.shadowOpacity = 0.5
        viewContent.layer.shadowRadius = 0.0
        viewContent.layer.masksToBounds = false
    }
    
    //----------------------------------------
    
    func isNeedToChangeBack()-> Bool {
        if self.isKind(of: DashboardVC.self) || self.isKind(of: AllActiveJobsVC.self) || self.isKind(of: CompletedJobsVC.self) || self.isKind(of: BiddingListVC.self) {
            return false
        }
        return true
    }
    
    //----------------------------------------
    
    func navtoWaze(location: String) {
        
        let url = URL(string: "waze://?q=\(location)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let wurl = url , UIApplication.shared.canOpenURL(wurl){
            UIApplication.shared.open(wurl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
        else if let itunesUrl = URL(string: "http://itunes.apple.com/us/app/id323229106") , UIApplication.shared.canOpenURL(itunesUrl) {
            UIApplication.shared.open(itunesUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
    }
    
    //----------------------------------------
    
    func navtoGoogleMaps(location: String) {
        
        let url = URL(string: "comgooglemaps://?q=\(location)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let wurl = url , UIApplication.shared.canOpenURL(wurl){
            UIApplication.shared.open(wurl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
        else if let itunesUrl = URL(string: "http://itunes.apple.com/us/app/id585027354") , UIApplication.shared.canOpenURL(itunesUrl) {
            UIApplication.shared.open(itunesUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
    }
    
    //----------------------------------------
    
    func getJobStatusColor(jobDetails: [String:AnyObject]) -> UIColor {
        
        let dispatch = jobDetails["dispatch"] as? Int ?? 0
        
        switch dispatch {
            
        case 5:
            
            if jobDetails["job_status"] as! String == "New"{ //Accept
                return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            } else { //ON Route
                return #colorLiteral(red: 1, green: 0.4705882353, blue: 0, alpha: 1)
            }
            
        case 4: //Arrived
                        
            return #colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1)
            
        case 0: //POB
            
            return #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)

        case 2: //Complete
           
            return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            
        default:
            return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    //----------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

     func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
}
