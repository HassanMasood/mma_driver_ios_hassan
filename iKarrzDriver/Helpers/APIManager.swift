//
//  APIManager.swift
//  AlfredMann
//
//  Created by Piyush on 03/04/17.
//  Copyright © 2017 TBMS. All rights reserved.
//

import UIKit

import Alamofire

class APIManager: NSObject {

    static let sharedInstance = APIManager()
    
    
  static  let serverTrustPolicies: [String: ServerTrustPolicy] = [
        "tbmslive.com": .disableEvaluation
    ]
    
    let sessionManager = SessionManager(
        serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
    )
}
