//
//  Constant.swift
//  AlfredMann
//
//  Created by Piyush on 31/03/17.
//  Copyright © 2017 TBMS. All rights reserved.
//

import Foundation

import UIKit


//var kGoogleAPIKey = "AIzaSyAp3b4oj9Nt8g2cl0Rfj_nNGnmN7SLzb9o"
var kGoogleAPIKey = ""

//piyush08apple@gmail.com


let appDelegate = UIApplication.shared.delegate as! AppDelegate

let AppName = "MMA Transfers Driver"
let OfficeName = "mma"
//let OfficeName = ""
let EmailID = "info@mmatransfers.com"
let PhoneCall = "+44 7828 926870"

let MaxAudioLength = 60 //voice chat recording is only allowed for 60 seconds
let documentsURL = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString

let locationDeniedMessage = "MMA Transfers Passenger use your location to assign job from current location, you denied the location access so please change the location access permission from the settings."

let kReachedViaData = "reachedViaData"

let kJobIdsData = "jobIdsString"

let trackingDuration = 20

let kPickArrivedVia = "_pickArrivedVia"
let kArrived = "arrived"
let kPickNoShow = "pickNoShow"
