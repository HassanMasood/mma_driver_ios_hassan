//
//  ResetPasswordVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 06/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import SCLAlertView
import Alamofire
import PKHUD


class ResetPasswordVC: CommonVC, UITextFieldDelegate {

    @IBOutlet weak var txt_office_name:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_old_password: UITextField!
    @IBOutlet weak var txt_new_password: UITextField!
    @IBOutlet weak var txt_confirm_password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Validation
    
    func Validation() -> (msg:String,isvalid:Bool){
        
        if txt_office_name.text!.isEmpty{
            return ("Office name must not be empty.",false)
        }
        if txt_email.text!.isEmpty{
            return ("User name must not be empty.",false)
        }
        if txt_old_password.text!.isEmpty{
            return ("Current Password must not be empty.",false)
        }
        if txt_new_password.text!.isEmpty{
            return ("Password must not be empty.",false)
        }
        if txt_confirm_password.text! != txt_new_password.text!{
            return ("Confirm Password does not be match with new password.",false)
        }
        
        return("Valid",true)
    }
    
    //MARK:- Change Password Button
    @IBAction func changePassword(){
        
        
        self.view.endEditing(true)
        
        let check = Validation()
        
        if check.isvalid{
            
            if appDelegate.reachability.connection == .unavailable{
                SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
                return
            }
            
            HUD.show(.progress)
            
            let parameter  = ["office_name":txt_office_name.text!,"type":"driver_changepass","username": txt_email.text!,"oldpassword": txt_old_password.text!,"newpassword": txt_new_password.text!]
            // print(parameter)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                switch response.result {
                case .success:
                    //debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["DATA"]?["msg"] as! String == "Your password change successfully"{
                        
                        if dict["DATA"] !=  nil{
                        }
                        DispatchQueue.main.async(execute: {
                            
                            SCLAlertView().showSuccess(AppName, subTitle: "Your password change successfully")
                            self.dismiss(animated: true, completion: { 
                                
                            })
                        })
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                        })
                    }
                    
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                    
                }
            })
        }
        else{
            SCLAlertView().showError(AppName, subTitle: check.msg)
        }
    }

    //MARK: - Cancel Press
    @IBAction func cancelPress(){
        self.dismiss(animated: true) { 
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
