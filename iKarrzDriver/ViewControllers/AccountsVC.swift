//
//  AccountsVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 14/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import Alamofire
import PKHUD
import SCLAlertView

class AccountsVC: UIViewController, UITextFieldDelegate {
    
   // @IBOutlet weak var txt_from : CustomTextField!
  //  @IBOutlet weak var txt_to : CustomTextField!
    
  //  var selectedTextField : UITextField?
    
    @IBOutlet weak var lbl_TopUps:UILabel!
    @IBOutlet weak var lbl_AC:UILabel!
    @IBOutlet weak var lbl_Cash:UILabel!
    @IBOutlet weak var lbl_Withdrawal:UILabel!
    @IBOutlet weak var lbl_credit1:UILabel!
    @IBOutlet weak var lbl_credit2:UILabel!
    
  //  @IBOutlet weak var view_date:UIView!
  //  @IBOutlet weak var pic_date:UIDatePicker!
    
    var accountDetails = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.getData()
        
        
    }
    
    //MARK:- SetupData
    
    func  SetupData() {
        
        if let ac = accountDetails["Total Account Payable To Driver"]{
            lbl_AC.text = "£ " + String(describing: ac)
        }
        if let cash = accountDetails["Total Cash Payable By Driver"]{
            lbl_Cash.text = "£ " + String(describing: cash)
        }
        if let credit = accountDetails["Total Credit"]{
            lbl_credit1.text = "£ " + String(describing: credit)
            lbl_credit2.text = "AVAILABLE CREDIT:" + "    £ " + String(describing: credit)
        }
        if let withdraw = accountDetails["Total Widthrawl"]{
            lbl_Withdrawal.text = "£ " + String(describing: withdraw)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:-  Get Data
    
    func getData(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"rent_details_of_drivers", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
           // debugPrint(response.request?.url ?? "")
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                self.accountDetails  = dict["DATA"] as! [String:AnyObject]
                if dict["RESULT"] as! String ==  "OK"{
                    
                    
                    DispatchQueue.main.async(execute: {
                       self.SetupData()
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
 
        })
        
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
