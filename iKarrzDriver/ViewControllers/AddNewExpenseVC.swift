//
//  AddNewExpenseVC.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 15/05/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

import Alamofire
import PKHUD
import SCLAlertView
import IQKeyboardManagerSwift
import AVFoundation

class AddNewExpenseVC: CommonVC , UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var txt_expense_name : UITextField!
    @IBOutlet weak var txt_expense_amount : UITextField!
    @IBOutlet weak var txt_date : UITextField!
    @IBOutlet weak var txt_time : UITextField!
    @IBOutlet weak var txt_notes : IQTextView!
    
    @IBOutlet weak var img_attachment : UIImageView!
    
    @IBOutlet weak var datePicker : UIDatePicker!
    @IBOutlet weak var view_datePicker : UIView!
    
    var selectedTextField = UITextField()
    
    var imgpicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupUI(){
        
        datePicker.maximumDate = Date()
        //txt_date.text = NSDate.toString(date: NSDate() , format: "dd MMM yyyy")
        //txt_time.text = NSDate.toString(date: NSDate() , format: "HH:mm")
        txt_date.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        txt_time.text = Date().stringWithFormatter(dateFormat: "HH:mm")
    }
    
    
    //MARK:-  TextField Delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == txt_date) {
            let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.view.endEditing(true)
            }
            selectedTextField = txt_date
            
            datePicker.datePickerMode = .date
            
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.view_datePicker.alpha = 1.0
                self.view_datePicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
            
        else if (textField == txt_time) {
            let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.view.endEditing(true)
            }
            selectedTextField = txt_time
            
            datePicker.datePickerMode = .time
            
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.view_datePicker.alpha = 1.0
                self.view_datePicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
        return false
    }
    
    
    //MARK:-  Date Picker
    
    @IBAction func Selected_datePicker(sender:UIDatePicker!){
        
        if selectedTextField == txt_date{
            let date = datePicker.date
            // txt_date.text = NSDate.toString(date: date as NSDate, format: "dd MMM yyyy")
            txt_date.text = date.stringWithFormatter(dateFormat: "dd MMM yyyy")
        }
        else if selectedTextField == txt_time{
            let date = datePicker.date
            //txt_time.text = NSDate.toString(date: date as NSDate, format: "HH:mm")
            txt_time.text = date.stringWithFormatter(dateFormat: "HH:mm")
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    @IBAction func closeDatePicker(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    //MARK:- Add image
    
    @IBAction func add_image(sender:UIButton!){
        
        let alert = UIAlertController(title: AppName, message: "Choose image source", preferredStyle: .actionSheet)
        
        let camera_action = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        
        let gallery_action = UIAlertAction(title: "Photos", style: .default) { (action) in
            self.openGallery()
        }
        
        let cancel_action = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(camera_action)
        alert.addAction(gallery_action)
        alert.addAction(cancel_action)
        
        self.present(alert, animated: true) {
            
        }
        
    }
    
    func openCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(.camera))
        {
            let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
            
            switch authStatus{
                
            case .authorized,.notDetermined:
                imgpicker.sourceType = .camera
                imgpicker.delegate = self
                self .present(imgpicker, animated: true, completion: nil)
                
            case .denied:
                
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                
                let alert = SCLAlertView(appearance: appearance)
                alert.addButton("Settings", action: {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                })
                alert.addButton("Cancel", action: {
                    
                })
                alert.showError(AppName, subTitle: "Camera access did not authorize. Please enable Camera access to capture photo.")
                
            default:
                appDelegate.showerror(str: "Can not open camera")
            }
            
            
        }else{
            self.openGallery()
        }
    }
    
    func openGallery()
    {
        imgpicker.sourceType = .photoLibrary
        imgpicker.delegate = self
        self.present(imgpicker, animated: true, completion: nil)
    }
    
    //MARK:- ImagePicker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        imgpicker.dismiss(animated: true, completion: nil)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            img_attachment.image = image
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        debugPrint("picker cancel.")
        imgpicker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Make Booking
    
    func checkValidation() -> (isValid:Bool, msg:String){
        
        if txt_expense_name.text!.isEmpty{
            return (false, "Expense title should not be empty")
        }
        else if txt_expense_amount.text!.isEmpty{
            return (false, "expense amount should not be empty")
        }
        
        else if txt_date.text!.isEmpty{
            return (false, "Journey date should not be empty")
        }
        else if txt_time.text!.isEmpty{
            return (false, "Journey time should not be empty")
        }

        
        return (true, "Valid")
    }
    
    
    @IBAction func Save_Expense(){
        
        let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.view.endEditing(true)
        }
        let check = checkValidation()
        
        if check.isValid{
            
            if appDelegate.reachability.connection == .unavailable {
                SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
                return
            }
            
            HUD.show(.progress)
            
            let data =  img_attachment.image!.jpegData(compressionQuality: 0)
            let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
            let timeInterval = Date().timeIntervalSince1970
            
            let param = ["office_name":user_dict["office_name"]!,"driver_id": user_dict["driver_id"]!,"type":"create_driver_expense","expense_name":txt_expense_name.text!,"expense_amount":txt_expense_amount.text!,"expense_date":txt_date.text!,"expense_time":txt_time.text!,"expense_note":txt_notes.text!] as [String : AnyObject]
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in param {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                multipartFormData.append(data!, withName: "expense_attachment", fileName: "\(timeInterval).jpg", mimeType: "image/jpeg")
                debugPrint(multipartFormData)
            }, usingThreshold:  UInt64.init(), to: BASE_URL, method: .post, headers: nil) { (result) in
                
                //HUD.hide()
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseString(completionHandler: { (response) in
                        debugPrint(response)
                    })
                    
                    upload.responseJSON { response in
                        debugPrint(response)
                        if let dict = response.result.value as? [String:AnyObject]{
                            if let msg = dict["msg"] as? String,msg == "Expense saved successfully"{
                                HUD.hide()
                                SCLAlertView().showSuccess(AppName, subTitle: msg)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil)
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                HUD.hide()
                                appDelegate.showerror(str: dict["msg"] as? String ?? "server error")
                            }
                        }
                    }
                case .failure(let encodingError):
                    debugPrint(encodingError)
                    appDelegate.showerror(str: encodingError.localizedDescription)
                    HUD.hide()
                }
            }
            
        }
        else{
            SCLAlertView().showError(AppName, subTitle: check.msg)
        }
    }
    
    @IBAction func exit_press(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
