//
//  AutocompletePlacesVC.swift
//  Uphoria
//
//  Created by Kishor Lodhia on 13/06/17.
//  Copyright © 2017 TBMS. All rights reserved.
//

import UIKit

import GooglePlaces

//MARK:- Selected Place Delegate
protocol selected_PlaceDeleagte:class {
    
    func selectedPlace(address:String!)
}

class AutocompletePlacesVC: CommonVC,UISearchControllerDelegate {

    var searchText = ""
    
    @IBOutlet weak var searchView : UIView!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    
    weak var SelectedPlacedelegate : selected_PlaceDeleagte?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    func setupUI(){
        
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "ffb347", withAlpha: 0.5)
        
        // This makes the view area include the nav bar even though it is opaque.
        // Adjust the view placement down.
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .top
        
        let filter = GMSAutocompleteFilter()
        //filter.type = .region
        //filter.country = "UK"
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        resultsViewController?.autocompleteFilter = filter
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        searchView.addSubview((searchController?.searchBar)!)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false

        searchController?.searchBar.barTintColor = UIColor(hexString: "ffb347", withAlpha: 0.5)
        searchController?.searchBar.tintColor = UIColor.darkText
        searchController?.delegate = self
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        resultsViewController?.tableCellBackgroundColor = UIColor(white: 1, alpha: 0.5)
        resultsViewController?.tableCellSeparatorColor = UIColor.darkText
        
        
        //searchController?.isActive = true
        searchController?.searchBar.text = searchText
        //searchController?.searchBar.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchController?.searchBar.sizeToFit()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //searchController?.searchBar.sizeToFit()
        searchController?.isActive = true
        DispatchQueue.main.async {
        self.searchController?.searchBar.becomeFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelPress(sender:UIBarButtonItem){
        searchController?.isActive = false
        self.dismiss(animated: true) { 
            
        }
    }
    
    @IBAction func donePress(sender:UIBarButtonItem){
        
        print(searchController?.searchBar.text ?? "")
        let address = searchController?.searchBar.text ?? ""
        SelectedPlacedelegate?.selectedPlace(address: address)
        
        searchController?.isActive = false
        self.dismiss(animated: true) {
            
        }
    }

//    func presentSearchController(_ searchController: UISearchController) {
//        searchController.searchBar.becomeFirstResponder()
//    }
    
    func didPresentSearchController(searchController: UISearchController) {
        
        searchController.searchBar.becomeFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




// MARK:- Handle the user's selection.
extension AutocompletePlacesVC: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        
        let address = place.formattedAddress ?? ""
        SelectedPlacedelegate?.selectedPlace(address: address)
        
        self.dismiss(animated: true) {
            
        }
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
