//
//  DashboardVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 05/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import CoreLocation
import Cosmos
import SCLAlertView
import Alamofire
import PKHUD
import Kingfisher
import APScheduledLocationManager

import Messages
import MessageUI

class DashboardVC: CommonVC, CLLocationManagerDelegate,APScheduledLocationManagerDelegate,MFMessageComposeViewControllerDelegate {

    @IBOutlet weak var btn_newJobs : UIButton!
    @IBOutlet weak var btn_acceptedJobs : UIButton!
    @IBOutlet weak var btn_add_bookings : UIButton!
    @IBOutlet weak var btn_break : UIButton!
    @IBOutlet weak var lbl_driverName: UILabel!
    @IBOutlet weak var lblVehicle: UILabel!
    @IBOutlet weak var lblEarnDate: UILabel!
    @IBOutlet weak var lblCash: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    
    @IBOutlet weak var lblDriverId: UILabel!
    @IBOutlet weak var lbl_driverLocation: UILabel!

    @IBOutlet weak var imgViewCall: UIImageView!
    @IBOutlet weak var imgViewShare: UIImageView!

    @IBOutlet weak var imgViewHistory: UIImageView!
    @IBOutlet weak var imgViewMessages: UIImageView!
    @IBOutlet weak var imgViewBooking: UIImageView!
    
    @IBOutlet weak var viewRatings: CosmosView!
    
    @IBOutlet weak var viewBookings: UIView!
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var viewMessages: UIView!
    @IBOutlet weak var viewBiddings: UIView!

    @IBOutlet weak var btnOfficeMessage : UIButton!
    @IBOutlet weak var btnCompletedJobs : UIButton!
    @IBOutlet weak var btnCallOffice : UIButton!
    @IBOutlet weak var btnBiddingJogs : UIButton!
    @IBOutlet weak var btnQueueStatus : UIButton!
    @IBOutlet weak var btnEnmergency : UIButton!
    @IBOutlet weak var btnMyExpense : UIButton!
    @IBOutlet weak var btnLogout : UIButton!
    
    var switchBreak: UISwitch!
    let locationManager = CLLocationManager()
    private var manager: APScheduledLocationManager!
    private var add_manager: APScheduledLocationManager!
    
    
    func initialSetup() {
        self.addLeftLogoutButton()
//        self.setupUI(for: self.viewNewBookings)
//        self.setupUI(for: viewAccepted)
        self.setupButtonUI(for: btnOfficeMessage)
        self.setupButtonUI(for: btnCompletedJobs)
//        self.setupButtonUI(for: self.btn_add_bookings)
        self.setupButtonUI(for: self.btn_break)
        self.setupButtonUI(for: btnCallOffice)
        self.setupButtonUI(for: btnQueueStatus)
        self.setupButtonUI(for: btnEnmergency)
        self.setupButtonUI(for: btnMyExpense)

        self.setupButtonUI(for: btnBiddingJogs)
        self.setupButtonUI(for: btnLogout)
                
        self.imgViewHistory.image = self.imgViewHistory.image?.withRenderingMode(.alwaysTemplate)
        self.imgViewHistory.tintColor = .black
        
        self.imgViewMessages.image = self.imgViewMessages.image?.withRenderingMode(.alwaysTemplate)
        self.imgViewMessages.tintColor = .red
        
        self.imgViewBooking.image = self.imgViewBooking.image?.withRenderingMode(.alwaysTemplate)
        self.imgViewBooking.tintColor = UIColor(hexString: "2EACE2")
        
        self.imgViewShare.image = self.imgViewShare.image?.withRenderingMode(.alwaysTemplate)
        self.imgViewShare.tintColor = .red
        
        self.imgViewCall.image = self.imgViewCall.image?.withRenderingMode(.alwaysTemplate)
        self.imgViewCall.tintColor = UIColor(hexString: "2EACE2")

    }
    
    //----------------------------------------
    
    func setEarningsData(data: [String: Any]) {
        self.lblEarnDate.text = "Date\n\(self.getTodaysDate())"
        var strCash = "0"
        if let totalCash = data["total_cash"] as? String {
            strCash = totalCash
        }else if let totalCash = data["total_cash"] as? Int {
            strCash = String(totalCash)
        }
        
        var strAccount = "0"
        if let account = data["total_account"] as? String {
            strAccount = account
        }else if let account = data["total_account"] as? Int {
            strAccount = String(account)
        }
        
        self.lblCash.text = strCash
        self.lblAccount.text = strAccount
    }
    
    //----------------------------------------
    
    func getTodaysDate()-> String {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "dd MMM yyyy"
        let str = df.string(from: date)
        return str
    }
    
    //----------------------------------------
    
    func addLeftLogoutButton() {
        
//        let leftButtonView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 50))
//
//        let leftButton = UIButton.init(type: .system)
//        leftButton.backgroundColor = .clear
//        leftButton.frame = CGRect(x: 50, y: 0, width: 40, height: 40)
//        leftButton.setImage(#imageLiteral(resourceName: "arrow_location"), for: .normal)
//        leftButton.tintColor = .black
//        leftButton.autoresizesSubviews = true
//        leftButton.autoresizingMask = [.flexibleWidth , .flexibleHeight]
//        leftButton.addTarget(self, action: #selector(sendManualLocation), for: .touchUpInside)
//        leftButtonView.addSubview(leftButton)
//
//        let lbl = UILabel(frame: CGRect(x: 0, y: 25, width: 90, height: 20))
//        lbl.textColor = .black
//        lbl.font = UIFont.systemFont(ofSize: 13)
//        lbl.text = "share location"
//        lbl.textAlignment = .center
//        leftButtonView.addSubview(lbl)
//
//        let leftBarButton = UIBarButtonItem.init(customView: leftButtonView)
//        navigationItem.rightBarButtonItem = leftBarButton
        
        let leftButtonView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 50))
        
        let leftButton = UIButton.init(type: .system)
        leftButton.backgroundColor = .clear
        leftButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        leftButton.setImage(#imageLiteral(resourceName: "logout"), for: .normal)
        leftButton.tintColor = .white
        leftButton.autoresizesSubviews = true
        leftButton.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        leftButton.addTarget(self, action: #selector(logout), for: .touchUpInside)
        leftButtonView.addSubview(leftButton)
                
        let leftBarButton = UIBarButtonItem.init(customView: leftButtonView)
        navigationItem.leftBarButtonItem = leftBarButton
        
        switchBreak = UISwitch()
        switchBreak.isOn = true
        
        switchBreak.onTintColor = UIColor.green
        switchBreak.tintColor = UIColor.red
        switchBreak.thumbTintColor = UIColor.white
        switchBreak.backgroundColor = UIColor.red
        switchBreak.layer.cornerRadius = 16
        
        switchBreak.setOn(true, animated: false)
        self.navigationItem.title = switchBreak.isOn ? "On Duty" : "Off Duty"
        switchBreak.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
        let rightBarButton = UIBarButtonItem(customView: switchBreak)
        navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    @objc func switchValueDidChange(sender:UISwitch!) {
        
        self.navigationItem.title = switchBreak.isOn ? "On Duty" : "Off Duty"
        
        self.callWSToChangDriverDutyStatus(isBreak: !switchBreak.isOn)
    }
    
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        // Do any additional setup after loading the view.
        self.setUpData()
        
        self.findMyLocation()
        
        self.getDriverStatus()
        
        manager = APScheduledLocationManager(delegate: self)
        add_manager = APScheduledLocationManager(delegate: self)
         if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            manager.startUpdatingLocation(interval: 45, acceptableLocationAccuracy: 100)
            add_manager.startUpdatingLocation(interval: 150, acceptableLocationAccuracy: 100)
        }
    }
    
    //----------------------------------------
    
    func setRoundedCorner(toView: UIView) {
        toView.layer.cornerRadius = 10
        toView.layer.masksToBounds = true
    }
    
    //----------------------------------------

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //----------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callWSToGetCurrentEarningsData()
    }
    
    //----------------------------------------
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setRoundedCorner(toView: self.viewBookings)
        self.setRoundedCorner(toView: self.viewHistory)
        self.setRoundedCorner(toView: self.viewMessages)
        self.setRoundedCorner(toView: self.viewBiddings)
    }
    
    //MARK:- Setup Data
    func setUpData()  {
        
         let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
//        lbl_driverName.text = (user_dict["user_name"] as! String).capitalized
        
        var strDriverData:String = "Driver : "
        
        if let name = user_dict["name"] as? String {
            strDriverData = strDriverData + name            
            lbl_driverName.text = strDriverData
        }
        
//        if let vehicleDetails = user_dict["vehicle_type"] as? String {
//            self.lblVehicle.text = "Vehicle : " + vehicleDetails
//        }
        

//        if let driverId = user_dict["loginid"] as? String {
//            let dId = "Driver ID : \(driverId)"
//            self.lblDriverId.text = dId
//        }
        
        if let driverRating = user_dict["driver_rating"] as? Double {
            viewRatings.settings.fillMode = .precise
            let rating = Double(round(10*driverRating)/10)

            viewRatings.rating = rating
            viewRatings.text = "(\(CGFloat(rating))) "
        }

        if let allow_booking = user_dict["allow_booking"] as? String, allow_booking.lowercased() == "no"{
            btn_add_bookings.backgroundColor = UIColor.lightGray
        }
    }
    
    
    //MARK:- Locate Me
     func findMyLocation() {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if CLLocationManager.authorizationStatus() == .notDetermined{
        //self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
        else{
            //self.locationManager.requestWhenInUseAuthorization()
            //self.locationManager.requestAlwaysAuthorization()

        }
    }
    
    //MARK:- Current Lcoation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        /*
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            
            if (error != nil) {
                //print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                //SCLAlertView().showError(AppName, subTitle: (error?.localizedDescription)!, duration: 4)
                return
            }
            
            if placemarks?.count != 0 {
                let pm = placemarks?[0]
                self.displayLocationInfo(placemark: pm!)
            } else {
                appDelegate.showerror(str: "Problem with the data received from geocoder")
            }
        }
 */
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        
        //locationManager.stopUpdatingLocation()
        //print(placemark.locality ?? "")
        //print(placemark.postalCode ?? "")
        //print(placemark.administrativeArea ?? "")
        //print(placemark.country ?? "")
        
        print("display Address")
        
        var address = ""
        
        if let name = placemark.name, !name.isEmpty{
            address = name
        }
        
        if let thoroughfare = placemark.thoroughfare, !thoroughfare.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + thoroughfare
        }
        
//        if let subThoroughfare = placemark.subThoroughfare, !subThoroughfare.isEmpty{
//            if !address.isEmpty{
//                address = address + ","
//            }
//            address = address + " " + subThoroughfare
//        }
        
        if let locality = placemark.locality, !locality.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + locality
        }
        
//        if let subLocality = placemark.subLocality, !subLocality.isEmpty{
//            if !address.isEmpty{
//                address = address + ","
//            }
//            address = address + " " + subLocality
//        }
        
        if let administrativeArea = placemark.administrativeArea, !administrativeArea.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + administrativeArea
        }
        
//        if let subAdministrativeArea = placemark.subAdministrativeArea, !subAdministrativeArea.isEmpty{
//            if !address.isEmpty{
//                address = address + ","
//            }
//            address = address + " " + subAdministrativeArea
//        }
        
        if let postalCode = placemark.postalCode, !postalCode.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + postalCode
        }
        
        if let country = placemark.country, !country.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + country
        }
        
        lbl_driverLocation.text = address
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //appDelegate.showerror(str: error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
            self.manager.startUpdatingLocation(interval: 45, acceptableLocationAccuracy: 100)
            add_manager.startUpdatingLocation(interval: 150, acceptableLocationAccuracy: 100)
        }
    }
    
    //MARK:- APScheduleLocation
    
    func scheduledLocationManager(_ manager: APScheduledLocationManager, didUpdateLocations locations: [CLLocation]) {

        if manager == add_manager{
            CLGeocoder().reverseGeocodeLocation(locations.first!) { (placemarks, error) in
                
                if (error != nil) {
                    //print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    //SCLAlertView().showError(AppName, subTitle: (error?.localizedDescription)!, duration: 4)
                    return
                }
                
                if placemarks?.count != 0 {
                    let pm = placemarks?[0]
                    self.displayLocationInfo(placemark: pm!)
                    //self.add_manager.stopUpdatingLocation()
                } else {
                    //appDelegate.showerror(str: "Problem with the data received from geocoder")
                }
            }
        }else{
            let l = locations.first!
            self.sendLocationToServer(loc: l)
        }
        
    }
    
    func scheduledLocationManager(_ manager: APScheduledLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
    
    func scheduledLocationManager(_ manager: APScheduledLocationManager, didFailWithError error: Error) {
        //appDelegate.showerror(str: error.localizedDescription)
    }
    
    func sendLocationToServer(loc:CLLocation!){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        let loggedIn = UserDefaults.standard.bool(forKey: "isDriverLoggedIn")
        if !loggedIn{
            return
        }
        
        if self.btn_break.titleLabel?.text == "FINISH BREAK"{
            return
        }
        
        let dateStr = Date().stringWithFormatter(dateFormat: "dd MMM yyyy HH:mm")
        //let dateStr = NSDate.toString(date: NSDate(), format: "dd MMM yyyy HH:mm:ss a")
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        var parameter  = ["type":"driver_location", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"latitude":"\(loc.coordinate.latitude)","longitude":"\(loc.coordinate.longitude)","date_time":dateStr] as [String : Any]
       // print(parameter)
        
        if loc.speed >= 0{
            let speed = String(format: "%.2f", loc.speed * 2.237) // convert m/s to mi/hr
            parameter["speed"] = speed
        }else{
            parameter["speed"] = "0.00"
        }
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                let dict2 = dict["DATA"] as! [String:AnyObject]
                if dict2["msg"] as! String ==  "Driver location inserted"{
                    print("Location Inserted")
                }
                else{
                    DispatchQueue.main.async(execute: {
                        // SCLAlertView().showError(AppName, subTitle: arr[0]["msg"] as? String ?? "Error", duration: 4)
                    })
                }
                
            case .failure(let error):
                print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        
    }

    //MARK:- Bookings
    @IBAction func gotoBookigns(sender:UIButton!) {
        self.performSegue(withIdentifier: "GotoBookings", sender: sender)
    }
    
    //----------------------------------------
    
    @IBAction func btnBiddingJobsTapped(sender:UIButton) {
        
        let biddingJobsVC = UIStoryboard.init(name: "Second", bundle: nil).instantiateViewController(withIdentifier: "biddingNavVC") as! UINavigationController
        self.present(biddingJobsVC, animated: true, completion: nil)
    }
    
    //----------------------------------------
    
    @IBAction func btnMessagesTapped(_ sender:UIButton) {
        
        let messagesVC = MessagesVC.viewController()
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
    
    //MARK: - Emergency PanicAlert Button
    
    @IBAction func PanicAlert(sender:UIButton!){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else { return }
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"panic_alert","username":user_dict["user_name"]!,"driver_id": user_dict["driver_id"]!] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                
                guard let dict = response.result.value as? [String:AnyObject] else { return }
                
                    DispatchQueue.main.async(execute: {
                        
                        guard let arr = dict["DATA"] as? [[String:AnyObject]], arr.count > 0 else { return }
                        
                        let msg = arr[0]["msg"] as? String ?? ""
                        
                        SCLAlertView().showSuccess(AppName, subTitle: msg)
                    })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
    }

    //MARK:- Call Office

    //----------------------------------------
    
    @IBAction func btnShareLocationTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: AppName, message: "Share location", preferredStyle: .actionSheet)
        
        let sms_action = UIAlertAction(title: "Via SMS", style: .default) { (action) in
            self.getSMSLink()
        }
        
        let system_action = UIAlertAction(title: "Via System", style: .default) { (action) in
            if let loc = self.locationManager.location{
                self.sendManualLocationToServer(loc: loc)
            }else{
                SCLAlertView().showError(AppName, subTitle: "No Location Found!")
            }
        }
        
        let cancel_action = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(sms_action)
        alert.addAction(system_action)
        alert.addAction(cancel_action)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnEmergencyTapped(_ sender: UIButton) {
        
    }
    
    //----------------------------------------
    
    @IBAction func CallOffice(sender:UIButton!){
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        if let  phonenumber = user_dict["phonenumber"] as? String{
            let arr_num = phonenumber.components(separatedBy: ",")
            
            if arr_num.isEmpty{
                appDelegate.showerror(str: "Contact info is missing")
            }else{
                if let url = URL(string: "telprompt://" + arr_num[0]){
                
                    if UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
                }
                else{
                    appDelegate.showerror(str: "Phone call is not available.")
                }
            }
        }
        
        
    }
    
    //----------------------------------------
    
    //MARK:- Driver Status
    func getDriverStatus() {
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else { return }
        
        let parameter  = ["type":"driver_current_status", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()

            switch response.result {
                
            case .success:

                guard let dict = response.result.value as? [String:AnyObject] else { return }
                
                guard let arr = dict["DATA"] as? [[String:AnyObject]] else { return }
                
                if !arr.isEmpty {
                    
                    if let status = arr[0]["driver_status"] as? String{
                        DispatchQueue.main.async(execute: {
                            
                            if status == "Vacant"{
                                self.btn_break.backgroundColor = #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1)
                                self.btn_break.setTitle("TAKE A BREAK", for: .normal)
                                self.switchBreak.setOn(true, animated: false)
                            }else{
                                self.btn_break.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                                self.btn_break.setTitle("FINISH BREAK", for: .normal)
                                self.switchBreak.setOn(false, animated: false)
                            }
                            self.navigationItem.title = self.switchBreak.isOn ? "On Duty" : "Off Duty"
//                            self.navigationItem.labl
                        })
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: arr[0]["msg"] as? String ?? "Failed to get Driver Status")
                        })
                    }
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
    }

    //----------------------------------------
    
    //MARk:- Add Bookings
    @IBAction func add_bookings() {
        
        if let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] {
            
            if var allow_booking = user_dict["allow_booking"] as? String {
                
                allow_booking = allow_booking.lowercased()
                
                if allow_booking == "yes" || allow_booking == "yesother" || allow_booking == "yesreassign" {
                    
                    self.performSegue(withIdentifier: "Add Booking", sender: self)
                    
                }else{
                    
                    appDelegate.showerror(str: "You are not authorized to add bookings.")
                    
                }
                
            }else{
                appDelegate.showerror(str: "You are not authorized to add bookings.")
            }
            
        }else{
            appDelegate.showerror(str: "You are not authorized to add bookings.")
        }
    }
    
    //----------------------------------------
    
    func callWSToChangDriverDutyStatus(isBreak: Bool) {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        var driverStatus = ""
        
        if isBreak {
            driverStatus = "onbreak"
        }else{
            driverStatus = "vacant"
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] ?? [:]
        
        let parameter  = ["office_name":user_dict["office_name"]!,
                          "type":"driver_status_change",
                          "driver_id": user_dict["driver_id"]!,
                          "driver_status":driverStatus ] as [String : Any]
        
         print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                guard let dict = response.result.value as? [String:AnyObject] else { return }
                
                if let data = dict["DATA"] as? [String: Any] {
                    
                    let msg = data["msg"] as? String ?? ""
                    
                    if msg == "Status changed Successfully" {
                        
                        print("Status changed...")
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]!["msg"] as? String ?? "")
                        })
                    }
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    //----------------------------------------
    
    func callWSToGetCurrentEarningsData() {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"driver_current_earning",
                          "office_name":user_dict["office_name"] as? String ?? "",
                          "driver_id": user_dict["driver_id"] as? String ?? ""] as [String : Any]
        
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            switch response.result {
            case .success:
                
                guard let responseData = response.result.value as? [String:AnyObject] else {
                    return
                }
                print(responseData)
                guard let dictData = responseData["DATA"] as? [String:Any] else {
                    return
                }
                
                self.setEarningsData(data: dictData)
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    //MARK: - Take a break Button
    
    @IBAction func BreakButton(sender:UIButton!){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        var driverStatus = ""
        
        if sender.titleLabel?.text == "TAKE A BREAK"{
            driverStatus = "onbreak"
        }else{
            driverStatus = "vacant"
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] ?? [:]
        
        let parameter  = ["office_name":user_dict["office_name"]!,
                          "type":"driver_status_change",
                          "driver_id": user_dict["driver_id"]!,
                          "driver_status":driverStatus ] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"] !=  nil {
                    
                    if dict["DATA"]?["msg"] as! String == "Status changed Successfully"{
                        
                        DispatchQueue.main.async(execute: {
                            
                            if driverStatus == "vacant" {
                                self.navigationItem.title = ""
                            }else{
                                self.navigationItem.title = ""
                            }
                        })
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]!["msg"] as? String ?? "")
                        })
                    }
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    //----------------------------------------
    
    func showAlertForLogoutConfirmation() {
        
        let alert = UIAlertController(title: AppName, message: "Are you sure that you want to logout ?", preferredStyle: .alert)
        
        let noAction = UIAlertAction(title: "No", style: .cancel) { (action) in
            
        }
        
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.callWSToLogout()
        }
        
        alert.addAction(noAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //----------------------------------------
    
    func callWSToLogout() {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
       // self.manager.stopUpdatingLocation()
       // locationManager.stopUpdatingLocation()
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"driver_logout","driver_id": user_dict["driver_id"]! ] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]?["msg"] as! String == "Logged Out Successfully"{
                    
                    if dict["DATA"] !=  nil{
                    }
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.add_manager.stopUpdatingLocation()
                        self.manager.stopUpdatingLocation()
                        self.locationManager.stopUpdatingLocation()
                        
                        DBHelper.sharedInstance.deleteAll(str_entity: "Jobs")
                        //BASE_URL = "http://tbmslive.com/taxi_app/WebServices/WSv7.php"
                        //UserDefaults.standard.set("No", forKey: "isLoggedIn")
                        UserDefaults.standard.set(false, forKey: "isDriverLoggedIn")
                        appDelegate.changeRootViewController(with: "LoginView", storyBoard: "Second")

                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                     appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    //----------------------------------------
    
    //MARK: - Logout Button
    
    @IBAction func logout() {
        
        self.showAlertForLogoutConfirmation()
        
    }

    //----------------------------------------
    
    //MARK:- Location
    
    @IBAction func sendManualLocation(){
        
        let alert = UIAlertController(title: AppName, message: "Share location", preferredStyle: .actionSheet)
        
        let sms_action = UIAlertAction(title: "Via SMS", style: .default) { (action) in
            self.getSMSLink()
        }
        
        let system_action = UIAlertAction(title: "Via System", style: .default) { (action) in
            if let loc = self.locationManager.location{
            self.sendManualLocationToServer(loc: loc)
            }else{
                SCLAlertView().showError(AppName, subTitle: "No Location Found!")
            }
        }
        
        let cancel_action = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(sms_action)
        alert.addAction(system_action)
        alert.addAction(cancel_action)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    
    func sendManualLocationToServer(loc:CLLocation!){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        //let dateStr = Date().stringWithFormatter(dateFormat: "dd MMM yyyy HH:mm")
        //let dateStr = NSDate.toString(date: NSDate(), format: "dd MMM yyyy HH:mm:ss a")
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"update_driver_loc", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"latitude":"\(loc.coordinate.latitude)","longitude":"\(loc.coordinate.longitude)"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                    DispatchQueue.main.async(execute: {
                        if let data = dict["DATA"] as? String,data == "Current Location Updated" {
                            SCLAlertView().showSuccess(AppName, subTitle: "Location sent successfully!")
                        }
                    })
                
                
            case .failure(let error):
                print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        
    }

    
    func getSMSLink(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        
        if self.locationManager.location == nil{
            SCLAlertView().showError(AppName, subTitle: "No Location Found!")
            return
        }
        
        let loc = locationManager.location!
        //let dateStr = Date().stringWithFormatter(dateFormat: "dd MMM yyyy HH:mm")
        //let dateStr = NSDate.toString(date: NSDate(), format: "dd MMM yyyy HH:mm:ss a")
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"return_link", "office_name":user_dict["office_name"]!,"latitude":"\(loc.coordinate.latitude)","longitude":"\(loc.coordinate.longitude)"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                DispatchQueue.main.async(execute: {
                    if let data = dict["DATA"] as? String{
                        self.makeSMS(urlStr: data)
                    }
                })
                
            case .failure(let error):
                print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
    }
    
    func makeSMS(urlStr : String!){
        
        if MFMessageComposeViewController.canSendText(){
            
            let message = MFMessageComposeViewController()
            message.messageComposeDelegate = self
            //message.recipients = [lbl_sms.text!]
            message.subject = AppName
            message.body = "My Current location is: \(urlStr!)"
            self.present(message, animated: true, completion: {
                
            })
        }
        else{
            appDelegate.showerror(str: "Can not able to send message.")
        }
    }
    
    //MARK:- Message Delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result {
        case .sent:
            SCLAlertView().showSuccess(AppName, subTitle: "Message Sent successfully.")
        case .cancelled:
            appDelegate.showerror(str: "Message Cancelled.")
            
        case .failed:
            appDelegate.showerror(str: "Message failed.")
        }
        controller.dismiss(animated: true) {
            
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showTopUpWebView"{
            let nav = segue.destination as! UINavigationController
            let signup = nav.viewControllers[0] as! SignupVC
            signup.titleStr = "Top Up"
            signup.signupURL = sender as! String
        }
        else if segue.identifier == "GotoBookings"{
            let all_jobs = segue.destination as! AllActiveJobsVC
            let btn = sender as! UIButton
            all_jobs.job_type =  btn == btn_acceptedJobs ? "Accepted" : "New"
        }
    }
    
    //MARK:- Unwind Segue
    @IBAction func unwindToDashboard(segue:UIStoryboardSegue) { }
    

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
