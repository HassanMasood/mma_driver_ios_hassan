//
//  NotificationJobVC.swift
//  Ecabbi
//
//  Created by Kishor Lodhia on 06/06/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import PKHUD
import SCLAlertView
import Alamofire
import Reachability

class NotificationJobVC: UIViewController {
    
    @IBOutlet weak var stackdetails:UIStackView!
    @IBOutlet weak var stack_mobile:UIStackView!
    @IBOutlet weak var stack_address:UIStackView!
    @IBOutlet weak var stack_others:UIStackView!
    
    var jobDetails = [String:AnyObject]()
    var jobID :NSInteger?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //self.setupData()
        self.Refresh_JobDetailsAPI { (fiinish) in
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Setup Data
    
    func setupData()  {
        //print(jobDetails)
        
        // Details 
        let arr_details = ["job_date","job_time","name"]
        for str in arr_details{
            if let jobdetail = jobDetails[str] as? String, !jobdetail.isEmptyAfterTrim{
                
                let lbl_detail = UILabel()
                lbl_detail.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_detail.backgroundColor = UIColor.clear
                lbl_detail.text  = jobdetail
                lbl_detail.textAlignment = .center
                stackdetails.addArrangedSubview(lbl_detail)
            }
        }
        
        // Mobiles 
        let arr_mobile = ["mobile","fare","payment_type"]
        let arr_mobile_icon = [#imageLiteral(resourceName: "mobile") , #imageLiteral(resourceName: "coin") , #imageLiteral(resourceName: "payment_method")]
        for (index,str) in arr_mobile.enumerated() {
            if let mobile = jobDetails[str] as?  String , !mobile.isEmptyAfterTrim{
                
                let sub_stack = UIStackView()
                sub_stack.axis  = .horizontal
                sub_stack.distribution  = .fillProportionally
                sub_stack.alignment = .center
                sub_stack.spacing   = 8.0
                
                let img_mobile = UIImageView()
                //img_mobile.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
                img_mobile.contentMode = .scaleAspectFit
                img_mobile.image = arr_mobile_icon[index]
                img_mobile.widthAnchor.constraint(equalToConstant: 30).isActive = true
                img_mobile.heightAnchor.constraint(equalToConstant: 30).isActive = true
                sub_stack.addArrangedSubview(img_mobile)
                
                let lbl_mobile = UILabel()
                lbl_mobile.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_mobile.backgroundColor = UIColor.clear
                lbl_mobile.text  = mobile
                lbl_mobile.textAlignment = .center
                sub_stack.addArrangedSubview(lbl_mobile)
                
                img_mobile.centerYAnchor.constraint(equalTo: lbl_mobile.centerYAnchor).isActive = true
                
                stack_mobile.addArrangedSubview(sub_stack)
                
            }
        }
        
        
        // Addresses 
        let arr_address = ["pickup","via_address","stop1","stop2","stop3","destination", "note"]
        let arr_address_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "road"),#imageLiteral(resourceName: "stop") ,#imageLiteral(resourceName: "stop"),#imageLiteral(resourceName: "stop"), #imageLiteral(resourceName: "destination"), #imageLiteral(resourceName: "note")]
        for (index,str) in arr_address.enumerated(){
            if let pickupadd = jobDetails[str] as?  String , !pickupadd.isEmptyAfterTrim{
                
                let img_pickupadd = UIImageView()
                img_pickupadd.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                //img_pickupadd.widthAnchor.constraint(equalTo: stack_address.widthAnchor, multiplier: 1.0).isActive = true
                img_pickupadd.heightAnchor.constraint(equalToConstant: 1).isActive = true
                stack_address.addArrangedSubview(img_pickupadd)
                
                let sub_stack = UIStackView()
                sub_stack.axis  = .horizontal
                sub_stack.distribution  = .fillProportionally
                sub_stack.alignment = .center
                sub_stack.spacing   = 15.0
                
                let img_addressIcon = UIImageView()
                //img_mobile.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
                img_addressIcon.contentMode = .scaleAspectFit
                img_addressIcon.image = arr_address_icon[index]
                img_addressIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
                img_addressIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
                sub_stack.addArrangedSubview(img_addressIcon)
                
                let lbl_address = UILabel()
                lbl_address.numberOfLines = 0
                lbl_address.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_address.backgroundColor = UIColor.clear
                //lbl_address.text  = "\(arr_address[index])".capitalized + ":" + "  " + pickupadd
                //lbl_address.text  = String(arr_address[index]!).capitalized + ":" + "  " + pickupadd
                lbl_address.text  = pickupadd
                lbl_address.textAlignment = .left
                sub_stack.addArrangedSubview(lbl_address)
                
                img_addressIcon.centerYAnchor.constraint(equalTo: lbl_address.centerYAnchor).isActive = true
                
                stack_address.addArrangedSubview(sub_stack)
                
            }
        }
        
        if stack_address.arrangedSubviews.count != 0 {
            let img_pickupadd = UIImageView()
            img_pickupadd.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            img_pickupadd.heightAnchor.constraint(equalToConstant: 1).isActive = true
            stack_address.addArrangedSubview(img_pickupadd)
        }
        
        
        
        
        // Others 
        let arr_others = ["vehicle_type","flight_no","caller","gratuity","car_park","wttime","extras","meter","luggage","num_of_people"]
        let arr_others_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "plane") ,#imageLiteral(resourceName: "caller") ,#imageLiteral(resourceName: "medal") ,#imageLiteral(resourceName: "car_parking"),#imageLiteral(resourceName: "sand_clock"), #imageLiteral(resourceName: "plus"),#imageLiteral(resourceName: "meter"),#imageLiteral(resourceName: "luggage"),#imageLiteral(resourceName: "people")]
        
        for i in stride(from: 0, to: arr_others.count, by: 2) {
            
            let sub_stack = UIStackView()
            sub_stack.axis  = .horizontal
            sub_stack.distribution  = .fillEqually
            sub_stack.alignment = .fill
            sub_stack.spacing   = 25.0
            
            for j in 0...1{
                
                let str  =    (j == 0) ?  arr_others[i] : arr_others[i+1]
                let icon =    (j == 0) ?  arr_others_icon[i] : arr_others_icon[i+1]
                
                if let other = jobDetails[str] as?  String , !other.isEmptyAfterTrim{
                    
                    let sub_stack2 = UIStackView()
                    sub_stack2.axis  = .horizontal
                    sub_stack2.distribution  = .fill
                    sub_stack2.alignment = .fill
                    sub_stack2.spacing   = 15.0
                    
                    let img_addressIcon = UIImageView()
                    img_addressIcon.contentMode = .scaleAspectFit
                    img_addressIcon.image = icon
                    img_addressIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
                    img_addressIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
                    sub_stack2.addArrangedSubview(img_addressIcon)
                    
                    let lbl_address = UILabel()
                    lbl_address.numberOfLines = 0
                    lbl_address.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    lbl_address.backgroundColor = UIColor.clear
                    lbl_address.text  = other
                    lbl_address.textAlignment = .left
                    sub_stack2.addArrangedSubview(lbl_address)
                    
                    img_addressIcon.centerYAnchor.constraint(equalTo: lbl_address.centerYAnchor).isActive = true
                    
                    sub_stack.addArrangedSubview(sub_stack2)
                }
            }
            
            if sub_stack.arrangedSubviews.count != 0 {
                stack_others.addArrangedSubview(sub_stack)
            }
        }
    }
    
    func Refresh_JobDetailsAPI(completion:@escaping (_ isFinish:Bool) -> Void){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_loaded","job_id":jobID!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"device_type":"ios"] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                let arr = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    
                    DispatchQueue.main.async(execute: {
                        self.jobDetails = arr[0]
                        self.setupData()
                        completion(true)
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: arr[0]["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:arr[0]["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        completion(true)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    completion(true)
                })
            }
        })
    }
    
    //MARK:- Action for Accept & Reject Job
    
    @IBAction func jobaction(sender:UIButton!){
        
        let jobtype =  sender.tag == 1 ? "driver_job_accept" : "driver_job_reject"
        self.Accept_Reject_JobAPI(jobType: jobtype)
    }
    
    func Accept_Reject_JobAPI(jobType:String!){
        
        //HUD.show(.progress)
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":jobType,"job_id":jobID!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            //HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Accepted"{
                    if jobType == "driver_job_accept"{
                    //self.ChangeJob_statusAPI(jobID: self.jobID!)
                        
                        self.Refresh_JobDetailsAPI { (finish) in
                            //self.navigationController?.popViewController(animated: false)
                            self.dismiss(animated: false, completion: {
                                
                                let jobProcessvc = JobProcessVC.viewController()
                                
                                jobProcessvc.jobDetails = self.jobDetails
                                
                                let nav  = appDelegate.window?.rootViewController as! UINavigationController
                                nav.pushViewController(jobProcessvc, animated: false)
                            })
                            
                        }
                    }
                    else{
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    
    func ChangeJob_statusAPI(jobID:NSInteger!){
        
        //HUD.show(.progress)
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_complete","job_id":jobID, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":4] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            // HUD.hide()
            
            //self.navigationController?.popViewController(animated: true)
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                
                if dict["DATA"]!["msg"] as! String ==  "Job Completed"{
                                        
                     DispatchQueue.main.async(execute: {
                     
                        self.Refresh_JobDetailsAPI { (finish) in

                            self.dismiss(animated: false, completion: {
                                let jobProcessvc = JobProcessVC.viewController()
                                
                                jobProcessvc.jobDetails = self.jobDetails
                                
                                let nav  = appDelegate.window?.rootViewController as! UINavigationController
                                nav.pushViewController(jobProcessvc, animated: false)
                            })
                            
                        }
                     
                     })
                    
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                       // SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
