//
//  MessagesVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 11/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import Messages
import MessageUI

import SCLAlertView
import Alamofire
import PKHUD

class MessagesVC: CommonVC , UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var lbl_call : UILabel!
    @IBOutlet weak var lbl_sms  : UILabel!
    
    var arr_static_messages = [[String:AnyObject]]()
    var arr_messages = [[String:AnyObject]]()
    
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var bottomConsTextfieldView: NSLayoutConstraint!
    @IBOutlet weak var txt_msg : UITextField!
    
    @IBOutlet weak var view_pic : UIView!
    @IBOutlet weak var pic : UIPickerView!
    
    let refreshControl = UIRefreshControl()
    
    //----------------------------------------
    
    // MARK: Abstract Methods
    
    //----------------------------------------

    class func viewController() -> MessagesVC {
        return UIStoryboard(name: "Second", bundle: nil).instantiateViewController(withIdentifier: "MessagesVC")  as! MessagesVC
    }
    
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.SetupData()
        
        
        let title = NSLocalizedString("PullToRefresh", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.addTarget(self,
                                 action: #selector(refreshData),
                                 for: .valueChanged)
        tbl.refreshControl = refreshControl
    }
    
    //MARK:- Setup Data
    
    func SetupData(){
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let arr_phone = (user_dict["phonenumber"] as! String).components(separatedBy: ",")
        
        if !arr_phone.isEmpty{
            lbl_call.text = arr_phone[0]
        }
        if arr_phone.count == 1 {
            lbl_sms.text = arr_phone[0]
        }
        else{
            lbl_sms.text = arr_phone[1]
        }
        
        
        
        tbl.estimatedRowHeight = 44.0
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView(frame: CGRect.zero)
        
        self.getAllMessages()
        self.getAll_static_Messages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK:- Keyboard Hide/Show
    /*
    @objc func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConsTextfieldView.constant = keyboardFrame.size.height + 0
        })
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification) {
        //let info = notification.userInfo!
        //let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConsTextfieldView.constant = 0
        })
    }
*/
    
    
    //MARK: call button
    
    @IBAction func makePhoneCall(sender:UIButton!){
        
       // guard let number = URL(string: "telprompt://" + sender.titleLabel!.text!) else { return }
       // UIApplication.shared.open(number, options: [:], completionHandler: nil)

        
        //let url = URL(string: "telprompt://" + sender.titleLabel!.text!)
        let url = URL(string: "telprompt://" + lbl_call.text!)
        
        if UIApplication.shared.canOpenURL(url!){
            UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        else{
            appDelegate.showerror(str: "Phone call is not available.")
        }
    }
    
    //MARK:- SMS Button
    
    @IBAction func makeSMS(sender:UIButton!){
        
        if MFMessageComposeViewController.canSendText(){
            
            let message = MFMessageComposeViewController()
            message.messageComposeDelegate = self
            //message.recipients = [sender.titleLabel!.text!]
            message.recipients = [lbl_sms.text!]
            message.subject = AppName
            self.present(message, animated: true, completion: { 
                
            })
        }
        else{
            appDelegate.showerror(str: "Can not able to send message.")
        }
    }
    
    //MARK:- Message Delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result {
        case .sent:
             SCLAlertView().showSuccess(AppName, subTitle: "Message Sent successfully.")
        case .cancelled:
            appDelegate.showerror(str: "Message Cancelled.")
            
        case .failed:
            appDelegate.showerror(str: "Message failed.")
        }
        controller.dismiss(animated: true) {
            
        }
    }
    
    
    //MARK:- Get Static Messages
    
    @IBAction func show_picker(){
        self.view.endEditing(true)
        self.pic.reloadAllComponents()
        self.pic.selectRow(0, inComponent: 0, animated: false)
        self.view_pic.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_pic.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_pic.alpha = 1.0
            self.view_pic.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @IBAction func cancel_picker(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_pic.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_pic.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    @IBAction func send_picker(){
        
        let dict = arr_static_messages[pic.selectedRow(inComponent: 0)]
        if let str = dict["msg_text"] as? String, !str.isEmpty{
            self.sendmessageAPI(msg_str: str)
        }
        self.cancel_picker()
    }
    
    func getAll_static_Messages(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"messages", "office_name":user_dict["office_name"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                if let arr = dict["DATA"] as? [[String:AnyObject]],!arr.isEmpty{
                 self.arr_static_messages = dict["DATA"] as! [[String:AnyObject]]
                if self.arr_static_messages[0]["msg"] ==  nil{
                    
                    DispatchQueue.main.async(execute: {
                        self.pic.reloadAllComponents()
                    })
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: self.arr_messages[0]["msg"] as! String)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    //MARK:- Picker View
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_static_messages.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let dict = arr_static_messages[row]
        
        return dict["msg_text"] as? String ?? ""
        
    }
    
    
    //MARK:- Get All Messages
    func getAllMessages(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            
            if isRefresh{
                self.refreshControl.endRefreshing()
            }
            
            return
        }
        
        
        if !isRefresh{
            HUD.show(.progress)
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"msg_transaction_list_driver", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            self.refreshControl.endRefreshing()

            switch response.result {
            case .success:
                
                guard let dict = response.result.value as? [String:AnyObject] else {
                    return
                }
                
                guard let arr = dict["DATA"] as? [[String:AnyObject]] else {
                    return
                }
                
                self.arr_messages = arr
                
                if self.arr_messages[0]["msg"] ==  nil {
                    
                    self.arr_messages = self.arr_messages.reversed()
                        DispatchQueue.main.async(execute: {
                            self.tbl.reloadData()
                            self.tbl.selectRow(at: NSIndexPath(row: self.arr_messages.count - 1, section: 0) as IndexPath, animated: true, scrollPosition: .bottom)
                        })
                    
                } else {
                    DispatchQueue.main.async(execute: {
                         appDelegate.showerror(str: self.arr_messages[0]["msg"] as? String ?? "")
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                     appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    
    //MARK:- Send message
    
    @IBAction func sendMessage(sender:UIButton!){
        self.view.endEditing(true)
        var str = txt_msg.text!
        str = str.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if !str.isEmpty{
            txt_msg.resignFirstResponder()
            self.sendmessageAPI(msg_str: str )
        }
        else{
            appDelegate.showerror(str: "Message seems invalid input.")
            
            txt_msg.text = ""
        }
    }
    
    func sendmessageAPI(msg_str:String!){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        txt_msg.text  = ""
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"msg_transaction_to_operator","from_type":"driver","msg_text":msg_str!, "office_name":user_dict["office_name"]!, "from_user_id": user_dict["driver_id"]!] as [String : Any]
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                guard let dict = response.result.value as? [String:AnyObject] else {
                    return
                }
                
                guard let arr = dict["DATA"] as? [[String:AnyObject]] else {
                    return
                }
                
                guard  let msg = arr[0]["msg"] as? String else {
                    return
                }
                
                if msg == "Message sent Successfully"{
                    
                    self.getAllMessages()
                    
                } else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: msg)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        
    }
    
    //MARK:- TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK:- Pull To Refresh
    
    @objc func refreshData(){
       self.getAllMessages(isRefresh: true)
    }
    
    /*
    @objc func refreshOptions(sender: UIRefreshControl) {
        // Perform actions to refresh the content
        // ...
        // and then dismiss the control
        
        if appDelegate.reachability.connection == .none{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"msg_transaction_list_driver", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            sender.endRefreshing()

            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                self.arr_messages = dict["DATA"] as! [[String:AnyObject]]
                if self.arr_messages[0]["msg"] ==  nil{
                    self.arr_messages = self.arr_messages.reversed()
                    DispatchQueue.main.async(execute: {
                        self.tbl.reloadData()
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                         appDelegate.showerror(str: self.arr_messages[0]["msg"] as! String)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                     appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        
        
        
    }
    */
    
    //MARK: - TableView 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr_messages.count
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tbl.dequeueReusableCell(withIdentifier: "Cell") as! MessageCell
        
        let dict = arr_messages[indexPath.row]
                
        let fromType = dict["from_type"] as? String ?? ""
        
        let message = dict["msg_text"] as? String ?? ""
        
        if fromType == "Driver" {
            
            cell.lblOutgoingMessage.isHidden = false
            cell.lblIncomingMessage.isHidden = true
            cell.viewOut.isHidden = false
            cell.viewIn.isHidden = true
            cell.lblOutgoingMessage.text =  " \(fromType) : \(message) "
//            cell.backgroundColor = UIColor(hexString: "FFFFFF", withAlpha: 0.4)
        } else{
            cell.viewOut.isHidden = true
            cell.viewIn.isHidden = false
            cell.lblOutgoingMessage.isHidden = true
            cell.lblIncomingMessage.isHidden = false
            cell.lblIncomingMessage.text = " \(fromType) : \(message) "
//            cell.backgroundColor = UIColor(complementaryFlatColorOf: UIColor(hexString: "ffb347"), withAlpha: 0.4)
        }
        
        return cell
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
