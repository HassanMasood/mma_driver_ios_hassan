//
//  AddBookingVC.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 11/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import GooglePlaces
import PKHUD
import SCLAlertView
import Alamofire

class AddBookingVC: CommonVC,CLLocationManagerDelegate,selected_PlaceDeleagte,UITextViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var datePicker : UIDatePicker!
    @IBOutlet weak var driverPicker : UIPickerView!
    
    @IBOutlet weak var vehiclePicker: UIPickerView!
    
    @IBOutlet weak var view_datePicker : UIView!
    @IBOutlet weak var view_Vehiclepicker: UIView!
    
    @IBOutlet weak var txt_account_name : UITextField!
    @IBOutlet weak var txt_passenger_name : UITextField!
    @IBOutlet weak var txt_customer_mobile : UITextField!
    @IBOutlet weak var txt_other_reference : UITextField!
    
    @IBOutlet weak var stepper_stop : UIStepper!
    
    @IBOutlet weak var txt_pickup : IQTextView!
    @IBOutlet weak var txt_via : IQTextView!
    @IBOutlet weak var txt_stop1 : IQTextView!
    @IBOutlet weak var txt_stop2 : IQTextView!
    @IBOutlet weak var txt_stop3 : IQTextView!
    @IBOutlet weak var txt_destination : IQTextView!
    
    @IBOutlet weak var txt_notes : IQTextView!
    
    @IBOutlet weak var txt_vehicle_type : UITextField!
    @IBOutlet weak var txt_fare : UITextField!
    // @IBOutlet weak var switch_meter : UISwitch!
    @IBOutlet weak var txt_date : UITextField!
    @IBOutlet weak var txt_time : UITextField!
    //@IBOutlet weak var txt_driver_percentage : UITextField!
    //@IBOutlet weak var txt_driver_incentive : UITextField!
    @IBOutlet weak var txt_flight_no : UITextField!
    @IBOutlet weak var txt_no_Of_people : UITextField!
    @IBOutlet weak var txt_luggage : UITextField!
    @IBOutlet weak var txt_hand_luggage : UITextField!
    @IBOutlet weak var txt_car_park : UITextField!
    @IBOutlet weak var txt_extra : UITextField!
    @IBOutlet weak var txt_gratuity : UITextField!
    @IBOutlet weak var txt_booking_fees : UITextField!
    @IBOutlet weak var txt_waiting_charge : UITextField!
    
    @IBOutlet weak var img_payment_receive : UIImageView!
    @IBOutlet weak var btn_payment_receive : UIButton!
    @IBOutlet weak var img_assign_driver : UIImageView!
    @IBOutlet weak var btn_assign_driver : UIButton!
    
    @IBOutlet weak var img_wheel_chair : UIImageView!
    @IBOutlet weak var btn_wheel_chair : UIButton!
    @IBOutlet weak var img_child_seat : UIImageView!
    @IBOutlet weak var btn_child_seat : UIButton!
    
    @IBOutlet weak var stackSelectDriver : UIStackView!
    @IBOutlet weak var btnSelectDriver : UIButton!
    @IBOutlet weak var txtDriver : UITextField!
    
    
    let locationManager = CLLocationManager()
    
    var placesClient = GMSPlacesClient()
    
    var selectedTextView = IQTextView()
    var arr_Vehicle = [[String:AnyObject]]()
    
    var selectedTextField = UITextField()
    var arrDrivers = [[String:AnyObject]]()
    var selectedDriverData = [String: Any]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    
    
    func setupUI(){
        
        datePicker.minimumDate = Date()
        //txt_date.text = NSDate.toString(date: NSDate() , format: "dd MMM yyyy")
        //txt_time.text = NSDate.toString(date: NSDate() , format: "HH:mm")
        txt_date.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        txt_time.text = Date().stringWithFormatter(dateFormat: "HH:mm")
        
        self.txtDriver.layer.borderWidth = 1.0
        self.txtDriver.layer.borderColor = UIColor.black.cgColor
        
        self.txtDriver.delegate = self
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if CLLocationManager.authorizationStatus() == .notDetermined{
            //self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.requestAlwaysAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
        
        self.stackSelectDriver.layer.cornerRadius = 5
        self.stackSelectDriver.layer.borderColor = UIColor.black.cgColor
        self.stackSelectDriver.layer.borderWidth = 2.0
        
        if isAllowsAssignOther() == true {
            self.stackSelectDriver.isHidden = false
            self.callWSToGetAllDriverList()
        }else{
            self.stackSelectDriver.isHidden = true
        }

    }
    
    //----------------------------------------
    
    func isAllowsAssignOther()-> Bool {
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return false
        }
        
        var driver_login = user_dict["allow_booking"] as? String ?? ""
        
        driver_login = driver_login.lowercased()
        
        if driver_login == "yesother" || driver_login == "yesreassign" {
            return true
        }
        
        return false
    }
    
    //----------------------------------------
    
    func isNeedToAssignToOtherDriver() -> Bool {
        
        if self.isAllowsAssignOther() == true {
            
            if selectedDriverData.count > 0, let txt = self.txtDriver.text, txt.count > 0 {
                return true
            }
        }
        return false
    }
    
    //----------------------------------------
    
    func callWSToAssignJobToDriver(withJobId jobId: String) {
        
        if appDelegate.reachability.connection == .unavailable {
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let driverId = self.selectedDriverData["driver_id"] as? String ?? ""
        
        let parameter  = ["type":"driver_job_assing",
                          "driver_id": driverId,
                          "job_id": jobId,
                          "office_name":user_dict["office_name"] as? String ?? ""] as [String : AnyObject]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL, method: .get, parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            print(response)
            
            switch response.result {
            case .success:
                
                guard let dict = response.result.value as? [String:AnyObject], let result = dict["RESULT"] as? String else {
                    return
                }
                
                if result == "OK" {
                    
                    print("Job assigned successfully..!!")
                    
                    SCLAlertView().showSuccess(AppName, subTitle: "Your booking is successfully received.")
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }else{
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
        
    }
    
    //----------------------------------------
    
    func callWSToGetAllDriverList(isFromPick: Bool = false) {
        
        if appDelegate.reachability.connection == .unavailable {
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"driver_list",
                          "office_name":user_dict["office_name"] as? String ?? ""] as [String : AnyObject]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL, method: .get, parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            print(response)
            
            switch response.result {
            case .success:
                
                guard let dict = response.result.value as? [String:AnyObject] else {
                    return
                }
                
                if dict["RESULT"] as! String == "OK"{
                    
                    guard var arr  = dict["DATA"] as? [[String:AnyObject]] else {
                        return
                    }
                    
                    arr.remove(at: 0)
                    
                    self.arrDrivers = arr
                    
                    DispatchQueue.main.async(execute: {
                        
                        if self.arrDrivers.count > 0 && isFromPick == true {
                            
                            let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
                            DispatchQueue.main.asyncAfter(deadline: when) {
                                self.view.endEditing(true)
                            }
                            self.selectedTextField = self.txtDriver
                            self.vehiclePicker.reloadAllComponents()
                            self.vehiclePicker.selectRow(0, inComponent: 0, animated: false)
                            self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                            self.view_Vehiclepicker.alpha = 0.0
                            UIView.animate(withDuration: 0.25, animations: {
                                self.view_Vehiclepicker.alpha = 1.0
                                self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            })
                        }
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
        
    }
    
    //----------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Current Lcoation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            
            if (error != nil) {
                //appDelegate.showerror(str: error?.localizedDescription)
                return
            }
            
            if placemarks?.count != 0 {
                let pm = placemarks?[0]
                self.displayLocationInfo(placemark: pm!)
                self.locationManager.stopUpdatingLocation()
            } else {
                appDelegate.showerror(str: "Problem with the data received from geocoder")
            }
        }
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        
        //locationManager.stopUpdatingLocation()
        
        var address = ""
        
        if let name = placemark.name, !name.isEmpty{
            address = name
        }
        
        if let thoroughfare = placemark.thoroughfare, !thoroughfare.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + thoroughfare
        }
        
        if let subThoroughfare = placemark.subThoroughfare, !subThoroughfare.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + subThoroughfare
        }
        
        if let locality = placemark.locality, !locality.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + locality
        }
        
        if let subLocality = placemark.subLocality, !subLocality.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + subLocality
        }
        
        if let administrativeArea = placemark.administrativeArea, !administrativeArea.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + administrativeArea
        }
        
        if let subAdministrativeArea = placemark.subAdministrativeArea, !subAdministrativeArea.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + subAdministrativeArea
        }
        
        if let postalCode = placemark.postalCode, !postalCode.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + postalCode
        }
        
        if let country = placemark.country, !country.isEmpty{
            if !address.isEmpty{
                address = address + ","
            }
            address = address + " " + country
        }
        
        //if selectedTextView == txt_pickup{
        if txt_pickup.text.isEmpty{
            txt_pickup.text = address
        }
        //}
        //        else if selectedTextView == txt_dropoff{
        //            txt_dropoff.text = address
        //        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //debugPrint("Error while updating location " + error.localizedDescription)
        // appDelegate.showerror(str: error.localizedDescription)
    }
    
    //MARK:- Hide Keyboard
    func hideKeyboard(){
        txt_account_name.resignFirstResponder()
        txt_passenger_name.resignFirstResponder()
        txt_customer_mobile.resignFirstResponder()
        txt_other_reference.resignFirstResponder()
        txt_notes.resignFirstResponder()
        txt_fare.resignFirstResponder()
        // txt_driver_percentage.resignFirstResponder()
        // txt_driver_incentive.resignFirstResponder()
        txt_flight_no.resignFirstResponder()
        txt_no_Of_people.resignFirstResponder()
        txt_car_park.resignFirstResponder()
        txt_extra.resignFirstResponder()
        txt_gratuity.resignFirstResponder()
        txt_booking_fees.resignFirstResponder()
        txt_waiting_charge.resignFirstResponder()
        
    }
    
    //MARK:-  TextField Delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        if (textField == txt_vehicle_type) {
            
            let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.view.endEditing(true)
            }
            //self.view.endEditing(true)
            //self.hideKeyboard()
            selectedTextField = txt_vehicle_type
            
            self.getVehicleData()
            
        } else if (textField == txt_luggage || textField == txt_hand_luggage || textField == txtDriver) {
            
            if textField == txtDriver && self.arrDrivers.count <= 0 {
                self.callWSToGetAllDriverList(isFromPick: true)
            }else{
                let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.view.endEditing(true)
                }
                selectedTextField = textField
                self.vehiclePicker.reloadAllComponents()
                self.vehiclePicker.selectRow(0, inComponent: 0, animated: false)
                self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.view_Vehiclepicker.alpha = 0.0
                UIView.animate(withDuration: 0.25, animations: {
                    self.view_Vehiclepicker.alpha = 1.0
                    self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
            }
            
        } else if (textField == txt_date) {
            let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.view.endEditing(true)
            }
            //            self.view.endEditing(true)
            //            self.hideKeyboard()
            selectedTextField = txt_date
            
            datePicker.minimumDate = Date()
            datePicker.datePickerMode = .date
            
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.view_datePicker.alpha = 1.0
                self.view_datePicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
            
        else if (textField == txt_time) {
            let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.view.endEditing(true)
            }
            //            self.view.endEditing(true)
            //            self.hideKeyboard()
            selectedTextField = txt_time
            
            datePicker.minimumDate = Date()
            datePicker.datePickerMode = .time
            
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.view_datePicker.alpha = 1.0
                self.view_datePicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
        return false
    }
    
    
    //MARK:-  TextView Delegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if (textView == txt_pickup || textView == txt_via || textView == txt_stop1 || textView == txt_stop2 || textView == txt_stop3 || textView==txt_destination){
            
            //self.hideKeyboard()
            selectedTextView = textView as! IQTextView
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controllernav  = storyboard.instantiateViewController(withIdentifier: "AutoCompletePlacesVC") as! UINavigationController
            let autocompleteplacevc = controllernav.topViewController as! AutocompletePlacesVC
            autocompleteplacevc.SelectedPlacedelegate = self
            autocompleteplacevc.searchText = textView.text!
            self.present(controllernav, animated: true, completion: nil)
            return false
        }
        
        return true
        
    }
    
    
    func selectedPlace(address: String!) {
        
        selectedTextView.text = address
    }
    
    //MARK:- Get Vehicle API
    
    func getVehicleData(){
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"vehicle_type"] as [String : Any]
        // debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //debugPrint(response.result.value!)
            switch response.result {
            case .success:
                
                let dict = response.result.value as! [String:AnyObject]
                
                let arr_temp = dict["DATA"] as! [[String:AnyObject]]
                let dict2 = arr_temp[0]
                if dict2["msg"] as! String == "Vehicle Type"{
                    
                    if dict["DATA"] !=  nil{
                        //UserDefaults.standard.set(dict["DATA"], forKey: "User_Info")
                        // let arr_temp = dict["DATA"] as! [[String:AnyObject]]
                        self.arr_Vehicle = arr_temp.filter({ $0["type"] != nil })
                        self.vehiclePicker.reloadAllComponents()
                        if !self.arr_Vehicle.isEmpty{
                            self.vehiclePicker.selectRow(0, inComponent: 0, animated: false)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        self.view_Vehiclepicker.alpha = 0.0
                        UIView.animate(withDuration: 0.25, animations: {
                            self.view_Vehiclepicker.alpha = 1.0
                            self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        })
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]?["msg"] as! String)
                    })
                }
                
                
            case .failure(let error):
                //debugPrint(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    //MARK:- Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // return arr_Vehicle.count
        switch selectedTextField {
        case txt_vehicle_type:
            return arr_Vehicle.count
        case txt_luggage,txt_hand_luggage:
            return 10
        case txtDriver:
            return self.arrDrivers.count

        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //let dict = arr_Vehicle[row]
        //return dict["type"] as? String
        
        switch selectedTextField {
        case txt_vehicle_type:
            let dict = arr_Vehicle[row]
            return dict["type"] as? String
        case txt_luggage,txt_hand_luggage:
            return String(row+1)
        case txtDriver:
            let dict = arrDrivers[row]
            return dict["driver_name"] as? String ?? ""
        default:
            return ""
        }
    }
    
    @IBAction func Selected_VehiclePicker(sender:UIPickerView!){
        
        let index = vehiclePicker.selectedRow(inComponent: 0)
        
        
        //let dict = arr_Vehicle[index]
        //txt_vehicle_type.text = dict["type"] as? String
        
        switch selectedTextField {
        case txt_vehicle_type:
            let dict = arr_Vehicle[index]
            txt_vehicle_type.text = dict["type"] as? String
        case txt_luggage,txt_hand_luggage:
            selectedTextField.text = String(index + 1)
        case txtDriver:
            self.selectedDriverData = arrDrivers[index]
            selectedTextField.text = self.selectedDriverData["driver_name"] as? String
        default:
            debugPrint("")
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_Vehiclepicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    @IBAction func closeVehiclePicker(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_Vehiclepicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    //MARK:-  Date Picker
    
    @IBAction func Selected_datePicker(sender:UIDatePicker!){
        
        if selectedTextField == txt_date{
            let date = datePicker.date
            // txt_date.text = NSDate.toString(date: date as NSDate, format: "dd MMM yyyy")
            txt_date.text = date.stringWithFormatter(dateFormat: "dd MMM yyyy")
        }
        else if selectedTextField == txt_time{
            let date = datePicker.date
            //txt_time.text = NSDate.toString(date: date as NSDate, format: "HH:mm")
            txt_time.text = date.stringWithFormatter(dateFormat: "HH:mm")
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    @IBAction func closeDatePicker(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_datePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_datePicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    //MARK:- WheelChair.child_seat,payment button
    
    @IBAction func button_select(sender:UIButton!){
        sender.isSelected = !sender.isSelected
        
        switch sender {
        case btn_payment_receive:
            if (sender.isSelected){
                img_payment_receive.image = #imageLiteral(resourceName: "check")
            }else{
                img_payment_receive.image = #imageLiteral(resourceName: "uncheck")
            }
        case btn_assign_driver:
            if (sender.isSelected){
                img_assign_driver.image = #imageLiteral(resourceName: "check")
            }else{
                img_assign_driver.image = #imageLiteral(resourceName: "uncheck")
            }
        case btn_wheel_chair:
            if (sender.isSelected){
                img_wheel_chair.image = #imageLiteral(resourceName: "check")
            }else{
                img_wheel_chair.image = #imageLiteral(resourceName: "uncheck")
            }
        case btn_child_seat:
            if (sender.isSelected){
                img_child_seat.image = #imageLiteral(resourceName: "check")
            }else{
                img_child_seat.image = #imageLiteral(resourceName: "uncheck")
            }
        default:
            debugPrint("Invalid")
        }
    }
    
    //MARK:- Make Booking
    
    func checkValidation() -> (isValid:Bool, msg:String){
        
        //        if txt_account_name.text!.isEmpty{
        //            return (false, "Account name should not be empty")
        //        }
        //        else if txt_customer_mobile.text!.isEmpty{
        //            return (false, "Mobile should not be empty")
        //        }
        //        else if txt_pickup.text!.isEmpty{
        //            return (false, "Pick up address should not be empty")
        //        }
        if txt_date.text!.isEmpty{
            return (false, "Journey date should not be empty")
        }
        else if txt_time.text!.isEmpty{
            return (false, "Journey time should not be empty")
        }
        //        else if txt_vehicleType.text!.isEmpty{
        //            return (false, "Vehicle type should not be empty")
        //        }
        
        return (true, "Valid")
    }
    
    
    @IBAction func makeBooking(){
        
        let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.view.endEditing(true)
        }
        //        self.view.endEditing(true)
        //        self.hideKeyboard()
        
        let check = checkValidation()
        
        if check.isValid{
            
            let user_dict = UserDefaults.standard.value(forKey: "User_Info") as! [String:AnyObject]
            
            //let meter = switch_meter.isOn ? 1: 0
            let wheel_chair = btn_wheel_chair.isSelected ? "yes" : "no"
            let child_seat = btn_child_seat.isSelected ? "yes" : "no"
            let payment_receive = btn_payment_receive.isSelected ? "yes" : "no"
            let donot_assign_driver = btn_assign_driver.isSelected ? "yes" : "no"
            
            
            let parameter  = ["office_name":user_dict["office_name"]!,"type":"driver_makebooking","driver_id": user_dict["driver_id"]!,"operator_id": user_dict["driver_id"]!,"name":txt_account_name.text!,"mobile_no":txt_customer_mobile.text!,"caller":txt_passenger_name.text!,"mobile":txt_customer_mobile.text!,"otherref":txt_other_reference.text!,"pickup":txt_pickup.text!,"via_address":txt_via.text!,"stop1":txt_stop1.text!,"stop2":txt_stop2.text!,"stop3":txt_stop3.text!,"destination":txt_destination.text!,"job_date":txt_date.text!,"job_time":txt_time.text!,"vehicle_type":txt_vehicle_type.text!,"job_type":"Cash","note":txt_notes.text!,"fare":txt_fare.text!,"extras":txt_extra.text!,"meter":"","calc_value":"","incentive_val":"","flight_no":txt_flight_no.text!,"num_of_people":txt_no_Of_people.text!,"luggage":txt_luggage.text!,"hand_luggage":txt_hand_luggage.text!,"car_park":txt_car_park.text!,"gratuity":txt_gratuity.text!,"bookingfees":txt_booking_fees.text!,"wttime":txt_waiting_charge.text!,"propose":0,"child_seat":child_seat,"vehicle_chair":wheel_chair,"payment_r":payment_receive,"DontAssign":donot_assign_driver] as [String : Any]
            
            debugPrint(parameter)
            
            HUD.show(.progress)
            
            //            let headers = [
            //                "Content-Type": "application/json"
            //            ]
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                //APIManager.sharedInstance.sessionManager.request(BASE_URL, method: .get, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseString(completionHandler: { (response) in
                HUD.hide()
                debugPrint(response.request?.url! ?? "")
                debugPrint(response.result.value ?? "")
                switch response.result {
                case .success:
                    
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["DATA"]?["msg"] as! String == "Booking successfully by driver"{
                        
                        if dict["DATA"] !=  nil{
                        }
                        
                        DispatchQueue.main.async {
                            
                            SCLAlertView().showSuccess(AppName, subTitle: "Your booking is successfully received.")
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]?["msg"] as! String)
                        })
                    }
                    
                case .failure(let error):
                    debugPrint("error is: \(error)")
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                    
                }
            })
        }
        else{
            SCLAlertView().showError(AppName, subTitle: check.msg)
        }
    }
    
    @IBAction func exit_press(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
