//
//  MeetGreetVC.swift
//  TBMSDriver
//
//  Created by Sagar Chauhan on 29/04/2019.
//  Copyright © 2019 Piyush Agrawal. All rights reserved.
//

import UIKit

class MeetGreetVC: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    var strPassengerName: String?

    //----------------------------------------
    
    class func viewController() -> MeetGreetVC {
        let meetVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MeetGreetVC") as! MeetGreetVC
        return meetVC
    }
    
    //----------------------------------------
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let name = self.strPassengerName {
            self.lblName.text = name.uppercased()
        }

//        let btnBack = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back-1"), style: .plain, target: self, action: #selector(btnBackTapped(_:)))
//        self.navigationItem.leftBarButtonItem = btnBack
        
    }

    //----------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

extension MeetGreetVC {
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    //-----------------------------------------------------------------------
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    
    //-----------------------------------------------------------------------
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeRight
    }
    
}
