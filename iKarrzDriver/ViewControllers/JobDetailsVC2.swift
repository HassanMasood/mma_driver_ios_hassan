//
//  JobDetailsVC2.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 11/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import SafariServices

import Messages
import MessageUI

import Alamofire
import PKHUD
import SCLAlertView

import BraintreeDropIn
import Braintree

import Stripe


class JobDetailsVC2: CommonVC ,SFSafariViewControllerDelegate,MFMessageComposeViewControllerDelegate,STPAddCardViewControllerDelegate{
    
    @IBOutlet weak var lbl_date : UILabel!
    @IBOutlet weak var lbl_time : UILabel!
    @IBOutlet weak var lbl_ref : UILabel!
    @IBOutlet weak var lbl_name : UILabel!
    @IBOutlet weak var lbl_bookedBy : UILabel!
    // @IBOutlet weak var lbl_caller : UILabel!
    @IBOutlet weak var view_caller: UIView!
    
    @IBOutlet weak var lbl_otherRef : UILabel!
    
    //@IBOutlet weak var lbl_flight : UILabel!
    @IBOutlet weak var btn_flight : UIButton!
    @IBOutlet weak var stack_flight : UIStackView!
    @IBOutlet weak var stack_othersRef : UIStackView!
    
    @IBOutlet weak var lbl_fare : UILabel!
    @IBOutlet weak var lbl_paymentType : UILabel!
    
    @IBOutlet weak var stack_sms: UIStackView!
    @IBOutlet weak var cons_bottom_sms : NSLayoutConstraint!
    @IBOutlet weak var stack_address: UIStackView!
    @IBOutlet weak var stack_others : UIStackView!
    
    @IBOutlet weak var lbl_cust_group_name : UILabel!
    
    var jobDetails : [String:AnyObject]!
    
    @IBOutlet weak var view_payment_info : UIView!
    @IBOutlet weak var lbl_payment_price : UILabel!
    @IBOutlet weak var img_cash_payment : UIImageView!
    @IBOutlet weak var img_card_payment : UIImageView!
    var dict_payment_method = [String:AnyObject]()
    
    var price_fare  = "0"
    var payment_method = "Cash"
    
    @IBOutlet weak var cons_height_button_receive_payments : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Setup Data
    
    func setupData()  {
        
        lbl_date.text = jobDetails["job_date"] as? String
        lbl_time.text = jobDetails["job_time"] as? String
        lbl_ref.text = String(describing: jobDetails["job_id"]!)
        lbl_name.text = jobDetails["name"] as? String
        lbl_bookedBy.text = jobDetails["caller"] as? String
        //lbl_caller.text = jobDetails["otherref"] as? String
        // lbl_caller.text = jobDetails["telephone"] as? String
        lbl_otherRef.text = jobDetails["telephone"] as? String
        
        //lbl_fare.text = "£ " + String(describing: jobDetails["fare"]!)
        lbl_fare.text = jobDetails["fare"] as? String
        lbl_paymentType.text = (jobDetails["payment_type"] as? String)?.uppercased()
        
        //lbl_mobile.text = String(describing: jobDetails["mobile"]!)
        if let flight = jobDetails["flight_no"] as? String, !flight.isEmpty{
            let str_attr = NSAttributedString(string: flight, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor : UIColor.white])
            btn_flight.setAttributedTitle(str_attr, for: .normal)
        }else{
            stack_flight.isHidden = true
        }
        
        print(jobDetails)
        if let caller = jobDetails["caller"] as? String, caller.isEmpty{
            view_caller.isHidden = true
        }
        
        if let flight = jobDetails["flight_no"] as? String, flight.isEmpty, let otherref = jobDetails["telephone"] as? String, otherref.isEmpty{
            stack_othersRef.isHidden = true
        }
        
        if let mobile = jobDetails["mobile"] as? String, mobile.isEmpty{
            stack_sms.heightAnchor.constraint(equalToConstant: 0).isActive = true
            cons_bottom_sms.constant = 0
        }
        
        if let payment_r = jobDetails["payment_r"] as? String, !payment_r.isEmpty{
            if payment_r == "yes"{
                cons_height_button_receive_payments.constant = 0
            }
        }
        
        price_fare = jobDetails["fare"] as? String ?? ""
        
        self.configAddressStack()
        self.configOthersStack()
        
        lbl_cust_group_name.text = jobDetails["cust_group_name"] as? String
        
    }
    
    
    func configAddressStack()  {
        
        stack_address.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let stackHeader = UILabel()
        stackHeader.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        stackHeader.backgroundColor = UIColor.darkGray
        stackHeader.text  = "Route Details"
        stackHeader.textAlignment = .center
        stackHeader.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        //stack_address.addArrangedSubview(stackHeader)
        
        let arr_address = ["pickup","via_address","stop1","stop2","stop3","destination","note"]
        let arr_addressTxt = ["Pickup","Via","Stop1","Stop2","Stop3","Dropoff","Notes"]
        let arr_address_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "road"),#imageLiteral(resourceName: "stop") ,#imageLiteral(resourceName: "stop"),#imageLiteral(resourceName: "stop"), #imageLiteral(resourceName: "destination"),#imageLiteral(resourceName: "note")]
        
        for (index,str) in arr_address.enumerated() {
            
            if let add = jobDetails[str] as?  String , !add.isEmptyAfterTrim{
                
                let sub_stack = UIStackView()
                sub_stack.axis  = .vertical
                sub_stack.distribution  = .fill
                sub_stack.alignment = .fill
                sub_stack.spacing   = 8.0
                
                
                let img_line = UIImageView()
                img_line.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                img_line.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
                sub_stack.addArrangedSubview(img_line)
                
                
                let sub_stack2 = UIStackView()
                sub_stack2.axis  = .horizontal
                sub_stack2.distribution  = .fill
                sub_stack2.alignment = .fill
                sub_stack2.spacing   = 8.0
                sub_stack2.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                sub_stack2.isLayoutMarginsRelativeArrangement = true
                
                let view = UIView()
                view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                
                
                let img_add = UIImageView()
                //img_add.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
                img_add.contentMode = .scaleAspectFit
                img_add.image = arr_address_icon[index]
                view.addSubview(img_add)
                img_add.widthAnchor.constraint(equalToConstant: 30).isActive = true
                img_add.heightAnchor.constraint(equalToConstant: 30).isActive = true
                img_add.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
                img_add.translatesAutoresizingMaskIntoConstraints = false
                //sub_stack2.addArrangedSubview(img_add)
                
                
                let lbl_title = UILabel()
                lbl_title.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_title.backgroundColor = UIColor.clear
                lbl_title.text  = arr_addressTxt[index]
                lbl_title.textAlignment = .left
                lbl_title.widthAnchor.constraint(equalToConstant: 80).isActive = true
                lbl_title.setContentHuggingPriority(UILayoutPriority(rawValue: 750), for: .horizontal)
                //sub_stack2.addArrangedSubview(lbl_title)
                
                let lbl_add = UILabel()
                lbl_add.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_add.backgroundColor = UIColor.clear
                lbl_add.text  = add
                lbl_add.numberOfLines = 0
                lbl_add.textAlignment = .left
                view.addSubview(lbl_add)
                lbl_add.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
                lbl_add.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
                lbl_add.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
                lbl_add.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
                lbl_add.leadingAnchor.constraint(equalTo: img_add.trailingAnchor, constant: 8).isActive = true
                lbl_add.translatesAutoresizingMaskIntoConstraints = false
                //sub_stack2.addArrangedSubview(lbl_add)
                
                
                img_add.centerYAnchor.constraint(equalTo: lbl_add.centerYAnchor).isActive = true
                
                
                let btn_waze = CustomButton(type: .custom)
                btn_waze.imageView!.contentMode = .scaleAspectFit
                //btn_waze.setImage(#imageLiteral(resourceName: "waze"), for: .normal)
                btn_waze.setImage(#imageLiteral(resourceName: "map_loc"), for: .normal)
                btn_waze.widthAnchor.constraint(equalToConstant: 30).isActive = true
                btn_waze.heightAnchor.constraint(equalToConstant: 30).isActive = true
                btn_waze.valueText = add
                //btn_waze.addTarget(self, action: #selector(navtoWaze(sender:)), for: .touchUpInside)
                btn_waze.addTarget(self, action: #selector(locate_map(sender:)), for: .touchUpInside)
                view.addSubview(btn_waze)
                btn_waze.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
                btn_waze.centerYAnchor.constraint(equalTo: lbl_add.centerYAnchor).isActive = true
                btn_waze.leadingAnchor.constraint(equalTo: lbl_add.trailingAnchor, constant: 8).isActive = true
                btn_waze.translatesAutoresizingMaskIntoConstraints = false
                if lbl_title.text == "Notes"{
                    //btn_waze.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
                    //sub_stack2.addArrangedSubview(btn_waze)
                    btn_waze.widthAnchor.constraint(equalToConstant: 0).isActive = true
                    btn_waze.heightAnchor.constraint(equalToConstant: 0).isActive = true
                }
                
                //sub_stack.addArrangedSubview(sub_stack2)
                sub_stack.addArrangedSubview(view)
                
                stack_address.addArrangedSubview(sub_stack)
                
            }
        }
    }
    
    
    func configOthersStack()  {
        
        stack_others.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let stackHeader = UILabel()
        stackHeader.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        stackHeader.backgroundColor = UIColor.darkGray
        stackHeader.text  = "OTHER DETAILS"
        stackHeader.textAlignment = .center
        stackHeader.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        //stack_others.addArrangedSubview(stackHeader)
        
        let img_line = UIImageView()
        img_line.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        img_line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //stack_others.addArrangedSubview(img_line)
        
        let arr_others = ["vehicle_type","luggage","hand_luggage","car_park","wttime","wttime_val","extras","gratuity","vehicle_chair","child_seat","bookingfees","num_of_people","payment_r","no_of_cars","calc_value","incentive_val"]
        let arr_others_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "luggage") , #imageLiteral(resourceName: "hand_lugagge")  ,#imageLiteral(resourceName: "car_parking") ,#imageLiteral(resourceName: "sand_clock") ,#imageLiteral(resourceName: "waiting_time") , #imageLiteral(resourceName: "plus") ,#imageLiteral(resourceName: "medal") , #imageLiteral(resourceName: "wheelChair") , #imageLiteral(resourceName: "child_seat") ,#imageLiteral(resourceName: "booking_fees") ,#imageLiteral(resourceName: "people") , #imageLiteral(resourceName: "payment_received") , #imageLiteral(resourceName: "no_of_cars") , #imageLiteral(resourceName: "handshake"), #imageLiteral(resourceName: "gift")]
        
        for i in stride(from: 0, to: arr_others.count, by: 3) {
            
            let sub_stack = UIStackView()
            sub_stack.axis  = .horizontal
            sub_stack.distribution  = .fillEqually
            sub_stack.alignment = .fill
            sub_stack.spacing   = 15.0
            
            for j in 0...2{
                                
                if i+j > arr_others.count - 1{
                    break
                }
                
                let str = arr_others[i+j]
                let icon = arr_others_icon[i+j]
                
                
                //if let other = jobDetails[str] as?  String , !other.isEmptyAfterTrim{
                //if let other = jobDetails[str] as?  String {
                
                let sub_stack2 = UIStackView()
                sub_stack2.axis  = .horizontal
                sub_stack2.distribution  = .fill
                sub_stack2.alignment = .fill
                sub_stack2.spacing   = 8.0
                
                let img_addressIcon = UIImageView()
                img_addressIcon.contentMode = .scaleAspectFit
                img_addressIcon.image = icon
                img_addressIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
                img_addressIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
                sub_stack2.addArrangedSubview(img_addressIcon)
                
                
                let lbl_address = UILabel()
                lbl_address.numberOfLines = 0
                lbl_address.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_address.backgroundColor = UIColor.clear
                lbl_address.text  = jobDetails[str] as?  String ?? ""
                if str == "payment_r" || str == "vehicle_chair" || str == "child_seat"{
                    if lbl_address.text == "yes"{
                        lbl_address.text = "✅"
                    }else{
                        lbl_address.text = "❌"
                    }
                }
                else if str == "calc_value"{
                    lbl_address.text = jobDetails[str] as? String ?? "0" + " %"
                }
                lbl_address.adjustsFontSizeToFitWidth = true
                lbl_address.minimumScaleFactor = 0.5
                lbl_address.textAlignment = .left
                sub_stack2.addArrangedSubview(lbl_address)
                
                img_addressIcon.centerYAnchor.constraint(equalTo: lbl_address.centerYAnchor).isActive = true
                
                /*
                 let btn = UIButton(type: .custom)
                 btn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                 btn.setTitle(other, for: .normal)
                 btn.titleLabel?.numberOfLines = 0
                 btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                 btn.titleLabel?.adjustsFontSizeToFitWidth = true
                 btn.contentHorizontalAlignment = .left
                 sub_stack2.addArrangedSubview(btn)
                 
                 img_addressIcon.centerYAnchor.constraint(equalTo: btn.centerYAnchor).isActive = true
                 
                 if str == "flight_no"{
                 let str_attr = NSAttributedString(string: other, attributes: [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
                 btn.setAttributedTitle(str_attr, for: .normal)
                 btn.addTarget(self, action: #selector(flight_no(btn:)), for: .touchUpInside)
                 }
                 */
                
                sub_stack.addArrangedSubview(sub_stack2)
                //}
            }
            
            if sub_stack.arrangedSubviews.count != 0 {
                stack_others.addArrangedSubview(sub_stack)
            }
        }
        
    }
    
    //MARK: - Flight
    @IBAction func flight_no(btn:UIButton!){
        //print("flight is \(String(describing: btn.titleLabel!.text))")
        
        if let str = btn.titleLabel?.text{
            //if let str = lbl_flight.text{
            let actionsheet = UIAlertController(title: AppName, message: "check your flight info", preferredStyle: .actionSheet)
            //let p = "h".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let google = UIAlertAction(title: "Google", style: .default, handler: { (action) in
                //let url = URL(string: "https://www.google.co.uk/search?q=\(str)")
                let url = URL(string: "https://www.google.co.uk/search?q=\(str)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
                let sf = SFSafariViewController(url: url!)
                sf.delegate = self
                self.present(sf, animated: true, completion: {
                    
                })
            })
            
            let flightAware = UIAlertAction(title: "FlightAware", style: .default, handler: { (action) in
                //let url = URL(string: "http://uk.flightaware.com/live/flight/\(str)")
                let url = URL(string: "http://uk.flightaware.com/live/flight/\(str)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
                let sf = SFSafariViewController(url: url!)
                sf.delegate = self
                self.present(sf, animated: true, completion: {
                })
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
                
            })
            
            actionsheet.addAction(google)
            actionsheet.addAction(flightAware)
            actionsheet.addAction(cancel)
            
            self.present(actionsheet, animated: true, completion: {
                
            })
        }
    }
    
    
    //MARK:- WAZE app
    @IBAction func navtoWaze(sender: CustomButton!){
        
        let url = URL(string: "waze://?q=\(sender.valueText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let wurl = url , UIApplication.shared.canOpenURL(wurl){
            UIApplication.shared.open(wurl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
        else if let itunesUrl = URL(string: "http://itunes.apple.com/us/app/id323229106") , UIApplication.shared.canOpenURL(itunesUrl) {
            UIApplication.shared.open(itunesUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
    }
    
    @IBAction func navtoGoogleMaps(sender: CustomButton!){
        
        let url = URL(string: "comgooglemaps://?q=\(sender.valueText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        if let wurl = url , UIApplication.shared.canOpenURL(wurl){
            UIApplication.shared.open(wurl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
        else if let itunesUrl = URL(string: "http://itunes.apple.com/us/app/id585027354") , UIApplication.shared.canOpenURL(itunesUrl) {
            UIApplication.shared.open(itunesUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
            })
        }
    }
    
    @IBAction func locate_map(sender: CustomButton!){
        
        let alert = UIAlertController(title: AppName, message: "choose map source", preferredStyle: .actionSheet)
        
        let waze_map = UIAlertAction(title: "via Waze", style: .default) { (action) in
            self.navtoWaze(sender: sender)
        }
        
        let google_map = UIAlertAction(title: "via Google maps", style: .default) { (action) in
            self.navtoGoogleMaps(sender: sender)
        }
        
        let apple_map = UIAlertAction(title: "via Apple maps", style: .default) { (action) in
            
            let url = URL(string: "http://maps.apple.com//?q=\(sender.valueText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            if let wurl = url , UIApplication.shared.canOpenURL(wurl){
                UIApplication.shared.open(wurl, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
                })
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(waze_map)
        alert.addAction(google_map)
        alert.addAction(apple_map)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    //MARK: - Back Press
    @IBAction func backpress(sender:UIButton!){
        self.dismiss(animated: true) {
            
        }
    }
    
    //MARK:- Phone Call
    @IBAction func makePhonecall(){
        
        let mobile = jobDetails["mobile"] as! String
        
        guard let number = URL(string: "telprompt://" + mobile) else { return }
        
        if UIApplication.shared.canOpenURL(number){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(number)
            }
        }
        else{
            SCLAlertView().showError(AppName, subTitle: "Phone call is not available on this device.")
        }
    }
    
    
    //MARK:- SMS Button
    
    @IBAction func makeSMS(sender:UIButton!){
        
        var type = "thank_sms"
        switch sender.tag {
        case 1:
            type = "inform_sms"
        case 2:
            type = "thank_sms"
        case 3:
            type = "enquire_sms"
        default:
            type = "inform_sms"
        }
        
        var bodyStr = ""
        
        if appDelegate.reachability.connection != .unavailable {
            
            let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
            
            let parameter  = ["type":"default_custom_sms", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"job_id":jobDetails["job_id"]!,"id":type] as [String : Any]
            // print(parameter)
            
            HUD.show(.progress)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                //print(response.result.value!)
                switch response.result {
                case .success:
                    // debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    let arr  = dict["DATA"] as! [[String:AnyObject]]
                    if let msg = arr[0]["msg"] as? String{
                        bodyStr = msg
                        
                        DispatchQueue.main.async(execute: {
                            self.sendMessage(bodyStr: bodyStr)
                        })
                        
                    }
                    
                case .failure(let error):
                    print(error)
                    //                    DispatchQueue.main.async(execute: {
                    //                        //self.tbl.isHidden = true
                    //                        appDelegate.showerror(str: error.localizedDescription)
                    //                    })
                }
            })
            
        }
        else{
            self.sendMessage(bodyStr: bodyStr)
        }
        
    }
    
    func sendMessage(bodyStr : String!){
        
        if MFMessageComposeViewController.canSendText(){
            
            let message = MFMessageComposeViewController()
            message.messageComposeDelegate = self
            //message.recipients = [sender.titleLabel!.text!]
            message.recipients =  [jobDetails["mobile"] as! String]
            message.subject = AppName
            message.body = bodyStr!
            self.present(message, animated: true, completion: {
                
            })
        }
        else{
            appDelegate.showerror(str: "Can not able to send message.")
        }
    }
    
    //MARK:- Message Delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result {
        case .sent:
            SCLAlertView().showSuccess(AppName, subTitle: "Message Sent successfully.")
        case .cancelled:
            appDelegate.showerror(str: "Message Cancelled.")
            
        case .failed:
            appDelegate.showerror(str: "Message failed.")
        }
        controller.dismiss(animated: true) {
            
        }
    }
    
    
    //MARK:- Payment View
    
    @IBAction func show_payment(){
        
        lbl_payment_price.text = "Total Payable Amount: \(price_fare)"
        
        self.view.endEditing(true)
        view_payment_info.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        view_payment_info.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_payment_info.alpha = 1.0
            self.view_payment_info.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @IBAction func pay_type(sender:UIButton!){
        
        img_cash_payment.image = sender.tag == 0 ? #imageLiteral(resourceName: "check") : #imageLiteral(resourceName: "uncheck")
        img_card_payment.image = sender.tag == 1 ? #imageLiteral(resourceName: "check") : #imageLiteral(resourceName: "uncheck")
        
        payment_method = sender.tag == 0 ? "Cash" : "Card"
    }
    
    @IBAction func make_payment(){
        
        //if img_cash_payment.image == #imageLiteral(resourceName: "check"){
        if payment_method == "Cash"{
            self.payment_modeAPI(type: "Cash")
        }else{
            self.payment_modeAPI(type: "Card")
            
            //self.Get_braintree_Token()
            self.payment_methodeAPI()
        }
        
        self.cancel_payment()
    }
    
    @IBAction func cancel_payment(){
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_payment_info.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_payment_info.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    
    func payment_modeAPI(type:String!){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payments_buttons","username":user_dict["user_name"]!,"driver_id": user_dict["driver_id"]!,"job_id":jobDetails["job_id"]!,"price":price_fare,"payment_method":type!,"payment_received": type == "Cash" ? "yes" : "no"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                print(dict)
                if dict["RESULT"] as? String == "OK"{
                    DispatchQueue.main.async {
                        if type == "Cash"{
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    
    //MARK:- Get Payment Method
    func payment_methodeAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
//        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payment_type_check"] as [String : Any]
        // print(parameter)
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payment_type_check", "job_id" : jobDetails["job_id"]!] as [String : Any]

        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                //                let dict = response.result.value as! [String:AnyObject]
                //
                //                print(dict)
                if let dict = response.result.value as? [String:AnyObject] , let dict2 = dict["DATA"] as? [String:AnyObject]{
                    
                    self.dict_payment_method = dict2
                    self.configure_payment()
                    
                    
                }else{
                    appDelegate.showerror(str: "Unknown server error!")
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    func configure_payment(){
        
        debugPrint(self.dict_payment_method["Payment_type"] as? String ?? "")
        
        if let type = self.dict_payment_method["Payment_type"] as? String, !type.isEmpty{
            
            switch type{
                
            case "Braintree" :
                self.Get_braintree_Token()
                break
                
            case "Worldpay" :
                let wp: Worldpay = Worldpay.sharedInstance()
                
                wp.clientKey = self.dict_payment_method["private_key"] as? String ?? ""
                //wp.clientKey = "T_C_0444e970-eb09-492b-b210-e984417581b1"
                wp.reusable = true
                wp.validationType = WorldpayValidationTypeAdvanced
                
                self.WP_GetCardDetails()
                
                break
                
            case "stripe" :
                STPPaymentConfiguration.shared().publishableKey = self.dict_payment_method["public_key"] as? String ?? ""
                self.STP_GetCardDetails()
                
            default :
                print("Invalid Json")
            }
            
        }else{
            appDelegate.showerror(str: "Unknown server error!")
        }
        
    }
    
    //MARK:- Braintree
    
    func Get_braintree_Token(){
        
        
        if Double(price_fare) == 0{
            appDelegate.showerror(str: "Price is invalid!")
        }
        else{
            
            let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
            
            let parameter  = ["office_name":user_dict["office_name"]!,"type":"generate_token"] as [String : Any]
            
            HUD.show(.progress)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                
                switch response.result {
                case .success:
                    debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["RESULT"] as? String == "OK"{
                        self.show_Braintree_DropIn(clientKey: dict["DATA"]!["token"]! as! String)
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                                // action here
                            }
                            SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                            //                            SCLAlertView().showError(AppName, subTitle: dict["DATA"]?["msg"] as! String, duration: 4)
                        })
                    }
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    })
                    
                }
            })
        }
        
    }
    
    func show_Braintree_DropIn(clientKey:String!) {
        
        let request =  BTDropInRequest()
        let dropInController = BTDropInController(authorization: clientKey!, request: request) { (dropController, dropInResult, error) in
            
            if(error != nil) {
                
                print("ERROR: \(error?.localizedDescription ?? "")")
                dropController.dismiss(animated: true, completion: nil)
            }
            else if(dropInResult?.isCancelled == true) {
                
                print("Canceled")
                dropController.dismiss(animated: true, completion: nil)
            }
            else if let result = dropInResult {
                
                //                let selectedPaymentOption = result.paymentOptionType
                let selectedPaymentMethod = result.paymentMethod
                //                let selectedPaymentMethodIcon = result.paymentIcon
                //                let selectedPaymentMethodDescription = result.paymentDescription
                
                dropController.dismiss(animated: true, completion: nil)
                
                self.Braintree_postNonceToServer(paymentMethodNonce: selectedPaymentMethod!.nonce)
            }
            
        }
        
        self.present(dropInController!, animated: true, completion: nil)
    }
    
    func Braintree_postNonceToServer(paymentMethodNonce : String) {
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payment_method_nonce","job_id":jobDetails["job_id"]!,"payment_method_nonce":paymentMethodNonce,"amount":price_fare] as [String : Any]
        
        HUD.show(.progress)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            debugPrint(response.request?.urlRequest ?? "not given")
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as? String == "OK"{
                    self.navigationController?.popToRootViewController(animated: true)
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]?["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
        
    }
    
    
    //MARK:- WorldPay
    
    func WP_GetCardDetails(){
        
        if Double(price_fare) == 0{
            appDelegate.showerror(str: "Price is invalid!")
        }
        else{
            
            let worldpayCardViewController: WorldpayCardViewController = WorldpayCardViewController(color: #colorLiteral(red: 0.2509803922, green: 0.3058823529, blue: 0.4117647059, alpha: 1), loadingTheme: CardDetailsLoadingThemeWhite)
            /*
             worldpayCardViewController.setSaveButtonTapBlockWithSuccess({(response) in
             // save the token,name,cardType and maskedCardNumber the way you like.
             debugPrint(response ?? "")
             debugPrint(response?["token"] ?? "")
             if let dict = response as? [String:Any]{
             if let token = dict["token"] as? String ,let name = dict["paymentMethod"] as? [String:Any]{
             self.WP_sendTokenToServer(token: token, cardName: name["name"] as! String)
             }
             }
             
             }, failure: {(response,errors)  in
             //handle the error
             print(response ?? "")
             appDelegate.showerror(str: "Server Technical Error.")
             })*/
            worldpayCardViewController.setSaveButtonTapBlockWithSuccess({ (response) in
                debugPrint(response ?? "")
                if let dict = response as? [String:Any]{
                    if let token = dict["token"] as? String ,let name = dict["paymentMethod"] as? [String:Any]{
                        self.WP_sendTokenToServer(token: token, cardName: name["name"] as! String)
                    }
                }
            }, failure: { (res, errors) in
                debugPrint(res ?? "")
                debugPrint(errors ?? "")
                let error = errors![0]
                debugPrint(error)
                appDelegate.showerror(str: "Technical server error.")
            })
            
            self.present(worldpayCardViewController, animated: true, completion: nil)
        }
    }
    
    func WP_sendTokenToServer(token:String!,cardName:String!){
        
        var amount:Double = 0
        
        if  Double(price_fare) != 0{
            amount = Double(price_fare)! * 100
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"username":user_dict["user_name"] as! String,"email":user_dict["user_name"]as! String,"type":"worldpay_payment_nonce","token":token!, "amount":amount,"name":cardName!,"currencyCode":Locale.current.currencyCode ?? "GBP","job_id":jobDetails["job_id"]!,"billingAddress":"Bill Test","orderDescription":"Test Description","customerOrderCode":jobDetails["job_id"]!] as [String : Any]
        
        print(parameter)
        
        HUD.show(.progress)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]?["paymentStatus"] as? String == "SUCCESS"{
                    
                    DispatchQueue.main.async {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
        
    }
    
    
    //MARK:- Stripe
    
    func STP_GetCardDetails(){
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        navigationController?.pushViewController(addCardViewController, animated: true)
    }
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        debugPrint(token)
        debugPrint(token.tokenId)
        //navigationController?.popToRootViewController(animated: true)
        self.send_stripe_token(token: token.tokenId)
    }
    
    func send_stripe_token(token:String!){
        
        var amount:Double = 0
        
        if  Double(price_fare) != 0{
            amount = Double(price_fare)!
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"username":user_dict["user_name"] as! String,"email":user_dict["user_name"]as! String,"type":"stripe_payment","stripeToken":token!, "amount":amount,"currencyCode":Locale.current.currencyCode ?? "GBP","job_id":jobDetails["job_id"]!,"billingAddress":"Bill Test","description":"Test Description"] as [String : Any]
        
        print(parameter)
        
        HUD.show(.progress)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]?["paymentStatus"] as? String == "Payment Completed Successfully"{
                    
                    DispatchQueue.main.async {
                        DispatchQueue.main.async(execute: {
                            let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                                // action here
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            SCLAlertView().showSuccess(AppName, subTitle:"Payment received successfully!",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        })
                        
                    }
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["paymentStatus"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
