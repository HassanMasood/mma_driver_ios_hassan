//
//  JobProcessVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 26/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import SafariServices
import Messages
import MessageUI
import Alamofire
import PKHUD
import SCLAlertView
import GoogleMaps
import CoreLocation
import BraintreeDropIn
import Braintree
import Stripe

class JobProcessVC: CommonVC ,SFSafariViewControllerDelegate,MFMessageComposeViewControllerDelegate, UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,YPSignatureDelegate,CompleteMeterDelegate,CLLocationManagerDelegate,STPAddCardViewControllerDelegate{
    
    @IBOutlet weak var header_view : UIView!
    @IBOutlet weak var cons_header_height:NSLayoutConstraint!
    @IBOutlet weak var const_Tbl_Address_Height:NSLayoutConstraint!
    
    @IBOutlet weak var btnNoShow : UIButton!
    @IBOutlet weak var btnSoonClear : UIButton!
    
    //----------------------------------------
    @IBOutlet weak var map_View : GMSMapView!
    
    @IBOutlet weak var btnNavMet : UIButton!
    @IBOutlet weak var btnCancelJob : UIButton!
    @IBOutlet weak var btnCall : UIButton!
    @IBOutlet weak var btnSaveInfo : UIButton!
    
    @IBOutlet weak var lblNName : UILabel!
    @IBOutlet weak var lblNJobId : UILabel!
    @IBOutlet weak var lblNPickup : UILabel!
    @IBOutlet weak var lblNDropoff : UILabel!
    @IBOutlet weak var lblNPayType : UILabel!
    @IBOutlet weak var lblNVehicleType : UILabel!
    
    //----------------------------------------
    
    @IBOutlet weak var sclView : UIScrollView!
    @IBOutlet weak var sclViewInfo : UIScrollView!
    @IBOutlet weak var tblViewAddress : UITableView!
    
    @IBOutlet weak var lbl_date : UILabel!
    @IBOutlet weak var lbl_time : UILabel!
    @IBOutlet weak var lbl_ref : UILabel!
    @IBOutlet weak var lbl_name : UILabel!
    @IBOutlet weak var lbl_bookedBy : UILabel!
    // @IBOutlet weak var lbl_caller : UILabel!
    @IBOutlet weak var view_caller: UIView!
    
    @IBOutlet weak var lbl_otherRef : UILabel!
    //@IBOutlet weak var lbl_flight : UILabel!
    @IBOutlet weak var btn_flight : UIButton!
    @IBOutlet weak var stack_flight : UIStackView!
    @IBOutlet weak var stack_othersRef : UIStackView!
    
    
    @IBOutlet weak var txt_fare : CustomTextField!
    @IBOutlet weak var lbl_fare : UILabel!
    //@IBOutlet weak var lbl_paymentType : UILabel!
    @IBOutlet weak var btn_paymentType : UIButton!
    @IBOutlet weak var btnMeet : UIButton!
    @IBOutlet weak var btnInfo : UIButton!
    
    @IBOutlet weak var payTypePicker: UIPickerView!
    @IBOutlet weak var view_payTypePicker: UIView!
    
    @IBOutlet weak var stack_sms: UIStackView!
//    @IBOutlet weak var cons_bottom_sms : NSLayoutConstraint!
    
    @IBOutlet weak var stack_address: UIStackView!
    @IBOutlet weak var stack_others : UIStackView!
    
    @IBOutlet weak var const_BtnMeet_Height : NSLayoutConstraint!
//    @IBOutlet weak var const_ViewMore_Bottom : NSLayoutConstraint!
    
    weak var txt_car_park : CustomTextField!
    weak var txt_wttime : CustomTextField!
    weak var txt_wttime_val : CustomTextField!
    weak var txt_extras : CustomTextField!
    weak var txt_tollcharge : CustomTextField!
    weak var txt_discount : CustomTextField!
    weak var txtFareValue : CustomTextField!
    
    @IBOutlet weak var lbl_cust_group_name : UILabel!
    
    // @IBOutlet weak var img_bottom_action : UIImageView!
    @IBOutlet weak var btnMainStatus : UIButton!
    
    @IBOutlet weak var view_signature : UIView!
    @IBOutlet weak var view_draw_signature : YPDrawSignatureView!
    
    //@IBOutlet weak var img_sig : UIImageView!
    
    @IBOutlet weak var view_meter : UIView!
    // @IBOutlet weak var btn_meter : UIButton!
    
    var jobDetails : [String:AnyObject]!
    
    let locationManager = CLLocationManager()
    
    var start_location : CLLocation?
    var final_location : CLLocation?
    var latest_location : CLLocation?
    
    var total_miles : Double = 0
    
    var price_fare  = "0"
    var payment_method = "Cash"
    
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var viewTransparent: UIView!
    //    @IBOutlet weak var lblCustName : UILabel!
    
    @IBOutlet weak var view_payment_info : UIView!
    @IBOutlet weak var lbl_payment_price : UILabel!
    @IBOutlet weak var img_cash_payment : UIImageView!
    @IBOutlet weak var img_card_payment : UIImageView!
    
    @IBOutlet weak var button_receive_payments : UIButton!
    
    @IBOutlet weak var view_Price : UIView!
    @IBOutlet weak var txt_price : CustomTextField!
    
    var arrAddresses: [String] = [String]()
    var dispatchState: Int = 0
    var totalViaCount : Int = 0
    
    var tapGesture: UITapGestureRecognizer?
    
    var dict_payment_method = [String:AnyObject]()
    
    struct ButtonTag {
        static let viewSignature = 2111
        static let startMeter = 2112
    }
    
    //----------------------------------------
    
    class func viewController()-> JobProcessVC {
        return UIStoryboard.init(name: "Second", bundle: nil).instantiateViewController(withIdentifier: "JobProcessVC") as! JobProcessVC
    }
    
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupData()
        self.setupNewData()
        self.setupLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Refresh_JobDetailsAPI), name: NSNotification.Name(rawValue: "Update_job"), object: nil)
    }
    
    //----------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    //MARK:- Setup Data
    func setupData()  {
        
        //        if self.tapGesture == nil {
        //            self.tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapgestureTapped(_:)))
        //        }
        //
        //        self.tapGesture?.numberOfTapsRequired = 1
        //        self.viewTransparent.addGestureRecognizer(self.tapGesture!)
        //
        //        self.viewPopUp.layer.cornerRadius = 10
        //        self.viewPopUp.layer.borderWidth = 4
        //        self.viewPopUp.layer.borderColor = UIColor.green.cgColor
        
        
        //btnMainStatus.layer.borderColor = #colorLiteral(red: 0.9647058824, green: 0.9333333333, blue: 0, alpha: 1)
        btnCancelJob.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnNoShow.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        // print(jobDetails)
        
        var dispatch : Int!
        if let  status = jobDetails["dispatch"] as? String ,status.isEmpty{
            dispatch = 0
        }
        else if let status = jobDetails["status"] as? String, status == "Completed"{
            dispatch = 2
            if let status = jobDetails["source"] as? String, status != "CustomerApp"{
                self.button_receive_payments.isHidden = false
            }
        }
        else{
            dispatch = jobDetails["dispatch"] as? Int
        }
        
        const_BtnMeet_Height.constant = 0
        self.btnMeet.isHidden = true
        
        self.btnCancelJob.setImage(#imageLiteral(resourceName: "signature"), for: .normal)
        self.btnCancelJob.tag = ButtonTag.viewSignature
        btnCancelJob.isHidden = true
        
        self.btnSoonClear.isHidden = true
        
        self.dispatchState = dispatch
        
        switch dispatch {
        case 5:
            
            if jobDetails["job_status"] as! String == "New"{ //Accept
                btnNoShow.isHidden = true
                //  img_bottom_action.image = nil
                btnMainStatus.setTitle("Accept", for: .normal)
                btnMainStatus.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                btnMainStatus.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            }
            else{ //ON Route
                btnNoShow.isHidden = true
                btnMainStatus.setTitle("ON ROUTE", for: .normal)
                btnMainStatus.backgroundColor = #colorLiteral(red: 1, green: 0.4705882353, blue: 0, alpha: 1)
                btnMainStatus.setTitleColor(#colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1), for: .normal)
            }
            
        case 4: //Arrived
            btnNoShow.isHidden = true
            btnMainStatus.setTitle("ARRIVED", for: .normal)
            btnMainStatus.backgroundColor = #colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1)
            btnMainStatus.setTitleColor(#colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1), for: .normal)
            
        case 0: //POB
            btnNoShow.isHidden = false
            btnMainStatus.setTitle("PICKED UP", for: .normal)
            btnMainStatus.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
            btnMainStatus.setTitleColor(#colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1), for: .normal)
            if jobDetails["meter"] as! String == "2" || jobDetails["meter"] as! String == "3" {
//                view_meter.isHidden = false
                self.btnCancelJob.isHidden = false
                self.btnCancelJob.setImage(#imageLiteral(resourceName: "meterIcon"), for: .normal)
                self.btnCancelJob.tag = ButtonTag.startMeter
            }
            
            if let passName = self.jobDetails["caller"] as? String, passName != "" {
                self.btnMeet.isHidden = false
                const_BtnMeet_Height.constant = 40
            }else{
                self.btnMeet.isHidden = true
                const_BtnMeet_Height.constant = 0
            }
            
        case 2: //Complete

            btnNoShow.isHidden = true
            self.btnSoonClear.isHidden = false
            btnMainStatus.setTitle("JOB COMPLETE", for: .normal)
            btnMainStatus.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            btnMainStatus.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
//            self.view_signature.isHidden = false
            self.btnCancelJob.isHidden = false
            //            view_meter.isHidden = true
            
            if let status = jobDetails["source"] as? String, status != "CustomerApp"{
                self.button_receive_payments.isHidden = false
            }
            
        default:
            print("Invalid Status")
        }
        
        btnMainStatus.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        btnMainStatus.layer.borderColor = #colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1)
        
        lbl_date.text = jobDetails["job_date"] as? String
        lbl_time.text = jobDetails["job_time"] as? String
        // lbl_ref.text = String(describing: jobDetails["job_id"]!)
        let accName = jobDetails["name"] as? String ?? ""
        lbl_name.text = "Account : \(accName)"
        
        //        self.lblCustName.text = jobDetails["caller"] as? String
        //lbl_bookedBy.text = jobDetails["caller"] as? String
        //lbl_caller.text = jobDetails["otherref"] as? String
        // lbl_caller.text = jobDetails["telephone"] as? String
        lbl_otherRef.text = jobDetails["telephone"] as? String
        
        
        //lbl_fare.text = "£ " + String(describing: jobDetails["fare"]!)
        lbl_fare.text = jobDetails["fare"] as? String
        txt_fare.text = jobDetails["fare"] as? String
        price_fare = lbl_fare.text!
        //lbl_paymentType.text = (jobDetails["payment_type"] as? String)?.uppercased()
        if let pay = jobDetails["payment_type"] as? String{
            btn_paymentType.setTitle(pay.uppercased(), for: .normal)
        }
        if jobDetails["meter"] as! String == "1" || jobDetails["meter"] as! String == "3" {
            lbl_fare.isHidden = true
            txt_fare.isHidden = false
        }else{
            lbl_fare.isHidden = false
            txt_fare.isHidden = true
        }
        
        //lbl_mobile.text = String(describing: jobDetails["mobile"]!)
        if let flight = jobDetails["flight_no"] as? String, !flight.isEmpty{
            let str_attr = NSAttributedString(string: flight, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor : UIColor.white])
            btn_flight.setAttributedTitle(str_attr, for: .normal)
        }else{
            stack_flight.isHidden = true
        }
        
        
        if let caller = jobDetails["caller"] as? String, caller.isEmpty{
            view_caller.isHidden = true
        }
        
        if let flight = jobDetails["flight_no"] as? String, flight.isEmpty, let otherref = jobDetails["telephone"] as? String, otherref.isEmpty{
            stack_othersRef.isHidden = true
        }
        
        if let mobile = jobDetails["mobile"] as? String, mobile.isEmpty{
            stack_sms.heightAnchor.constraint(equalToConstant: 0).isActive = true
//            cons_bottom_sms.constant = 0
        }
        
        //self.configAddressStack()
        self.configOthersStack()
        
        self.btnSaveInfo.backgroundColor = self.btnMainStatus.backgroundColor
        
        lbl_cust_group_name.text = jobDetails["cust_group_name"] as? String
        view_draw_signature.delegate = self
        
        self.arrAddresses.removeAll()
        
        self.arrAddresses = ["pickup"]
        
        if let viaCount = self.jobDetails["viacount"] as? Int, viaCount != 0 {
            self.totalViaCount = viaCount
            for index in 1...viaCount {
                let viaAddress = "via_address\(index)"
                self.arrAddresses.append(viaAddress)
            }
        }
        
        self.arrAddresses.append("destination")
        
        if let strNotes = self.jobDetails["note"] as? String, !strNotes.isEmptyAfterTrim {
            self.arrAddresses.append("note")
        }
        
        self.tblViewAddress.reloadData()
        
        self.tblViewAddress.layoutIfNeeded()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.const_Tbl_Address_Height.constant = self.tblViewAddress.contentSize.height + 30
            self.view.layoutIfNeeded()
            
        }
    }
    
    //----------------------------------------
    
    func setupNewData() {
        
        let jobId = String(describing: jobDetails["job_id"]!)
        self.lblNJobId.text = "JobId : \(jobId)"
        
        let passenger = jobDetails["caller"] as? String ?? ""
        self.lblNName.text = passenger
        
        let pickup = jobDetails["pickup"] as? String ?? ""
        self.lblNPickup.text = "Pickup address : \(pickup)"
        
        let drop = jobDetails["destination"] as? String ?? ""
        self.lblNDropoff.text = "Destination address : \(drop)"
        
        let vehicleType = jobDetails["vehicle_type"] as? String ?? ""
        self.lblNVehicleType.text = vehicleType
        
//        let pay = jobDetails["payment_type"] as? String ?? ""
//        self.lblNPayType.text = "Payment Type : \(pay)"
    }
    
    //----------------------------------------
    
    func setCurrentLocation() {
        
        if latest_location != nil, let loc = latest_location {
            let camera = GMSCameraPosition.camera(withLatitude: loc.coordinate.latitude,longitude: loc.coordinate.longitude,zoom: 19)
            
            map_View.camera = camera
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    //----------------------------------------
    
    func showAlertForMultipleMobile(with arr: [String]) {
        
        if arr.count > 0 {
            
            let alert = UIAlertController.init(title: AppName, message: nil, preferredStyle: .alert)
            
            arr.forEach { (mobile) in
                
                let action = UIAlertAction.init(title: mobile, style: .default, handler: mobileNumberTappedFromAlert)
                
                alert.addAction(action)
            }
            
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(actionCancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //----------------------------------------
    
    func mobileNumberTappedFromAlert(_ action: UIAlertAction) {
        
        if let mobile = action.title {
            self.callToMobileNumber(mobile: mobile)
        }
    }
    
    //----------------------------------------
    
    func callToMobileNumber(mobile: String) {
        
        if let url = URL(string: "telprompt://" + mobile) {
            
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
    }
    
    //----------------------------------------
    
    func hideShowPassengerDetails(inCell cell: JobAddressCell, isHide: Bool, viaNo: Int = 0) {
        cell.const_lblPass_Top.constant = isHide ? 0 : 20
        cell.const_lblPass_Bottom.constant = isHide ? 0 : 20
//        cell.const_lblPass_Height.constant = isHide ? 0 : 20
        cell.const_Stack_Height.constant = isHide ? 0 : 35
        cell.const_btnMobile_Height.constant = isHide ? 0 : 30
        cell.lblPassenger.isHidden = isHide
        cell.btnMobile.isHidden = isHide
        cell.stackStatus.isHidden = isHide
        cell.btnArrived.isHidden = isHide
        cell.btnPickedUp.isHidden = isHide
        cell.btnArrived.isHidden = isHide
        
        if isHide == false {
            let isPickedUp = self.isViaPickedNoShow(viaNo: viaNo)
            let isArrived = self.isViaArrived(viaNo: viaNo)
            if isPickedUp {
                cell.stackStatus.isHidden = true
                cell.const_Stack_Height.constant = 0
            }else if isArrived {
                cell.btnArrived.isHidden = true
            }
        }
        
    }
    
    //----------------------------------------
    
    func isViaPickedNoShow(viaNo: Int)-> Bool {
        
        let pickKey = (self.jobDetails["job_id"] as? String ?? "") + kPickArrivedVia

        var dictPickArrived = UserDefaults.standard.value(forKey: pickKey) as? [String: Any] ?? [:]
        
        if dictPickArrived.count > 0 {
            let picked = dictPickArrived[kPickNoShow] as? Int ?? 0
            if picked != 0 && viaNo <= picked {
                print("Via \(viaNo) picked...!!")
                return true
            }
        }
        print("Via \(viaNo) not picked...!!")
        return false
    }
    
    //----------------------------------------
    
    func isViaArrived(viaNo: Int)-> Bool {
        let pickKey = (self.jobDetails["job_id"] as? String ?? "") + kPickArrivedVia
        
        var dictPickArrived = UserDefaults.standard.value(forKey: pickKey) as? [String: Any] ?? [:]
        
        if dictPickArrived.count > 0 {
            let arrived = dictPickArrived[kArrived] as? Int ?? 0
            if arrived != 0 && viaNo <= arrived {
                print("Via \(viaNo) arrived...!!")
                return true
            }
        }
        print("Via \(viaNo) not arrived...!!")
        return false
    }
    
    //----------------------------------------
    
    func isViaReached(viaNo: String)-> Bool {
        
        let jobId = self.jobDetails["job_id"] as? String ?? ""
        
        let reachedVia = UserDefaults.standard.value(forKey: jobId) as? String ?? ""
        
        if reachedVia != "" {
            
            let arr = reachedVia.components(separatedBy: ",")
            
            if arr.contains(viaNo) {
                return true
            }
        }
        
        return false
    }
    
    //----------------------------------------
    
    func isViaSelectedOrNot()-> Bool {
        
        let jobId = self.jobDetails["job_id"] as? String ?? ""
        
        if let totalVia = self.jobDetails["viacount"] as? Int, totalVia != 0 {
            
            let reachedVia = UserDefaults.standard.value(forKey: jobId) as? String ?? ""
            
            let arr = reachedVia.components(separatedBy: ",")
            
            let lastVia = "v\(totalVia)"
            
            let secondLastVia = "v\(totalVia - 1)"
            
            if arr.contains(lastVia) || arr.contains(secondLastVia) {
                
                if !arr.contains(lastVia) {
                    self.callWSToReachVia(viaNumber: lastVia)
                }
                return true
            }
            
            return false
        }
        
        return true
    }
    
    //----------------------------------------
    
    func isValidViaToSelect(viaNo: Int) -> (Bool, Int) {
        
        let pickKey = (self.jobDetails["job_id"] as? String ?? "") + kPickArrivedVia
        
        var dictPickArrived = UserDefaults.standard.value(forKey: pickKey) as? [String: Any] ?? [:]
        
        if dictPickArrived.count > 0 {
            
            let picked = dictPickArrived[kPickNoShow] as? Int ?? 0
            
            let arrived = dictPickArrived[kArrived] as? Int ?? 0
            
            if (viaNo - 1) == picked || (viaNo - 1) == arrived {
                if (viaNo - 1) == arrived && (viaNo - 1) != picked {
                    
                    if viaNo == arrived {
                        return(true, 0)
                    }else{
                        return (false, arrived)
                    }
                    
                }else{
                    return (true, 0)
                }
                
            }else if picked != 0 || arrived != 0 {
                if picked >= arrived {
                    return (false, picked + 1)
                }else{
                    return (false, arrived)
                }
            }
        }else if viaNo == 1 {
            return (true, 0)
        }else{
            return (false, 1)
        }
        
        return (false, 0)
    }
    
    //----------------------------------------
    
    func updateArrivedAndPickedUp(strVia: String, isArrived: Bool) {
        
        let viaNo = Int(String(strVia.last!))
        
        let pickKey = (self.jobDetails["job_id"] as? String ?? "") + kPickArrivedVia

        var dictPickArrived = UserDefaults.standard.value(forKey: pickKey) as? [String: Any] ?? [:]
        
        if isArrived {
            dictPickArrived[kArrived] = viaNo
        }else{
            dictPickArrived[kPickNoShow] = viaNo
        }
        
        UserDefaults.standard.set(dictPickArrived, forKey: pickKey)
        
        UserDefaults.standard.synchronize()
    }
    
    //----------------------------------------
    
    func updateViaCount(viaNo: String) {
        
        let jobId = self.jobDetails["job_id"] as? String ?? ""
        
        var reachedVia = UserDefaults.standard.value(forKey: jobId) as? String ?? ""
        
        if reachedVia == "" {
            
            reachedVia = viaNo
            
        }else{
            
            let arr = reachedVia.components(separatedBy: ",")
            
            if !arr.contains(viaNo) {
               reachedVia = reachedVia + "," + viaNo
            }
        }
        
        UserDefaults.standard.set(reachedVia, forKey: jobId)
        
        UserDefaults.standard.synchronize()
    }
    
    //----------------------------------------
    
    @objc func tapgestureTapped(_ sender: UITapGestureRecognizer) {
        
        self.addAnimationToView(to: self.viewPopUp, isShow: false, completion: { (comp) in
            
        })
        
    }
    
    //MARK:- Setup Current Location
    func setupLocation(){
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if CLLocationManager.authorizationStatus() == .notDetermined{
            //self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.requestAlwaysAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
        self.map_View.isMyLocationEnabled = true
        self.map_View.settings.myLocationButton = false
    }
    
    //----------------------------------------
    
    func addAnimationToView(to viewAmin: UIView, isShow: Bool, completion: @escaping(_ completed: Bool)-> Void) {
        
        viewAmin.isHidden = false
        
        viewAmin.transform = isShow ? CGAffineTransform.init(scaleX: 0.01, y: 0.01) : CGAffineTransform.identity
        
        UIView.animate(withDuration: 0.25, animations: {
            
            viewAmin.transform = isShow ? CGAffineTransform.identity : CGAffineTransform.init(scaleX: 0.01, y: 0.01)
            
        }) { (completed) in
            
            viewAmin.isHidden = !isShow
            
            self.viewTransparent.isHidden = !isShow
            
            self.view.layoutIfNeeded()
            completion(true)
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnCancelOrMeterTapped(_ sender: UIButton) {
        
        if sender.tag == ButtonTag.startMeter {
            
            let meterVC = MeterVC.viewController()
            meterVC.MeterFinishDelegate = self
            meterVC.jobDetails = self.jobDetails
            self.navigationController?.pushViewController(meterVC, animated: true)
            
        }else if sender.tag == ButtonTag.viewSignature {
            
            self.view_signature.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

//            let alert = UIAlertController(title: AppName, message: "Are you sure to cancel this job?", preferredStyle: .alert)
//
//            let yes_action = UIAlertAction(title: "Yes", style: .default) { (yes_action) in
//                self.Cancel_JobAPI()
//            }
//
//            let no_action = UIAlertAction(title: "No", style: .cancel) { (cancel_action) in
//
//            }
//
//            alert.addAction(yes_action)
//            alert.addAction(no_action)
//
//            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnNavTapped(_ sender: UIButton) {
        
        print("Nav button tapped...")
        
        let pickup = jobDetails["pickup"] as? String ?? ""
        
        let drop = jobDetails["destination"] as? String ?? ""
        
        if dispatchState == 2 {
            self.shareLocation(location: drop)
        }else{
            self.shareLocation(location: pickup)
        }
        
    }
    
    //----------------------------------------
    
    @IBAction func btnSoonToClearTapped(_ sender: UIButton) {
        
        self.callWSForSoonToClear()
        
    }
    
    //----------------------------------------
    
    @IBAction func btnCallTapped(_ sender: UIButton) {
        
        let mobile = jobDetails["mobile"] as! String
        
        guard let number = URL(string: "telprompt://" + mobile) else { return }
        
        if UIApplication.shared.canOpenURL(number){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(number)
            }
        } else {
            SCLAlertView().showError(AppName, subTitle: "Phone call is not available on this device.")
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
//        self.const_ViewMore_Bottom.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnMoreCloseTapped(_ sender: UIButton) {
//        self.const_ViewMore_Bottom.constant = -500
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
        self.sclViewInfo.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnCloseInfoTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.sclViewInfo.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnCloseSignatureTapped(_ sender: UIButton) {
        self.view_signature.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    //MARK:- Current Lcoation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let latest_location1 = locations[locations.count - 1]
        
        // latest_location = locations[locations.count - 1]
        
        if let status = jobDetails["source"] as? String, status == "CustomerApp"{
            if start_location == nil {
                start_location = locations[locations.count - 1]
                latest_location = latest_location1
            }
            let distanceBetween: CLLocationDistance = latest_location1.distance(from: latest_location!)
            print("pmy distance %.2f",distanceBetween)
            
            let distance_miles:Double = Double(distanceBetween) *  0.00062137
            
            if final_location == nil{
                total_miles += distance_miles
            }
        }
        
        latest_location = latest_location1
        
        self.setCurrentLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //appDelegate.showerror(str: error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func configAddressStack()  {
        
        stack_address.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let stackHeader = UILabel()
        stackHeader.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        stackHeader.backgroundColor = UIColor.darkGray
        stackHeader.text  = "Route Details"
        stackHeader.textAlignment = .center
        stackHeader.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        //stack_address.addArrangedSubview(stackHeader)
        
        let arr_address = ["pickup","via_address","stop1","stop2","stop3","destination","note"]
        let arr_addressTxt = ["Pickup","Via","Stop1","Stop2","Stop3","Dropoff","Notes"]
        let arr_address_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "road"),#imageLiteral(resourceName: "stop") ,#imageLiteral(resourceName: "stop"),#imageLiteral(resourceName: "stop"), #imageLiteral(resourceName: "destination"),#imageLiteral(resourceName: "note")]
        
        for (index,str) in arr_address.enumerated() {
            
            if let add = jobDetails[str] as?  String , !add.isEmptyAfterTrim{
                
                let sub_stack = UIStackView()
                sub_stack.axis  = .vertical
                sub_stack.distribution  = .fill
                sub_stack.alignment = .fill
                sub_stack.spacing   = 8.0
                
                
                let img_line = UIImageView()
                img_line.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                img_line.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
                sub_stack.addArrangedSubview(img_line)
                
                
                let sub_stack2 = UIStackView()
                sub_stack2.axis  = .horizontal
                sub_stack2.distribution  = .fill
                sub_stack2.alignment = .fill
                sub_stack2.spacing   = 8.0
                sub_stack2.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                sub_stack2.isLayoutMarginsRelativeArrangement = true
                
                let view = UIView()
                view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                
                let img_add = UIImageView()
                //img_add.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
                img_add.contentMode = .scaleAspectFit
                img_add.image = arr_address_icon[index]
                view.addSubview(img_add)
                img_add.widthAnchor.constraint(equalToConstant: 30).isActive = true
                img_add.heightAnchor.constraint(equalToConstant: 30).isActive = true
                img_add.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
                img_add.translatesAutoresizingMaskIntoConstraints = false
                //sub_stack2.addArrangedSubview(img_add)
                
                
                let lbl_title = UILabel()
                lbl_title.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_title.backgroundColor = UIColor.clear
                lbl_title.text  = arr_addressTxt[index]
                lbl_title.textAlignment = .left
                lbl_title.widthAnchor.constraint(equalToConstant: 80).isActive = true
                lbl_title.setContentHuggingPriority(UILayoutPriority(rawValue: 750), for: .horizontal)
                //sub_stack2.addArrangedSubview(lbl_title)
                
                let lbl_add = UILabel()
                lbl_add.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_add.backgroundColor = UIColor.clear
                lbl_add.text  = add
                lbl_add.numberOfLines = 0
                lbl_add.textAlignment = .left
                view.addSubview(lbl_add)
                lbl_add.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
                lbl_add.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
                lbl_add.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
                lbl_add.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
                lbl_add.leadingAnchor.constraint(equalTo: img_add.trailingAnchor, constant: 8).isActive = true
                lbl_add.translatesAutoresizingMaskIntoConstraints = false
                //sub_stack2.addArrangedSubview(lbl_add)
                
                
                img_add.centerYAnchor.constraint(equalTo: lbl_add.centerYAnchor).isActive = true
                
                
                let btn_waze = CustomButton(type: .custom)
                btn_waze.imageView!.contentMode = .scaleAspectFit
                //btn_waze.setImage(#imageLiteral(resourceName: "waze"), for: .normal)
                btn_waze.setImage(#imageLiteral(resourceName: "map_loc"), for: .normal)
                btn_waze.widthAnchor.constraint(equalToConstant: 30).isActive = true
                btn_waze.heightAnchor.constraint(equalToConstant: 30).isActive = true
                btn_waze.valueText = add
                //btn_waze.addTarget(self, action: #selector(navtoWaze(sender:)), for: .touchUpInside)
                btn_waze.addTarget(self, action: #selector(locate_map(sender:)), for: .touchUpInside)
                view.addSubview(btn_waze)
                btn_waze.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
                btn_waze.centerYAnchor.constraint(equalTo: lbl_add.centerYAnchor).isActive = true
                btn_waze.leadingAnchor.constraint(equalTo: lbl_add.trailingAnchor, constant: 8).isActive = true
                btn_waze.translatesAutoresizingMaskIntoConstraints = false
                if lbl_title.text == "Notes"{
                    //btn_waze.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
                    //sub_stack2.addArrangedSubview(btn_waze)
                    btn_waze.widthAnchor.constraint(equalToConstant: 0).isActive = true
                    btn_waze.heightAnchor.constraint(equalToConstant: 0).isActive = true
                }
                
                //sub_stack.addArrangedSubview(sub_stack2)
                sub_stack.addArrangedSubview(view)
                
                stack_address.addArrangedSubview(sub_stack)
                
            }
        }
    }
    
    
    func configOthersStack()  {
        
        stack_others.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let stackHeader = UILabel()
        stackHeader.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        stackHeader.backgroundColor = UIColor.darkGray
        stackHeader.text  = "OTHER DETAILS"
        stackHeader.textAlignment = .center
        stackHeader.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        //stack_others.addArrangedSubview(stackHeader)
        
        let img_line = UIImageView()
        img_line.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        img_line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //stack_others.addArrangedSubview(img_line)
        
        
        let arr_others = ["fare","vehicle_type","luggage","hand_luggage","car_park","wttime","wttime_val","extras","gratuity","vehicle_chair","child_seat","bookingfees","num_of_people","payment_r","no_of_cars","calc_value","incentive_val","toll_charge", "no_of_hours"]
        
        let arr_others_icon = [#imageLiteral(resourceName: "receivePay"),#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "luggage") , #imageLiteral(resourceName: "hand_lugagge")  ,#imageLiteral(resourceName: "car_parking") ,#imageLiteral(resourceName: "sand_clock") ,#imageLiteral(resourceName: "waiting_time") , #imageLiteral(resourceName: "plus") ,#imageLiteral(resourceName: "medal") , #imageLiteral(resourceName: "wheelChair") , #imageLiteral(resourceName: "child_seat") ,#imageLiteral(resourceName: "booking_fees") ,#imageLiteral(resourceName: "people") , #imageLiteral(resourceName: "payment_received") , #imageLiteral(resourceName: "no_of_cars") , #imageLiteral(resourceName: "handshake"), #imageLiteral(resourceName: "gift"), #imageLiteral(resourceName: "tallcharge"), #imageLiteral(resourceName: "clock")]
        
        let arrOthersName = ["Fare","Vehicle type", "Hand bag", "Luggage", "Parking", "Waiting Charge","Waiting Time", "Extras", "Gratuity", "Wheel Chair", "Child seat", "Book Fees", "No. of people", "Payment", "No. of Cars", "Driver Percentage", "Driver incentive", "Toll charge", "No. of hours"]
        
        //        for i in stride(from: 0, to: arr_others.count, by: 3) {
        for i in stride(from: 0, to: arr_others.count, by: 1) {
            
            let str = arr_others[i]
            let icon = arr_others_icon[i]
            
            if (jobDetails["meter"] as! String == "1" || jobDetails["meter"] as! String == "3") && str == "fare" {
                
                self.btnSaveInfo.isHidden = false
                print("Fare unlocked...")
                
            }else{
                
                if str == "fare" {
                    continue
                }
                
                if let _ = self.jobDetails[str] as? NSNull {
                    
                    if self.needToHideField(for: str) == true {
                        continue
                    }
                    
                }
                
                if let strValue = self.jobDetails[str] as? String, (strValue.isEmptyAfterTrim || strValue == "0" || strValue == "0.00") {
                    
                    if self.needToHideField(for: str) == true {
                        continue
                    }
                }
                
                if let intValue = self.jobDetails[str] as? Int, intValue == 0 {
                    
                    if self.needToHideField(for: str) == true {
                        continue
                    }
                    
                }
            }
            
            let sub_stack = UIStackView()
            sub_stack.axis  = .horizontal
            sub_stack.distribution  = .fillEqually
            sub_stack.alignment = .fill
            sub_stack.spacing   = 15.0
            
            
            let sub_stack2 = UIStackView()
            sub_stack2.axis  = .horizontal
            sub_stack2.distribution  = .fill
            sub_stack2.alignment = .fill
            sub_stack2.spacing   = 15.0
            
            let img_addressIcon = UIImageView()
            img_addressIcon.contentMode = .scaleAspectFit
            img_addressIcon.image = icon.withRenderingMode(.alwaysTemplate)
            img_addressIcon.tintColor = .white
            img_addressIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
            img_addressIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
            sub_stack2.addArrangedSubview(img_addressIcon)
            
            
            let lbl_address = UILabel()
            lbl_address.numberOfLines = 0
            lbl_address.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            lbl_address.backgroundColor = UIColor.clear
            
            lbl_address.text  = arrOthersName[i] + " : " + (jobDetails[str] as?  String ?? "")
            
            //                lbl_address.text  = jobDetails[str] as?  String ?? ""
            
            if str == "payment_r" || str == "vehicle_chair" || str == "child_seat"{
                
                if lbl_address.text == arrOthersName[i] + " : " + "yes" {
                    lbl_address.text = "\(arrOthersName[i]) : ✅"
                }else{
                    lbl_address.text = "\(arrOthersName[i]) : ❌"
                }
                
            }
            else if str == "calc_value"{
                //                    lbl_address.text = jobDetails[str] as!  String + " %"
                
                lbl_address.text = arrOthersName[i] + " : " + (jobDetails[str] as!  String) + " %"
            }
            
            lbl_address.adjustsFontSizeToFitWidth = true
            lbl_address.minimumScaleFactor = 0.5
            lbl_address.textAlignment = .left
            
            if jobDetails["meter"] as! String == "1"  || jobDetails["meter"] as! String == "3" || jobDetails["meter"] as! String == "4" {
                
                if str == "car_park" || str == "wttime" || str == "wttime_val" || str == "extras" || str == "toll_charge" || str == "fare" {
                    
                    self.btnSaveInfo.isHidden = false
                    
                    let textfield = CustomTextField()
                    textfield.borderStyle = .roundedRect
                    textfield.keyboardType = .decimalPad
                    textfield.font = UIFont.boldSystemFont(ofSize: 17)
                    textfield.backgroundColor = UIColor.white
                    textfield.adjustsFontSizeToFitWidth = true
                    textfield.LeftPadding = 5
                    textfield.clearButtonMode = .whileEditing
                    switch str{
                    case "car_park" :
                        textfield.text = jobDetails["car_park"] as? String
                        txt_car_park = textfield
                    case "wttime" :
                        textfield.text = jobDetails["wttime"] as? String
                        txt_wttime = textfield
                    case "wttime_val" :
                        textfield.placeholder = "Value in minutes"
                        //                        textfield.keyboardType = .default
                        textfield.text = jobDetails["wttime_val"] as? String
                        txt_wttime_val = textfield
                    //txt_wttime_val.delegate = self
                    case "extras" :
                        textfield.text = jobDetails["extras"] as? String
                        txt_extras = textfield
                        
                    case "toll_charge" :
                        textfield.text = jobDetails["toll_charge"] as? String
                        txt_tollcharge = textfield
                      
                    case "fare" :
                        textfield.text = jobDetails["fare"] as? String
                        txtFareValue = textfield

                        
                    default:
                        txt_extras = textfield
                    }
                    
                    sub_stack2.addArrangedSubview(textfield)
                    img_addressIcon.centerYAnchor.constraint(equalTo: textfield.centerYAnchor).isActive = true
                }else{
                    sub_stack2.addArrangedSubview(lbl_address)
                    img_addressIcon.centerYAnchor.constraint(equalTo: lbl_address.centerYAnchor).isActive = true
                }
            }else{
                sub_stack2.addArrangedSubview(lbl_address)
                img_addressIcon.centerYAnchor.constraint(equalTo: lbl_address.centerYAnchor).isActive = true
            }
            
            sub_stack.addArrangedSubview(sub_stack2)
            //}
            //            }
            
            if sub_stack.arrangedSubviews.count != 0 {
                stack_others.addArrangedSubview(sub_stack)
            }
        }
        
    }
    
    //----------------------------------------
    
    func needToHideField(for str: String)-> Bool {
        
        if jobDetails["meter"] as! String == "1"  || jobDetails["meter"] as! String == "3" || jobDetails["meter"] as! String == "4" {
            if str == "car_park" || str == "wttime" || str == "wttime_val" || str == "extras" || str == "toll_charge" {
                return false
            }else{
                return true
            }
            
        } else{
            return true
        }
    }
    
    //----------------------------------------
    
    //MARK: - Flight
    @IBAction func flight_no(btn:UIButton!){
        //print("flight is \(String(describing: btn.titleLabel!.text))")
        
        if let str = btn.titleLabel?.text{
            
            let actionsheet = UIAlertController(title: AppName, message: "check your flight info", preferredStyle: .actionSheet)
            
            let google = UIAlertAction(title: "Google", style: .default, handler: { (action) in
                //let url = URL(string: "https://www.google.co.uk/search?q=\(str)")
                let url = URL(string: "https://www.google.co.uk/search?q=\(str)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
                let sf = SFSafariViewController(url: url!)
                sf.delegate = self
                self.present(sf, animated: true, completion: {
                    
                })
            })
            
            let flightAware = UIAlertAction(title: "FlightAware", style: .default, handler: { (action) in
                //let url = URL(string: "http://uk.flightaware.com/live/flight/\(str)")
                let url = URL(string: "http://uk.flightaware.com/live/flight/\(str)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
                let sf = SFSafariViewController(url: url!)
                sf.delegate = self
                self.present(sf, animated: true, completion: {
                })
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
                
            })
            
            actionsheet.addAction(google)
            actionsheet.addAction(flightAware)
            actionsheet.addAction(cancel)
            
            self.present(actionsheet, animated: true, completion: {
                
            })
        }
    }
    
    @IBAction func locate_map(sender: CustomButton!){
        
        let alert = UIAlertController(title: AppName, message: "choose map source", preferredStyle: .actionSheet)
        
        let waze_map = UIAlertAction(title: "via Waze", style: .default) { (action) in
            self.navtoWaze(location: sender.valueText)
        }
        
        let google_map = UIAlertAction(title: "via Google maps", style: .default) { (action) in
            self.navtoGoogleMaps(location: sender.valueText)
        }
        
        let apple_map = UIAlertAction(title: "via Apple maps", style: .default) { (action) in
            
            let url = URL(string: "http://maps.apple.com//?q=\(sender.valueText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            if let wurl = url , UIApplication.shared.canOpenURL(wurl){
                UIApplication.shared.open(wurl, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
                })
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(waze_map)
        alert.addAction(google_map)
        alert.addAction(apple_map)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    //----------------------------------------
    
    func shareLocation(location: String) {
        
        let alert = UIAlertController(title: AppName, message: "choose map source", preferredStyle: .actionSheet)
        
        let waze_map = UIAlertAction(title: "via Waze", style: .default) { (action) in
            self.navtoWaze(location: location)
        }
        
        let google_map = UIAlertAction(title: "via Google maps", style: .default) { (action) in
            self.navtoGoogleMaps(location: location)
        }
        
        let apple_map = UIAlertAction(title: "via Apple maps", style: .default) { (action) in
            
            let url = URL(string: "http://maps.apple.com//?q=\(location)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            if let wurl = url , UIApplication.shared.canOpenURL(wurl){
                UIApplication.shared.open(wurl, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
                })
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(waze_map)
        alert.addAction(google_map)
        alert.addAction(apple_map)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    //MARK: - Back Press
    @IBAction func backpress(sender:UIButton!){
        self.dismiss(animated: true) { 
            
        }
    }
    
    
    //MARK:- PayType Change
    @IBAction func changePayType(sender:UIButton!){
        self.payTypePicker.reloadAllComponents()
        self.payTypePicker.selectRow(0, inComponent: 0, animated: false)
        self.view_payTypePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_payTypePicker.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_payTypePicker.alpha = 1.0
            self.view_payTypePicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    //MARK:- YPDRAWSignatureView Delegate
    
    func didStart() {
        sclView.isScrollEnabled = false
    }
    
    func didFinish() {
        sclView.isScrollEnabled = true
    }
    
    //MARK:- Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch row {
        case 0:
            return "Cash"
        case 1:
            return "Card"
        case 2:
            return "Account"
        case 3:
            return "Cheque"
        default:
            return ""
        }
    }
    
    @IBAction func Selected_paymentType(sender:UIBarButtonItem!){
        
        let index = payTypePicker.selectedRow(inComponent: 0)
        
        var SelectedPaymentType = ""
        
        switch index {
        case 0:
            SelectedPaymentType = "Cash"
        case 1:
            SelectedPaymentType = "Card"
        case 2:
            SelectedPaymentType = "Account"
        case 3:
            SelectedPaymentType = "Cheque"
        default:
            SelectedPaymentType = ""
        }
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"payment_method_change","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "payment_type": SelectedPaymentType] as [String : Any]
        //print(parameter)
        HUD.show(.progress)
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                //let arr = dict["DATA"] as! [String:AnyObject]
                if dict["DATA"]!["msg"] as! String ==  "Payment type changed successfully."{
                    
                    DispatchQueue.main.async(execute: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        self.btn_paymentType.setTitle(SelectedPaymentType, for: .normal)
                        SCLAlertView().showSuccess(AppName, subTitle: "Payment type changed successfully.")
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_payTypePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_payTypePicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
        
    }
    
    @IBAction func closePaymentType(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_payTypePicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_payTypePicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    //MARK:- Phone Call
    @IBAction func makePhonecall() {
        
        let mobile = jobDetails["mobile"] as! String
        
        guard let number = URL(string: "telprompt://" + mobile) else { return }
        
        if UIApplication.shared.canOpenURL(number){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(number)
            }
        }
        else{
            SCLAlertView().showError(AppName, subTitle: "Phone call is not available on this device.")
        }
    }
    
    //MARK:- Location
    
    @IBAction func sendManualLocation(){
        
        let alert = UIAlertController(title: AppName, message: "Share location", preferredStyle: .actionSheet)
        
        let sms_action = UIAlertAction(title: "Via SMS", style: .default) { (action) in
            self.getSMSLink()
        }
        
        let system_action = UIAlertAction(title: "Via System", style: .default) { (action) in
            if let loc = self.locationManager.location{
                self.sendManualLocationToServer(loc: loc)
            }else{
                SCLAlertView().showError(AppName, subTitle: "No Location Found!")
            }
        }
        
        let cancel_action = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(sms_action)
        alert.addAction(system_action)
        alert.addAction(cancel_action)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    
    func sendManualLocationToServer(loc:CLLocation!){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        //let dateStr = Date().stringWithFormatter(dateFormat: "dd MMM yyyy HH:mm")
        //let dateStr = NSDate.toString(date: NSDate(), format: "dd MMM yyyy HH:mm:ss a")
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"update_driver_loc", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"latitude":"\(loc.coordinate.latitude)","longitude":"\(loc.coordinate.longitude)"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                DispatchQueue.main.async(execute: {
                    if let data = dict["DATA"] as? String,data == "Current Location Updated" {
                        SCLAlertView().showSuccess(AppName, subTitle: "Location sent successfully!")
                    }
                })
                
                
            case .failure(let error):
                print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        
    }
    
    
    func getSMSLink(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        
        if self.locationManager.location == nil{
            SCLAlertView().showError(AppName, subTitle: "No Location Found!")
            return
        }
        
        let loc = locationManager.location!
        //let dateStr = Date().stringWithFormatter(dateFormat: "dd MMM yyyy HH:mm")
        //let dateStr = NSDate.toString(date: NSDate(), format: "dd MMM yyyy HH:mm:ss a")
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"return_link", "office_name":user_dict["office_name"]!,"latitude":"\(loc.coordinate.latitude)","longitude":"\(loc.coordinate.longitude)"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                DispatchQueue.main.async(execute: {
                    if let data = dict["DATA"] as? String{
                        self.makeSMS(urlStr: data)
                    }
                })
                
            case .failure(let error):
                print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
    }
    
    func makeSMS(urlStr : String!){
        
        if MFMessageComposeViewController.canSendText(){
            
            let message = MFMessageComposeViewController()
            message.messageComposeDelegate = self
            //message.recipients = [lbl_sms.text!]
            message.recipients =  [jobDetails["mobile"] as! String]
            message.subject = AppName
            message.body = "My Current location is: \(urlStr!)"
            self.present(message, animated: true, completion: {
                
            })
        }
        else{
            appDelegate.showerror(str: "Can not able to send message.")
        }
    }
    
    //MARK:- SMS Button
    /*
     @IBAction func makeSMS(sender:UIButton!){
     
     if MFMessageComposeViewController.canSendText(){
     
     let message = MFMessageComposeViewController()
     message.messageComposeDelegate = self
     //message.recipients = [sender.titleLabel!.text!]
     message.recipients =  [jobDetails["mobile"] as! String]
     message.subject = AppName
     self.present(message, animated: true, completion: {
     
     })
     }
     else{
     appDelegate.showerror(str: "Can not able to send message.")
     }
     }*/
    
    
    @IBAction func makeSMS(sender:UIButton!){
        
        var type = "thank_sms"
        switch sender.tag {
        case 1:
            type = "inform_sms"
        case 2:
            type = "thank_sms"
        case 3:
            type = "enquire_sms"
        default:
            type = "inform_sms"
        }
        
        var bodyStr = ""
        
        if appDelegate.reachability.connection != .unavailable {
            
            let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
            
            let parameter  = ["type":"default_custom_sms", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"job_id":jobDetails["job_id"]!,"id":type] as [String : Any]
            // print(parameter)
            
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL, method: .get, parameters: parameter, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                HUD.hide()
                print(response)
                switch response.result {
                case .success:
                    // debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    let arr  = dict["DATA"] as! [[String:AnyObject]]
                    if let msg = arr[0]["msg"] as? String{
                        bodyStr = msg
                        
                        DispatchQueue.main.async(execute: {
                            self.sendMessage(bodyStr: bodyStr)
                        })
                        
                    }
                    
                case .failure(let error):
                    print(error)
                    //                    DispatchQueue.main.async(execute: {
                    //                        //self.tbl.isHidden = true
                    //                        appDelegate.showerror(str: error.localizedDescription)
                    //                    })
                }
            }
            
        }
        else{
            self.sendMessage(bodyStr: bodyStr)
        }
        
    }
    
    func sendMessage(bodyStr : String!){
        
        if MFMessageComposeViewController.canSendText(){
            
            let message = MFMessageComposeViewController()
            message.messageComposeDelegate = self
            //message.recipients = [sender.titleLabel!.text!]
            message.recipients =  [jobDetails["mobile"] as! String]
            message.subject = AppName
            message.body = bodyStr
            self.present(message, animated: true, completion: {
                
            })
        }
        else{
            appDelegate.showerror(str: "Can not able to send message.")
        }
    }
    
    
    //MARK:- Message Delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result {
        case .sent:
            SCLAlertView().showSuccess(AppName, subTitle: "Message Sent successfully.")
        case .cancelled:
            appDelegate.showerror(str: "Message Cancelled.")
            
        case .failed:
            appDelegate.showerror(str: "Message failed.")
        }
        controller.dismiss(animated: true) {
            
        }
    }
    
    //MARK:- Meter Button
    @IBAction func show_meter(){
        //self.ChangeJob_statusAPI(dispatch: 2)
        self.performSegue(withIdentifier: "ShowMeter", sender: self)
    }
    
    func meterFinished() {
        self.Refresh_JobDetailsAPI()
    }
    
    //MARK:-  DB Operations
    
    
    func deleteJobFromDb(){
        
        let predicate = NSPredicate(format: "job_id == %@", jobDetails["job_id"] as! String)
        let jobs = DBHelper.sharedInstance.featchFromEntityWithPredicate(Entity: "Jobs", predicate: predicate)
        
        if let arr = jobs.arr as? [Jobs] , !arr.isEmpty{
            let job = arr.last
            appDelegate.persistentContainer.viewContext.delete(job!)
            appDelegate.saveContext()
        }
    }
    
    func UpdateJobDb(){
        
        let job = DBHelper.sharedInstance.checkDuplication(Entity: "Jobs", predicate_str: "job_id", ID: jobDetails["job_id"] as! String) 
        
        if job.Error == nil{
            if job.isExists{
                let latestJob = job.arr?.last as! Jobs
                let dispatch_Str = String(describing: jobDetails["dispatch"])
                latestJob.dispatch = Int64(dispatch_Str) ?? 0
                appDelegate.saveContext()
            }
            else{
                DBHelper.sharedInstance.saveAllJobs(arr_jobs: [jobDetails], completion: { (finish) in
                    
                })
            }
            
        }
        
    }
    
    //----------------------------------------
    
    func Cancel_JobAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_cancel","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                let dict2 = dict["DATA"] as! [String:AnyObject]
                if dict2["msg"] as! String ==  "Job Cancelled"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        // self.deleteJobFromDb()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict2["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict2["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    //MARK: - No Show Job
    
    @IBAction func btnNoShowJobTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: AppName, message: "Are you sure to No Show this job?", preferredStyle: .alert)
        
        let yes_action = UIAlertAction(title: "Yes", style: .default) { (yes_action) in
            self.NoShow_JobAPI()
            
        }
        
        let no_action = UIAlertAction(title: "No", style: .cancel) { (cancel_action) in
            
        }
        
        alert.addAction(yes_action)
        alert.addAction(no_action)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func NoShow_JobAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_no_show","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                //let arr = dict["DATA"] as! [String:AnyObject]
                if dict["DATA"]!["msg"] as! String ==  "Job No Showed"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        //self.deleteJobFromDb()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    //MARK:- Button bottom action
    
    /* @IBAction func change_Status(){
     
     var dispatch : NSInteger!
     if let  status = jobDetails["dispatch"] as? String ,status.isEmpty{
     dispatch = 0
     }else{
     dispatch = jobDetails["dispatch"] as! NSInteger
     }
     
     if dispatch == 3{
     //print("make card payment")
     
     ///self.performSegue(withIdentifier: "GotoPayment", sender: self)
     let fare = Double(String(describing: jobDetails["fare"]!))
     if fare == 0{
     SCLAlertView().showError(AppName, subTitle: "Sorry, payment is not avaible when fare is zero.")
     }else{
     self.paymentURL()
     }
     }
     else{
     switch dispatch {
     
     case 5: //Accept
     //dispatch = 4 //On Route
     self.Accept_JobAPI()
     return
     case 4: //on route
     dispatch = 0 //arrived
     case 0: //arrived
     dispatch = 2 //pob
     case 2: //pob
     dispatch = 3 //make payment
     default:
     print("Invalid Input")
     }
     self.ChangeJob_statusAPI(dispatch: dispatch)
     }
     
     }
     */
    
    @IBAction func btnMainStatusTapped(_ sender: UIButton) {
        
        self.changeJobStatus()
    }
    
    //----------------------------------------
    
    func changeJobStatus() {
        
        if jobDetails["meter"] as! String == "1"  || jobDetails["meter"] as! String == "3" {
            self.UpdateFareAPI()
        }
        
        if jobDetails["meter"] as! String == "4" {
            self.UpdateParkingAPI()
        }
        
        var dispatch : NSInteger!
        if let  status = jobDetails["dispatch"] as? String ,status.isEmpty{
            dispatch = 0
        }
        else if let status = jobDetails["status"] as? String, status == "Completed"{
            dispatch = 2
            if let status = jobDetails["source"] as? String, status != "CustomerApp"{
                self.button_receive_payments.isHidden = false
            }
        }else{
            dispatch = jobDetails["dispatch"] as! NSInteger
        }
        
        if dispatch == 2{
            
            final_location = latest_location
            
            if let status = jobDetails["source"] as? String, status == "CustomerApp"{
                
                if Double(price_fare) != 0 {
                    self.show_payment()
                }else if total_miles != 0 {
                    self.SendMilesAPI()
                }else{
                    show_priceView()
                }
            } else{
                self.CompleteJobAPI()
            }
            
        }
        else{
            switch dispatch {
                
            case 5: //Accept
                //dispatch = 4 //On Route
                if jobDetails["job_status"] as! String == "New"{
                    self.Accept_JobAPI()
                    return
                }else{
                    dispatch = 4
                }
                
            case 4: //on route
                dispatch = 0 //arrived
            case 0: //arrived
                dispatch = 2 //pob
            case 2: //pob
                dispatch = 3 //make payment
            default:
                print("Invalid Input")
            }
            self.ChangeJob_statusAPI(dispatch: dispatch)
        }
        
    }
    
    //----------------------------------------
    
    
    func Accept_JobAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_accept","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Accepted"{
                    // self.ChangeJob_statusAPI(dispatch: 4)
                    self.Refresh_JobDetailsAPI()
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    // SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    func ChangeJob_statusAPI(dispatch:Int){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        //let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":dispatch] as [String : Any]
        var parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":dispatch] as [String : Any]
        
        if let loc = latest_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                debugPrint(response.request?.url ?? "")
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Completed"{
                    
                    DispatchQueue.main.async(execute: {
                        /*
                         if (self.navigationController?.viewControllers[1] as? JobsInProgressVC) != nil {
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                         }else{
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_NewList_Jobs"), object: nil)
                         }
                         */
                        
                        self.Refresh_JobDetailsAPI()
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    @objc func CompleteJobAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        var  img_data : Data?
        var  img64Str  = ""
        let img = self.view_draw_signature.getSignature()
        if let imgg = img{
            img_data = imgg.jpegData(compressionQuality: 0)
            img64Str = img_data!.base64EncodedString(options: .lineLength64Characters)
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        //let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":3,"sign_img": img_data ?? ""] as [String : Any]
        // let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":3,"sign_img": img64Str] as [String : Any]
        var parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":3,"sign_img": img64Str] as [String : Any]
        if let loc = latest_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        
        print(parameter)
        
        // APIManager.sharedInstance.sessionManager.request("http://tbmslive.com/taxi_app/WebServices/WSv7P.php",  parameters: parameter).responseJSON(completionHandler: { (response) in
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Completed"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        //self.deleteJobFromDb()
                        
                        //                            if (self.navigationController?.viewControllers[1] as? JobsInProgressVC) != nil {
                        //                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        //                            }else{
                        //                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_NewList_Jobs"), object: nil)
                        //                            }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        
                        //self.navigationController?.popViewController(animated: true)
                        self.btnMainStatus.isEnabled = false
                        self.btnMainStatus.setTitle("Completed", for: .normal)
                        if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                            //self.cons_header_height.constant = 0
                            //self.view.layoutIfNeeded()
                            self.navigationController?.popToRootViewController(animated: true)
                        }else{
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        }
                        self.button_receive_payments.isHidden = false
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    
    func SendMilesAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        if total_miles == 0{
            
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alert = SCLAlertView(appearance: appearance)
            alert.addButton("Call Office", action: {
                self.CallOffice()
            })
            alert.addButton("Cancel", action: {
                
            })
            alert.showNotice(AppName, subTitle: "It seems travel distance is invalid.")
            
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        //let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":dispatch] as [String : Any]
        let parameter  = ["type":"vehicle_mileage_price","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"vehicle_id":jobDetails["vehicle_id"]!,"total_mileage":self.total_miles] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.request?.url)
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if let dict2 = dict["DATA"] as? [String:Any], let price = dict2["price"]as? NSNumber{
                    
                    DispatchQueue.main.async(execute: {
                        let price2 = Double(truncating: price)
                        if price2 == 0{
                            self.show_priceView()
                        }else{
                            self.price_fare = String(describing: price)
                            self.show_payment()
                        }
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:"Server Error",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    @IBAction func btnMeetTapped(_ sender: UIButton) {
        
        let meetVC = MeetGreetVC.viewController()
        meetVC.strPassengerName = self.jobDetails["caller"] as? String ?? ""
        meetVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(meetVC, animated: true, completion: nil)
    }
    
    //----------------------------------------
    
    
    @IBAction func btnMobileTapped(_ sender: UIButton) {
        
        var mobileNumber = self.jobDetails[sender.accessibilityIdentifier ?? ""] as? String ?? ""
        
        //        mobileNumber = "3213213210, 8908908908, 3453453453"
        
        mobileNumber = mobileNumber.replacingOccurrences(of: " ", with: "")
        
        let arr = mobileNumber.components(separatedBy: ",")
        
        if arr.count > 1 {
            
            self.showAlertForMultipleMobile(with: arr)
            
        }else{
            
            self.callToMobileNumber(mobile: mobileNumber)
            
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnViaPickUpTappedTapped(_ sender: UIButton) {
        
        let viaNo = sender.accessibilityIdentifier ?? ""
        
        guard let vNo = Int(String(viaNo.last!)) else{
            return
        }
        
        let (isValid, selectVia) = self.isValidViaToSelect(viaNo: vNo)
        
        if isValid {
            
            self.callWSToChangeViaStatus(status: "pickedup", viaNo: viaNo)
            
        }else{
            
            SCLAlertView().showWarning(AppName, subTitle: "Please select via \(selectVia)", closeButtonTitle: "Ok")
        }
        
    }
    
    //----------------------------------------
    
    @IBAction func btnViaArrivedTapped(_ sender: UIButton) {
        
        let viaNo = sender.accessibilityIdentifier ?? ""
        
        guard let vNo = Int(String(viaNo.last!)) else{
            return
        }
        
        let (isValid, selectVia) = self.isValidViaToSelect(viaNo: vNo)
        
        if isValid {
            
            self.callWSToChangeViaStatus(status: "arrived", viaNo: viaNo)
            
        }else{
            
            SCLAlertView().showWarning(AppName, subTitle: "Please select via \(selectVia)", closeButtonTitle: "Ok")
        }
        
    }
    
    //----------------------------------------
    
    @IBAction func btnViaNoshowTapped(_ sender: UIButton) {
        
        let viaNo = sender.accessibilityIdentifier ?? ""
        
        guard let vNo = Int(String(viaNo.last!)) else{
            return
        }
        
        let (isValid, selectVia) = self.isValidViaToSelect(viaNo: vNo)
        
        if isValid {
            
            self.callWSToChangeViaStatus(status: "no_show", viaNo: viaNo)
            
        }else{
            
            SCLAlertView().showWarning(AppName, subTitle: "Please select via \(selectVia)", closeButtonTitle: "Ok")
        }
        
        //        self.callWSToReachVia(viaNumber: viaNo)
        
    }
    
    //----------------------------------------
    
    @IBAction func btnNavigateToLocationTapped(_ sender: CustomButton) {
        self.locate_map(sender: sender)
    }
    
    //----------------------------------------
    
    
    //MARK:- Call Office
    
    @IBAction func CallOffice(){
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        if let  phonenumber = user_dict["phonenumber"] as? String{
            let arr_num = phonenumber.components(separatedBy: ",")
            
            if arr_num.isEmpty{
                appDelegate.showerror(str: "Contact info is missing")
            }else{
                let url = URL(string: "telprompt://" + arr_num[0])
                
                if UIApplication.shared.canOpenURL(url!){
                    UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
                else{
                    appDelegate.showerror(str: "Phone call is not available.")
                }
            }
        }
        
        
    }
    
    
    @objc func Refresh_JobDetailsAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_loaded","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"device_type":"ios"] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                let arr = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        //self.UpdateJobDb()
                        
                        //                        if (self.navigationController?.viewControllers[1] as? JobsInProgressVC) != nil {
                        //                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        //                        }else{
                        //                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_NewList_Jobs"), object: nil)
                        //                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        if !arr.isEmpty{
                            self.jobDetails = arr[0]
                        }
                        self.setupData()
                    })
                    
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        // SCLAlertView().showError(AppName, subTitle: arr[0]["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:arr[0]["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    func callWSToChangeViaStatus(status: String, viaNo: String) {
        
        if appDelegate.reachability.connection == .none{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"via_status",
                          "office_name":user_dict["office_name"] as? String ?? "",
                          "job_id":jobDetails["job_id"] as? String ?? "",
                          "via_status":status,
                          "via_no":viaNo
                          ] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            print(response.request?.url ?? "")
            switch response.result {
            case .success:
                debugPrint(response.result.value ?? "")
                
                if let dict = response.result.value as? [String:AnyObject], let msg = dict["DATA"] as? String {
                    
                    if msg == "via updated successfully."  {
                        
                        DispatchQueue.main.async {
                            
                            if status == "arrived" {
                                self.updateArrivedAndPickedUp(strVia: viaNo, isArrived: true)
                            }else{
                                self.updateArrivedAndPickedUp(strVia: viaNo, isArrived: false)
                            }
                            
                            self.tblViewAddress.reloadData()
                        }
                        
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("OK") {
                            
                        }
                        alertView.showSuccess(AppName, subTitle: msg)
                        
                    }
                }
                
            case .failure(let error):
                debugPrint(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    func callWSToReachVia(viaNumber: String) {
        
        if appDelegate.reachability.connection == .none{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"via_arrived",
                          "office_name":user_dict["office_name"]!,
                          "job_id":self.jobDetails["job_id"] as? String ?? "",
                          "via_no": viaNumber
                          ] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            print(response.request?.url ?? "")
            switch response.result {
            case .success:
                debugPrint(response.result.value ?? "")
                
                if let dict = response.result.value as? [String:AnyObject], let msg = dict["DATA"] as? String {
                    
                    if msg == "via updated successfully." {
                        
                        DispatchQueue.main.async {
                            
                            self.updateViaCount(viaNo: viaNumber)
                            
                            self.tblViewAddress.reloadData()
                        }
                        
                    }else{
                        
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("OK") {
                            
                        }
                        alertView.showError(msg)

                    }
                }
                
            case .failure(let error):
                debugPrint(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    func callWSForSoonToClear() {
        
        if appDelegate.reachability.connection == .unavailable {
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"driver_soon_clear",                                                               "office_name":user_dict["office_name"]!,
            "driver_id":user_dict["driver_id"]!] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            print(response.request?.url ?? "")
            switch response.result {
            case .success:
                debugPrint(response.result.value ?? "")
                
                if let dict = response.result.value as? [String:AnyObject], let data = dict["DATA"] as? [String:AnyObject] {
                    
                    if let msg = data["msg"] as? String  {
                        
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("OK") {
                            
                        }
                        alertView.showSuccess(AppName, subTitle: msg)

                    }
                }

            case .failure(let error):
                debugPrint(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    //MARK:- Update Fare
    
    @IBAction func btnSaveInfoTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.sclViewInfo.isHidden = true

        self.changeJobStatus()
//        self.UpdateFareAPI(needToShowMessage: true)
    }
    
    //----------------------------------------
    
    func UpdateFareAPI(needToShowMessage: Bool = false) {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        
        var fare = ""
        
        if self.txtFareValue != nil {
            fare = String(format:"%.2f",Float(txtFareValue.text ?? "0") ?? 0)
        }else {
            fare = self.jobDetails["fare"] as? String ?? ""
        }
        
        let parameter  = ["type":"updatefare","job_id":jobDetails["job_id"]!,                                   "office_name":user_dict["office_name"]!,
                          "driver_id":user_dict["driver_id"]!,
                          "fare":fare,
                          "carpark":String(format:"%.2f",Float(txt_car_park.text ?? "0") ?? 0),
                          "extras":String(format:"%.2f",Float(txt_extras.text ?? "0") ?? 0),
                          "toll_charge":String(format:"%.2f",Float(txt_tollcharge.text ?? "0") ?? 0),
                          "waitcharge":String(format:"%.2f",Float(txt_wttime.text ?? "0") ?? 0),
                          "wttime_val":txt_wttime_val.text ?? ""] as [String : Any]
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            switch response.result {
            case .success:
                
                if needToShowMessage == true {
                    guard let dict = response.result.value as? [String:Any] else {
                        return
                    }
                    
                    guard let arr = dict["DATA"] as? [[String:Any]], arr.count > 0 else {
                        return
                    }
                    
                    guard let msg = arr[0]["msg"] as? String else {
                        return
                    }
                    
                    appDelegate.showeSuccess(str: msg)
                    
                }
            case .failure(let error):
                debugPrint(error)
            }
        })
        
    }
    
    //MARK:- Update Parking
    
    func UpdateParkingAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"updatefare","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"carpark":String(format:"%.2f",Float(txt_car_park.text ?? "0") ?? 0),"extras":String(format:"%.2f",Float(txt_extras.text ?? "0") ?? 0),"waitcharge":String(format:"%.2f",Float(txt_wttime.text ?? "0") ?? 0),"wttime_val":txt_wttime_val.text ?? "","toll_charge":String(format:"%.2f",Float(txt_tollcharge.text ?? "0") ?? 0)] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            print(response.request?.url ?? "")
            switch response.result {
            case .success:
                debugPrint(response.result.value ?? "")
            case .failure(let error):
                debugPrint(error)
            }
        })
        
    }
    
    //MARK:- Update Fare if price is zero
    
    @IBAction func UpdateFarePriceAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        let price = Double(txt_price.text ?? "0")
        //        if let price_fare = txt_price.text , !price_fare.isEmptyAfterTrim{
        //            price = Double()
        //        }
        if price == 0 || price == nil{
            SCLAlertView().showError(AppName, subTitle: "Price must not be zero.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        // let parameter  = ["type":"updatefare","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"fare":String(format:"%.2f",Float(txt_price.text ?? "0") ?? 0)] as [String : Any]
        let parameter  = ["type":"updatefareforcustomer","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"fare":String(format:"%.2f",Float(txt_price.text ?? "0") ?? 0)] as [String : Any]
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            print(response.request?.url ?? "")
            switch response.result {
            case .success:
                debugPrint(response.result.value ?? "")
                self.price_fare = self.txt_price.text ?? "0"
                //self.CompleteJobAPI()
                self.cancel_priceView()
                self.show_payment()
            case .failure(let error):
                debugPrint(error)
                SCLAlertView().showError(AppName, subTitle: error.localizedDescription)
            }
        })
        
    }
    
    //MARK:- Price View
    func show_priceView(){
        self.view.endEditing(true)
        view_Price.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        view_Price.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_Price.alpha = 1.0
            self.view_Price.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @IBAction func cancel_priceView(){
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_Price.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_Price.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    //MARK:- Payment View
    
    @IBAction func show_payment(){
        
        if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
            
            lbl_payment_price.text = "Total Payable Amount: \(price_fare)"
            
            self.view.endEditing(true)
            view_payment_info.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            view_payment_info.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.view_payment_info.alpha = 1.0
                self.view_payment_info.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }else{
            self.payment_modeAPI(type: "Card")
            self.payment_methodeAPI()
        }
    }
    
    @IBAction func pay_type(sender:UIButton!){
        
        img_cash_payment.image = sender.tag == 0 ? #imageLiteral(resourceName: "check") : #imageLiteral(resourceName: "uncheck")
        img_card_payment.image = sender.tag == 1 ? #imageLiteral(resourceName: "check") : #imageLiteral(resourceName: "uncheck")
        
        payment_method = sender.tag == 0 ? "Cash" : "Card"
    }
    
    @IBAction func make_payment(){
        
        //if img_cash_payment.image == #imageLiteral(resourceName: "check"){
        if payment_method == "Cash"{
            self.payment_modeAPI(type: "Cash")
            if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                self.CompleteJobAPI()
                // self.payment_methodeAPI()
            }
        }else{
            self.payment_modeAPI(type: "Card")
            
            //self.Get_braintree_Token()
            //self.payment_methodeAPI()
            if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                self.CompleteJobAPI()
                // self.payment_methodeAPI()
            }else{
                self.payment_methodeAPI()
            }
        }
        
        self.cancel_payment()
    }
    
    @IBAction func cancel_payment(){
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_payment_info.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_payment_info.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    
    func payment_modeAPI(type:String!){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payments_buttons","username":user_dict["user_name"]!,"driver_id": user_dict["driver_id"]!,"job_id":jobDetails["job_id"]!,"price":price_fare,"payment_method":type!,"payment_received": type == "Cash" ? "yes" : "no"] as [String : Any]
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                print(dict)
                if dict["RESULT"] as? String == "OK"{
                    DispatchQueue.main.async {
                        
                        //   if type == "Cash"{
                        if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        //                            }else{
                        //                                self.CompleteJobAPI()
                        //                            }
                        //self.CompleteJobAPI()
                        //       }
                    }
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    
    //MARK:- Get Payment Method
    func payment_methodeAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payment_type_check"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                //                let dict = response.result.value as! [String:AnyObject]
                //
                //                print(dict)
                if let dict = response.result.value as? [String:AnyObject] , let dict2 = dict["DATA"] as? [String:AnyObject]{
                    
                    self.dict_payment_method = dict2
                    self.configure_payment()
                    
                    
                }else{
                    appDelegate.showerror(str: "Unknown server error!")
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    func configure_payment(){
        
        debugPrint(self.dict_payment_method["Payment_type"] as? String ?? "")
        
        if let type = self.dict_payment_method["Payment_type"] as? String, !type.isEmpty{
            
            switch type{
                
            case "Braintree" :
                self.Get_braintree_Token()
                break
                
            case "Worldpay" :
                let wp: Worldpay = Worldpay.sharedInstance()
                
                wp.clientKey = self.dict_payment_method["private_key"] as? String ?? ""
                //wp.clientKey = "T_C_0444e970-eb09-492b-b210-e984417581b1"
                wp.reusable = true
                wp.validationType = WorldpayValidationTypeAdvanced
                
                self.WP_GetCardDetails()
                
                break
                
            case "stripe" :
                STPPaymentConfiguration.shared().publishableKey = self.dict_payment_method["public_key"] as? String ?? ""
                self.STP_GetCardDetails()
                
            default :
                print("Invalid Json")
            }
            
        }else{
            appDelegate.showerror(str: "Unknown server error!")
        }
        
    }
    
    //MARK:- Braintree
    
    @IBAction func Get_braintree_Token(){
        
        
        if Double(price_fare) == 0{
            appDelegate.showerror(str: "Price is invalid!")
        }
        else{
            
            let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
            
            let parameter  = ["office_name":user_dict["office_name"]!,"type":"generate_token"] as [String : Any]
            
            HUD.show(.progress)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                
                switch response.result {
                case .success:
                    debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["RESULT"] as? String == "OK"{
                        self.show_Braintree_DropIn(clientKey: dict["DATA"]!["token"]! as! String)
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                                // action here
                            }
                            SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                            //                            SCLAlertView().showError(AppName, subTitle: dict["DATA"]?["msg"] as! String, duration: 4)
                        })
                    }
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    })
                    
                }
            })
        }
        
    }
    
    func show_Braintree_DropIn(clientKey:String!) {
        
        let request =  BTDropInRequest()
        let dropInController = BTDropInController(authorization: clientKey!, request: request) { (dropController, dropInResult, error) in
            
            if(error != nil) {
                
                print("ERROR: \(error?.localizedDescription ?? "")")
                dropController.dismiss(animated: true, completion: nil)
            }
            else if(dropInResult?.isCancelled == true) {
                
                print("Canceled")
                dropController.dismiss(animated: true, completion: nil)
            }
            else if let result = dropInResult {
                
                //                let selectedPaymentOption = result.paymentOptionType
                let selectedPaymentMethod = result.paymentMethod
                //                let selectedPaymentMethodIcon = result.paymentIcon
                //                let selectedPaymentMethodDescription = result.paymentDescription
                
                dropController.dismiss(animated: true, completion: nil)
                
                self.Braintree_postNonceToServer(paymentMethodNonce: selectedPaymentMethod!.nonce)
            }
            
        }
        
        self.present(dropInController!, animated: true, completion: nil)
    }
    
    func Braintree_postNonceToServer(paymentMethodNonce : String) {
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"payment_method_nonce","job_id":jobDetails["job_id"]!,"payment_method_nonce":paymentMethodNonce,"amount":price_fare] as [String : Any]
        
        HUD.show(.progress)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            debugPrint(response.request?.urlRequest ?? "not given")
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as? String == "OK"{
                    //self.navigationController?.popToRootViewController(animated: true)
                    DispatchQueue.main.async(execute: {
                        
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("OK") {
                            //                            if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                            //                                self.navigationController?.popToRootViewController(animated: true)
                            //                            }else{
                            //                                //self.CompleteJobAPI()
                            //                            }
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        alertView.showSuccess(AppName, subTitle: "Payment received successfully!")
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]?["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
        
    }
    
    
    //MARK:- WorldPay
    
    @IBAction func WP_GetCardDetails(){
        
        if Double(price_fare) == 0{
            appDelegate.showerror(str: "Price is invalid!")
        }
        else{
            
            let worldpayCardViewController: WorldpayCardViewController = WorldpayCardViewController(color: #colorLiteral(red: 0.2509803922, green: 0.3058823529, blue: 0.4117647059, alpha: 1), loadingTheme: CardDetailsLoadingThemeWhite)
            /*
             worldpayCardViewController.setSaveButtonTapBlockWithSuccess({(response) in
             // save the token,name,cardType and maskedCardNumber the way you like.
             debugPrint(response ?? "")
             debugPrint(response?["token"] ?? "")
             if let dict = response as? [String:Any]{
             if let token = dict["token"] as? String ,let name = dict["paymentMethod"] as? [String:Any]{
             self.WP_sendTokenToServer(token: token, cardName: name["name"] as! String)
             }
             }
             
             }, failure: {(response,errors)  in
             //handle the error
             print(response ?? "")
             appDelegate.showerror(str: "Server Technical Error.")
             })*/
            worldpayCardViewController.setSaveButtonTapBlockWithSuccess({ (response) in
                debugPrint(response ?? "")
                if let dict = response as? [String:Any]{
                    if let token = dict["token"] as? String ,let name = dict["paymentMethod"] as? [String:Any]{
                        self.WP_sendTokenToServer(token: token, cardName: name["name"] as! String)
                    }
                }
            }, failure: { (res, errors) in
                debugPrint(res ?? "")
                debugPrint(errors ?? "")
                let error = errors![0]
                debugPrint(error)
                appDelegate.showerror(str: "Technical server error.")
            })
            
            self.present(worldpayCardViewController, animated: true, completion: nil)
        }
    }
    
    func WP_sendTokenToServer(token:String!,cardName:String!){
        
        var amount:Double = 0
        
        if  Double(price_fare) != 0{
            amount = Double(price_fare)! * 100
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"username":user_dict["user_name"] as! String,"email":user_dict["user_name"]as! String,"type":"worldpay_payment_nonce","token":token ?? "", "amount":amount,"name":cardName!,"currencyCode":Locale.current.currencyCode ?? "GBP","job_id":jobDetails["job_id"]!,"billingAddress":"Bill Test","orderDescription":"Test Description","customerOrderCode":jobDetails["job_id"]!] as [String : Any]
        
        print(parameter)
        
        HUD.show(.progress)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]?["paymentStatus"] as? String == "SUCCESS"{
                    
                    DispatchQueue.main.async {
                        //self.navigationController?.popToRootViewController(animated: true)
                        DispatchQueue.main.async(execute: {
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false
                            )
                            let alertView = SCLAlertView(appearance: appearance)
                            alertView.addButton("OK") {
                                //                                if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                                //                                    self.navigationController?.popToRootViewController(animated: true)
                                //                                }else{
                                //                                    self.CompleteJobAPI()
                                //                                }
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            alertView.showSuccess(AppName, subTitle: "Payment received successfully!")
                        })
                    }
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as? String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
        
    }
    
    
    //MARK:- Stripe
    
    func STP_GetCardDetails() {
        
        let addCardViewController = STPAddCardViewController()
        
        addCardViewController.delegate = self
        
        self.navigationController?.pushViewController(addCardViewController, animated: true)
        
//        let navigationController = UINavigationController(rootViewController: addCardViewController)
//        navigationController.navigationBar.barTintColor = .black
//
//        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.backgroundColor : UIColor.black]
//
//        navigationController.modalPresentationStyle = .fullScreen
//
//        self.present(navigationController, animated: true, completion: nil)

    }
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        debugPrint(token)
        debugPrint(token.tokenId)
        //navigationController?.popToRootViewController(animated: true)
        self.send_stripe_token(token: token.tokenId)
    }
    
    func send_stripe_token(token:String!){
        
        var amount:Double = 0
        
        if  Double(price_fare) != 0{
            amount = Double(price_fare)!
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"username":user_dict["user_name"] as! String,"email":user_dict["user_name"]as! String,"type":"stripe_payment","stripeToken":token!, "amount":amount,"currencyCode":Locale.current.currencyCode ?? "GBP","job_id":jobDetails["job_id"]!,"billingAddress":"Bill Test","description":"Test Description"] as [String : Any]
        
        print(parameter)
        
        HUD.show(.progress)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]?["paymentStatus"] as? String == "Payment Completed Successfully"{
                    
                    DispatchQueue.main.async {
                        DispatchQueue.main.async(execute: {
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false
                            )
                            let alertView = SCLAlertView(appearance: appearance)
                            alertView.addButton("OK") {
                                //                                if let status = self.jobDetails["source"] as? String, status == "CustomerApp"{
                                //                                    self.navigationController?.popToRootViewController(animated: true)
                                //                                }else{
                                //                                    self.CompleteJobAPI()
                                //                                }
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            alertView.showSuccess(AppName, subTitle: "Payment received successfully!")
                        })
                        
                    }
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["paymentStatus"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    //MARK: - Payment URL
    
    func paymentURL(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"get_rent_link","username":user_dict["user_name"]!,"driver_id": user_dict["driver_id"]!] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if let url_str  = dict["DATA"]?["rentlink"] as? String{
                    
                    //print(URL(string: url_str)!)
                    let str = "http://" + url_str
                    
                    DispatchQueue.main.async(execute: {
                        self.performSegue(withIdentifier: "GotoPayment", sender: str)
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]?["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
                
            }
        })
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "GotoPayment" {
            
            //            let nav = segue.destination as! UINavigationController
            //            let pay = nav.viewControllers[0] as! PaymentView
            //            //pay.job_id = Int(String(describing: jobDetails["job_id"]!))!
            //            //pay.fare = String(describing: jobDetails["fare"]!)
            //
            //            var str = sender as! String
            //            str += "?job_id=" + String(describing: jobDetails["job_id"]!) + "&fare=" + String(describing: jobDetails["fare"]!)
            //            pay.paymentURL = str
            
        }
        else if segue.identifier == "ShowMeter"{
            let metervc = segue.destination as! MeterVC
            metervc.MeterFinishDelegate = self
            metervc.jobDetails = self.jobDetails
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txt_fare {
            self.view.endEditing(true)
            self.btnInfoTapped(self.btnInfo)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

extension JobProcessVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddresses.count
    }
    
    //-------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let strKey = self.arrAddresses[indexPath.row]
        
        if strKey == "pickup" || strKey == "destination" || strKey == "note" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickUpCell") as! PickUpCell
            
            let strText = self.jobDetails[strKey] as? String ?? ""
            
            cell.lblAddress.text = strText
            
            cell.btnNav.valueText = strText
            
            cell.btnNav.isHidden = false
            
            cell.btnNav.tag = indexPath.row
            
            cell.btnNav.addTarget(self, action: #selector(btnNavigateToLocationTapped(_:)), for: .touchUpInside)
            cell.lblAddress.makeRegular()
            
            if strKey == "pickup" {
                cell.imgView.image = #imageLiteral(resourceName: "greenPin")
                cell.lblAddress.text = strText
                self.dispatchState == 2 ? cell.lblAddress.makeRegular() : cell.lblAddress.makeBolded()
            }else if strKey == "destination" {
                cell.imgView.image = #imageLiteral(resourceName: "redPin")
                cell.lblAddress.text = strText
                self.dispatchState == 2 ? cell.lblAddress.makeBolded() : cell.lblAddress.makeRegular()
            }else if strKey == "note" {
                cell.imgView.image = #imageLiteral(resourceName: "note")
                cell.lblAddress.text = "Notes : \(strText)"
                cell.btnNav.isHidden = true
            }
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobAddressCell") as! JobAddressCell
            
            let strText = self.jobDetails[strKey] as? String ?? ""

            cell.lblAddress.text = strText
            
            cell.btnNav.isHidden = false
            
            cell.lblPassenger.isHidden = true
            
            cell.btnMobile.isHidden = true
            
            cell.btnArrived.isEnabled = true
            
            cell.btnNav.valueText = self.jobDetails[strKey] as? String ?? ""
            
            cell.btnArrived.accessibilityIdentifier = ""
            
            cell.btnMobile.accessibilityIdentifier = ""
            
            cell.btnNav.tag = indexPath.row
            cell.btnMobile.tag = indexPath.row
            cell.btnArrived.tag = indexPath.row
            cell.btnPickedUp.tag = indexPath.row
            cell.btnNoShow.tag = indexPath.row
            
            cell.btnNav.addTarget(self, action: #selector(btnNavigateToLocationTapped(_:)), for: .touchUpInside)
            
            cell.btnMobile.addTarget(self, action: #selector(btnMobileTapped(_:)), for: .touchUpInside)
            
            cell.btnArrived.addTarget(self, action: #selector(btnViaArrivedTapped(_:)), for: .touchUpInside)
            
            cell.btnPickedUp.addTarget(self, action: #selector(btnViaPickUpTappedTapped(_:)), for: .touchUpInside)
            
            cell.btnNoShow.addTarget(self, action: #selector(btnViaNoshowTapped(_:)), for: .touchUpInside)
            
            
            self.hideShowPassengerDetails(inCell: cell, isHide: true)
            
            cell.lblAddress.makeRegular()
            
            if strKey == "pickup" {
                cell.imgView.image = #imageLiteral(resourceName: "greenPin")
                cell.lblAddress.text = strText
                self.dispatchState == 2 ? cell.lblAddress.makeRegular() : cell.lblAddress.makeBolded()

            }else if strKey == "destination" {
                cell.imgView.image = #imageLiteral(resourceName: "redPin")
                cell.lblAddress.text = strText
                self.dispatchState == 2 ? cell.lblAddress.makeBolded() : cell.lblAddress.makeRegular()

            }else if strKey == "note" {
                cell.imgView.image = #imageLiteral(resourceName: "note")
                cell.lblAddress.text = "Notes : \(strText)"

                cell.btnNav.isHidden = true
            }else{
                
                cell.lblAddress.text = strText

                //                if dispatchState == 4 { // Arrived
                //                    self.hideShowPassengerDetails(inCell: cell, isHide: false)
                //                }
                
                cell.imgView.image = #imageLiteral(resourceName: "arrow_location")
                
                let index = strKey.last!
                
                let viaNumber = "v\(index)"
                
                if dispatchState == 0 { // Picked Up
                    self.hideShowPassengerDetails(inCell: cell, isHide: false, viaNo: Int(String(index))!)
                }
                
                cell.btnArrived.accessibilityIdentifier = viaNumber
                cell.btnPickedUp.accessibilityIdentifier = viaNumber
                cell.btnNoShow.accessibilityIdentifier = viaNumber
                
                let passengerName = self.jobDetails["passenger\(String(describing: index))"] as? String ?? " "
                
                let mobile = self.jobDetails["mobile\(String(describing: index))"] as? String ?? ""
                
                cell.btnMobile.accessibilityIdentifier = "mobile\(String(describing: index))"
                
                cell.btnMobile.isHidden = mobile.isEmptyAfterTrim
                
                cell.lblPassenger.text = passengerName
            }
            return cell
        }
    }
    
    //-------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //-------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}

extension UILabel {
    
    func makeBolded() {
        self.font = UIFont.boldSystemFont(ofSize: self.font.pointSize)
    }
    
    //----------------------------------------
    
    func makeRegular() {
        self.font = UIFont.systemFont(ofSize: self.font.pointSize)
    }
}
