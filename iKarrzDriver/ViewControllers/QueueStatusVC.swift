//
//  QueueStatusVC.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 08/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import Alamofire
import SCLAlertView
import PKHUD

import Kingfisher

class QueueStatusVC: CommonVC, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tbl: UITableView!
    
    var arr_allData = [AnyObject]()
    let refreshControl = UIRefreshControl()
    
    var arr_expanded_section = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tbl.estimatedRowHeight = 44.0
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView(frame: .zero)
        
        
        let title = NSLocalizedString("PullToRefresh", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tbl.refreshControl = refreshControl
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func refreshData(){
        self.loadData(isRefresh: true)
    }
    
    //MARK:- Load Data
    @IBAction func loadData(isRefresh:Bool = false){

        if appDelegate.reachability.connection == .unavailable{
                return
            }
        if !isRefresh{
            HUD.show(.progress)
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
            let parameter  = ["office_name":user_dict["office_name"]!,"type":"driver_queue_status"] as [String : Any]
            // print(parameter)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                self.refreshControl.endRefreshing()
                
                switch response.result {
                case .success:
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["RESULT"] as! String ==  "OK"{
                        
                        if let arr = dict["DATA"] as? [AnyObject]{
                        self.arr_allData = arr
                        self.sort_data()
                        DispatchQueue.main.async(execute: {
                            self.tbl.reloadData()
                        })
                        }
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]?["msg"] as! String)
                        })
                    }
                    
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                }
            })
       
    }
    
    func sort_data(){
        
        for (index, dict) in arr_allData.enumerated(){
            var dd = dict as! [String:AnyObject]
            if let arr_queue = dict["queue"] as? [[String:AnyObject]], !arr_queue.isEmpty{
               
                let sorted_arr = arr_queue.sorted(by: {
                    var a = 0
                    if let p1  = $0["queue_pos"] as? String , !p1.isEmpty{
                        a = Int(p1)!
                    }
                    
                    var b = 0
                    if let p2  = $1["queue_pos"] as? String , !p2.isEmpty{
                        b = Int(p2)!
                    }
//                    let b = Int($1["queue_pos"] as? String ?? "0")
//                    debugPrint("a is \(a) \n b is \(b)")
//                    return (Int($0["queue_pos"] as? String ?? "0")!) < (Int($1["queue_pos"] as? String ?? "0")!)
                    
                    return  a < b
                    
                })
                dd["queue"] = sorted_arr as AnyObject
                arr_allData[index] = dd as AnyObject
            }
        }
        
    }
    
    
    //MARK:- TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arr_allData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell_header = tableView.dequeueReusableCell(withIdentifier: "userheader") as! QueueHeaderCell
        
        let dict = arr_allData[section] as! [String:AnyObject]
        
        //header?.textLabel?.text = dict["plot_name"] as? String
        cell_header.btn.setTitle(dict["plot_name"] as? String ?? "", for: .normal)
        cell_header.btn.tag = section
        cell_header.btn.addTarget(self, action: #selector(expand_collapse_section(sender:)), for: .touchUpInside)
        
        if arr_expanded_section.contains(section){
            cell_header.btn.setImage(#imageLiteral(resourceName: "arrow_up"), for: .normal)
        }else{
            cell_header.btn.setImage(#imageLiteral(resourceName: "arrow_down2"), for: .normal)
        }
        
        
        if let color_str = dict["color"] as? String{
            
            switch color_str{
            case "red":
                cell_header.btn.setTitleColor(UIColor.white, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.red
                //header?.textLabel?.textColor = UIColor.red
            case "grey":
                cell_header.btn.setTitleColor(UIColor.black, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.lightGray
                //header?.textLabel?.textColor = UIColor.gray
            case "green":
                cell_header.btn.setTitleColor(UIColor.white, for: .normal)
                cell_header.contentView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.7411764706, blue: 0.1254901961, alpha: 1)
                //header?.textLabel?.textColor = UIColor.green
            case "blue":
                cell_header.btn.setTitleColor(UIColor.white, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.blue
                //header?.textLabel?.textColor = UIColor.blue
            case "orange":
                cell_header.btn.setTitleColor(UIColor.white, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.orange
                //header?.textLabel?.textColor = UIColor.orange
            case "purple":
                cell_header.btn.setTitleColor(UIColor.white, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.purple
                //header?.textLabel?.textColor = UIColor.purple
            case "yellow":
                cell_header.btn.setTitleColor(UIColor.black, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.yellow
                //header?.textLabel?.textColor = UIColor.yellow
            default:
                cell_header.btn.setTitleColor(UIColor.white, for: .normal)
                cell_header.contentView.backgroundColor = UIColor.black
                //header?.textLabel?.textColor = UIColor.black
            }
        }
        
        
        return cell_header.contentView
    }
    
    @objc func expand_collapse_section(sender:UIButton!){
        
        if arr_expanded_section.contains(sender.tag){
            let index = arr_expanded_section.firstIndex(of: sender.tag)
            arr_expanded_section.remove(at: index!)
        }else{
            arr_expanded_section.append(sender.tag)
        }
        tbl.reloadSections(IndexSet(integer: sender.tag), with: .none)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dict = arr_allData[section] as! [String:AnyObject]
        
        if let arr_queue = dict["queue"] as? [AnyObject]{
            if arr_expanded_section.contains(section){
                return arr_queue.count
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QueueCell") as! QueueStatusCell
        
        let dict = arr_allData[indexPath.section] as! [String:AnyObject]
        
        if let arr_queue = dict["queue"] as? [AnyObject]{
            let driver_dict = arr_queue[indexPath.row] as![String:AnyObject]
            
            if let time = driver_dict["waiting_since"] as? String , !time.isEmpty{
                cell.waiting_since.text = time
                cell.cons_clock_width.constant = 20
                cell.layoutSubviews()
            }else{
                cell.waiting_since.text = ""
                cell.cons_clock_width.constant = 0
                cell.layoutSubviews()
            }
            
            cell.driver_name.text = driver_dict["username"] as? String
            
            if let color_str = driver_dict["color"] as? String{
                
                switch color_str{
                case "red":
                    cell.driver_name.textColor = UIColor.red
                case "grey":
                    cell.driver_name.textColor = UIColor.gray
                case "green":
                    cell.driver_name.textColor = UIColor.green
                case "blue":
                    cell.driver_name.textColor = UIColor.blue
                case "orange":
                    cell.driver_name.textColor = UIColor.orange
                case "purple":
                    cell.driver_name.textColor = UIColor.purple
                case "yellow":
                    cell.driver_name.textColor = UIColor.yellow
                default:
                    cell.driver_name.textColor = UIColor.black
                }
            }
            
            if let str = driver_dict["image"] as? String{
                let url = URL(string: str)
                cell.img.kf.setImage(with: url)
            }else{
                cell.img.image = nil
            }
            
        }
        
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
