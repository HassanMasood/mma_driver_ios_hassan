//
//  NewJobsVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 26/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import PKHUD
import SCLAlertView
import Alamofire

class NewJobsVC: CommonVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbl : UITableView!
    
    var arr_jobs = [[String:AnyObject]]()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tbl.estimatedRowHeight = 44.0
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView(frame: CGRect.zero)
        
        self.refreshControl.addTarget(self, action: #selector(refreshdata), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tbl.refreshControl = refreshControl
        
        self.getJobs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getJobs), name: NSNotification.Name(rawValue: "Update_NewList_Jobs"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Load Jobs from DB
    func loadFromDB(){
        
        let predicate = NSPredicate(format: "job_status == %@", "New")
        let jobs = DBHelper.sharedInstance.featchFromEntityWithPredicate(Entity: "Jobs", predicate: predicate)
        
        if jobs.Error == nil{
            //var arr_all = [[String:AnyObject]]()
            self.arr_jobs.removeAll()
            for job_obj in jobs.arr!{
                if let job = job_obj as? Jobs {
                    let keys = Array(job.entity.attributesByName.keys)
                    let dict = job.dictionaryWithValues(forKeys: keys) as [String:AnyObject]
                    self.arr_jobs.append(dict)
                }
            }
            
            if self.arr_jobs.isEmpty{
                appDelegate.showerror(str: "No job found!")
            }
            self.tbl.reloadData()
        }
        else{
            SCLAlertView().showError(AppName, subTitle: (jobs.Error?.localizedDescription)!)
        }
    }
    
    //MARK: - Fetch Jobs
    
    @objc func getJobs(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            
            self.loadFromDB()
            
            if isRefresh{
                self.refreshControl.endRefreshing()
            }
            return
        }
        
         if !isRefresh{
        HUD.show(.progress)
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_details", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            self.refreshControl.endRefreshing()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                self.arr_jobs  = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    self.arr_jobs.remove(at: 0)
                    DBHelper.sharedInstance.deleteAll(str_entity: "Jobs")
                    DBHelper.sharedInstance.saveAllJobs(arr_jobs: self.arr_jobs, completion: { (finish) in
                        
                    })
                    self.arr_jobs = self.arr_jobs.filter({$0["job_status"] as! String == "New"})
                    
                    DispatchQueue.main.async(execute: {
                        //self.tbl.isHidden = false
                        self.tbl.reloadData()
                        
                        if self.arr_jobs.isEmpty{
                            //appDelegate.showerror(str: "Not any job allocated.")
                        }
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: self.arr_jobs[0]["msg"] as! String)
                        //self.tbl.isHidden = true
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
            
        })
        
    }
    
    //MARK: Refresh Data
    
    @objc func refreshdata(){
        self.getJobs(isRefresh: true)
    }
    
    /*
    @objc func refreshdata(){
        
        if appDelegate.reachability.connection == .none{
            return
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_details", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            self.refreshControl.endRefreshing()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                self.arr_jobs  = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    self.arr_jobs.remove(at: 0)
                    self.arr_jobs = self.arr_jobs.filter({$0["job_status"] as! String == "New"})
                    
                    DispatchQueue.main.async(execute: {
                        //self.tbl.isHidden = false
                        self.tbl.reloadData()
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                       // SCLAlertView().showError(AppName, subTitle: self.arr_jobs[0]["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:self.arr_jobs[0]["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        //self.tbl.isHidden = true
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
            
        })
        
        
    }
    */
    
    //MARK:- TableView Delegate & Datasource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_jobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl.dequeueReusableCell(withIdentifier: "Cell") as! JobsInProgressCell
        
        let dict = arr_jobs[indexPath.row]
        
        cell.lbl_pickup.text = dict["pickup"] as? String
        cell.lbl_destination.text = dict["destination"] as? String
        cell.lbl_date.text = dict["job_date"] as? String
//        cell.lbl_time.text = dict["job_time"] as? String
        cell.lbl_payment.text = dict["payment_type"] as? String
        cell.lbl_fare.text = dict["fare"] as? String
        
        
        let arr_details = ["name","mobile","caller"]
        
        cell.info_stack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        for str in arr_details{
            if let jobdetail = dict[str] as? String, !jobdetail.isEmptyAfterTrim{
                
                let lbl_detail = UILabel()
                lbl_detail.numberOfLines = 0
                lbl_detail.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_detail.backgroundColor = UIColor.clear
                if str == "name"{
                    lbl_detail.text  = "Name:" + "  " + jobdetail
                }
                else if str == "mobile"{
                    lbl_detail.text  = "mobile:" + "  " + jobdetail
                }else{
                    lbl_detail.text = jobdetail
                }
                lbl_detail.textAlignment = .left
                cell.info_stack.addArrangedSubview(lbl_detail)
            }
        }
        
        //cell.backgroundColor = #colorLiteral(red: 1, green: 0.8823529412, blue: 0.7960784314, alpha: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "NewtoJobProcess", sender: indexPath)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "NewtoJobProcess" {
            let indexPath = sender as! IndexPath
            
            let jobprocess = segue.destination as! JobProcessVC
            jobprocess.jobDetails = arr_jobs[indexPath.row]
        }
    }
    
}
