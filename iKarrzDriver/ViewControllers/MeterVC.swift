//
//  MeterVC.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 25/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import MapKit
import CoreLocation

import PKHUD
import SCLAlertView

protocol CompleteMeterDelegate {
    func meterFinished()
}


class MeterVC: CommonVC , CLLocationManagerDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{

    @IBOutlet weak var vehiclePicker: UIPickerView!
    @IBOutlet weak var view_Vehiclepicker: UIView!
    var arr_Vehicle = [[String:AnyObject]]()
    
    @IBOutlet weak var map : MKMapView!
    
    @IBOutlet weak var lbl_miles : UILabel!
    @IBOutlet weak var lbl_vehicleType : UILabel!
    @IBOutlet weak var txt_wttime : CustomTextField!
    @IBOutlet weak var lbl_wtcharge : UILabel!
    @IBOutlet weak var txt_extra : CustomTextField!
    @IBOutlet weak var txt_discount : CustomTextField!
    
    @IBOutlet weak var lbl_Total : UILabel!
    
    
    let locationManager = CLLocationManager()
    
    var start_location : CLLocation?
    var final_location : CLLocation?
    var latest_location : CLLocation?
    
    var last_distance:Double = 0
    var last_timeInterval: TimeInterval!
    var last_Date =  Date()
    var total_waitingTime:Double = 0
    
    var vehicle_type = ""
    var arr_data = [[String:AnyObject]]()
    var arr_miles = [[String:AnyObject]]()
    
    var total_miles : Double = 0
    
    var jobDetails : [String:AnyObject]!
    
    var MeterFinishDelegate : CompleteMeterDelegate?
    
    var isCompleted = false
    
    //----------------------------------------
    
    class func viewController()-> MeterVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MeterVC") as! MeterVC
    }
    
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupData()
        
        
        vehicle_type = jobDetails["vehicle_type"] as? String ?? ""
        lbl_vehicleType.text = vehicle_type
        
        if !vehicle_type.isEmpty{
            self.GetPrices()
        }
        
        txt_extra.text = jobDetails["extras"] as? String ?? "0"
        lbl_wtcharge.text = jobDetails["wttime"] as? String ?? "0"
        //txt_wttime.text = jobDetails["wttime"] as? String ?? "0"
        
        var wttime_minutes = jobDetails["wttime_val"] as? String ?? "0"
        wttime_minutes = wttime_minutes.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        if Double(wttime_minutes) != nil{
            txt_wttime.text = wttime_minutes
        }else{
            txt_wttime.text = "0"
        }
        
        if Double(jobDetails["discount"] as! String) != nil{
            txt_discount.text = jobDetails["discount"] as? String
        }else{
            txt_discount.text = "0"
        }
        
       // txt.text = jobDetails["wttime_val"] as? String ?? "0"
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if vehicle_type.isEmpty{
            appDelegate.showerror(str: "Please select the vehicle first & meter willl be start automatically.")
            self.getVehicleData()
        }
        
        let myBackButton:UIButton = UIButton.init(type: .custom)
        myBackButton.addTarget(self, action: #selector(back_press), for: .touchUpInside)
        let img = UIImage(named: "back-1")?.withRenderingMode(.alwaysTemplate)
        
        myBackButton.setImage(img, for: .normal)
        myBackButton.imageView?.tintColor = .white
        myBackButton.imageView?.contentMode = .scaleAspectFit
        
        //Add back button to navigationBar as left Button
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
//        let back1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back_press))
//       // UIBarButtonItem.appearance().setBackButtonBackgroundImage(#imageLiteral(resourceName: "back"), forState: .Normal, barMetrics: .Default)
//        self.navigationItem.leftBarButtonItem  = back1

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.change_Status()
        
    }
    
    @objc func back_press(){
        if !isCompleted{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alert = SCLAlertView(appearance: appearance)
            alert.addButton("Proceed", action: {
                self.navigationController?.popViewController(animated: true)
            })
            alert.addButton("Cancel", action: {
                
            })
            
            alert.showNotice(AppName, subTitle: "This action will stop and reset meter, do you wish to proceed ?")
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setupData(){
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if CLLocationManager.authorizationStatus() == .notDetermined{
            //self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.requestAlwaysAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Get Prices
    
     func GetPrices() {
            
        if appDelegate.reachability.connection == .unavailable{
                SCLAlertView().showError(AppName, subTitle: "Please check your internet connection, unable to get price fare structure.")
                return
            }
            
           // HUD.show(.progress)
        
         let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"vehicles_meter_set","office_name":user_dict["office_name"]! ,"vehicle_type": vehicle_type] as [String : Any]
             print(parameter)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                
                switch response.result {
                case .success:
                    debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["RESULT"] as! String == "OK"{
                        
                        if let arr = dict["DATA"] as? [[String:AnyObject]], let all_miles = dict["Miles"] as? [[String:AnyObject]]{
                            self.arr_data = arr
                            self.arr_miles = all_miles  
                            self.setupData()
                        }
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: "Server error")
                        })
                    }
                    
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                    
                }
            })
    }

    
    //MARK:- Get Vehicle API
    
    func getVehicleData(){
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"vehicle_type"] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                
                let dict = response.result.value as! [String:AnyObject]
                
                let arr_temp = dict["DATA"] as! [[String:AnyObject]]
                let dict2 = arr_temp[0]
                if dict2["msg"] as! String == "Vehicle Type"{
                    
                    if dict["DATA"] !=  nil{
                        //UserDefaults.standard.set(dict["DATA"], forKey: "User_Info")
                        // let arr_temp = dict["DATA"] as! [[String:AnyObject]]
                        self.arr_Vehicle = arr_temp.filter({ $0["type"] != nil })
                        self.vehiclePicker.reloadAllComponents()
                        if !self.arr_Vehicle.isEmpty{
                            self.vehiclePicker.selectRow(0, inComponent: 0, animated: false)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        self.view_Vehiclepicker.alpha = 0.0
                        UIView.animate(withDuration: 0.25, animations: {
                            self.view_Vehiclepicker.alpha = 1.0
                            self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        })
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]?["msg"] as! String)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    //MARK:- Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_Vehicle.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        let dict = arr_Vehicle[row]
        return dict["type"] as? String
        
    }
    
    @IBAction func Selected_VehiclePicker(sender:UIPickerView!){
        
        let index = vehiclePicker.selectedRow(inComponent: 0)
        
        let dict = arr_Vehicle[index]
        vehicle_type = dict["type"] as? String ?? ""
        lbl_vehicleType.text = dict["type"] as? String ?? ""
        
        self.GetPrices()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_Vehiclepicker.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_Vehiclepicker.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    //MARK:- Current Lcoation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let latest_location1 = locations[locations.count - 1]
        
       // latest_location = latest_location1
        
        if vehicle_type.isEmpty || isCompleted{
            return
        }
        
        if start_location == nil {
            start_location = latest_location1
            latest_location = latest_location1
            last_timeInterval = Date().timeIntervalSince1970
        }
        print("location \(latest_location1.coordinate.latitude), \(latest_location1.coordinate.longitude)")
        //let distanceBetween: CLLocationDistance = latest_location!.distance(from: start_location!)
        let distanceBetween: CLLocationDistance = latest_location1.distance(from: latest_location!)
        print("pmy distance %.2f",distanceBetween)
        
      //  debugPrint(latest_location1)
      //  debugPrint(latest_location!)
        
        //if last_distance != 0{
        //if Double(distanceBetween) - last_distance == 0 && start_location != nil{
        if distanceBetween <= 1  && start_location != nil{
            //total_waitingTime += Date().timeIntervalSince(last_Date)
            print("Before time \(total_waitingTime)")
            total_waitingTime +=  Double(Date().timeIntervalSince1970 - last_timeInterval)
            print("after adding time \(total_waitingTime)")
            let minutes = (Int(total_waitingTime) / 60) % 60
            print("calcu time \(total_waitingTime)")
            print("minutes: \(minutes)")
            //let total_minutes = minutes + Int(txt_wttime.text!)!
            txt_wttime.text = "\(minutes)"
            //txt_wttime.text = "\(total_minutes)"
        }else{
            let distance_miles:Double = Double(distanceBetween) *  0.00062137
            total_miles += distance_miles
            if final_location == nil{
                //lbl_miles.text = String(format: "%.2f", distance_miles)
                lbl_miles.text = String(format: "%.2f", total_miles)
                self.calculatePrice()
            }
        }
        
        last_distance = distanceBetween
        latest_location = latest_location1
        
        last_timeInterval = Date().timeIntervalSince1970
        last_Date = Date()
        //else{
        
          //  }
       // }
 
        let center = CLLocationCoordinate2D(latitude: latest_location!.coordinate.latitude, longitude: latest_location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001))
        
        map.setRegion(region, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //appDelegate.showerror(str: error.localizedDescription)
        debugPrint(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
    }
    
//    func add_waiting_time(){
//        total_waitingTime += 1
//        txt_wttime.text = "\(total_waitingTime)"
//    }
    
    //MARK:- TextField
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        self.calculatePrice()
        return true
    }
    
    //MARK:- Calculate Price
    
    func calculatePrice(){
        
        var miles = Double(lbl_miles.text ?? "0") ?? 0
        //miles = 100
        let extra = Double(txt_extra.text ?? "0") ?? 0
        let discount = Double(txt_discount.text ?? "0") ?? 0
        
        var minimum: Double = 0
        var waiting_charge: Double = 0
        var price:Double = extra
        
            for dict in arr_miles{
                if let dict_mile = dict["Range"] as? String{
                    
                    let arr = dict_mile.replacingOccurrences(of: " ", with: "0").components(separatedBy: "-")
                    if arr.count > 1{
                        let lower_mile = Double(arr[0])!
                        let upper_mile = Double(arr[1])!
                        
                        //if miles <= lower_mile {
                            let mile_price_str = dict["price"] as! String
                            let mile_price = Double(mile_price_str)!
                            if miles > upper_mile{
                                price += (upper_mile - lower_mile) * mile_price
                                miles -= (upper_mile - lower_mile)
                            }else{
                                price += miles * mile_price
                                break
                            }
                        //}
                    }
                }
            }
        
        for dict in arr_data{
            if let mini = dict["minimun_price"] as? String{
                minimum = Double(mini)!
                price += minimum
            }
            if let wt = dict["waiting_charges"] as? String{
                let wt_time =  Double(txt_wttime.text ?? "0") ?? 0
                let minute_charge = Double(wt)
                waiting_charge = Double(wt_time * (minute_charge!/30))
                price += waiting_charge
            }
        }
        
        price -= price * (discount * 0.01)
        
        lbl_Total.text = String(format: "%.2f", price)
    }
    
    @IBAction func MeterFinish(){
        final_location = latest_location
        self.UpdateFareAPI()
    }
    
    
    //MARK:- Update Fare
    
    func UpdateFareAPI(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection, unable to update price on server.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        let parameter  = ["type":"updatemeterfare","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"fare":String(format:"%.2f",Float(lbl_Total.text ?? "0") ?? 0),"extras":String(format:"%.2f",Float(txt_extra.text ?? "0") ?? 0),"waitcharge":String(format:"%.2f",Float(lbl_wtcharge.text ?? "0") ?? 0),"wttime_val":
            txt_wttime.text ?? "" + " M","discount":String(format:"%.2f",Float(txt_discount.text ?? "0") ?? 0)] as [String : Any]
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                debugPrint(response.result.value ?? "")
                
                self.isCompleted = true
                if let delegate = self.MeterFinishDelegate {
                    delegate.meterFinished()
                }
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_job"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_job"), object: nil)
                self.navigationController?.popViewController(animated: true)
                
                
            case .failure(let error):
                debugPrint(error)
                appDelegate.showerror(str: error.localizedDescription)
            }

            DispatchQueue.main.async(execute: {
                //self.change_Status()
            })
        })
        
    }
    
    //MARK:- Change Status
    /*
    func change_Status(){
        if appDelegate.reachability.connection == .none{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
       // let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2] as [String : Any]
        //let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2,"latitude":final_location?.coordinate.latitude ?? 0,"longitude":final_location?.coordinate.longitude ?? 0] as [String : Any]
        
         var parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2,"latitude":final_location?.coordinate.latitude ?? 0,"longitude":final_location?.coordinate.longitude ?? 0] as [String : Any]
        
        if let loc = final_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            // print(response.request?.url)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Completed"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.isCompleted = true
                        if let delegate = self.MeterFinishDelegate {
                            delegate.meterFinished()
                        }
                        
                        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_job"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_job"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                       // self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
    }
    */

    
    func change_Status(){
        if appDelegate.reachability.connection == .unavailable{
            //SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
       // HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        // let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2] as [String : Any]
        //let parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2,"latitude":final_location?.coordinate.latitude ?? 0,"longitude":final_location?.coordinate.longitude ?? 0] as [String : Any]
        
        //var parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2,"latitude":final_location?.coordinate.latitude ?? 0,"longitude":final_location?.coordinate.longitude ?? 0] as [String : Any]
        var parameter  = ["type":"driver_job_complete","job_id":jobDetails["job_id"]!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":2] as [String : Any]
        
        if let loc = latest_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            // print(response.request?.url)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Completed"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        /*
                        self.isCompleted = true
                        if let delegate = self.MeterFinishDelegate {
                            delegate.meterFinished()
                        }
                        
                        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_job"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_job"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        // self.navigationController?.popToRootViewController(animated: true)
                         */
                        
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: dict["DATA"]!["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:dict["DATA"]!["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
