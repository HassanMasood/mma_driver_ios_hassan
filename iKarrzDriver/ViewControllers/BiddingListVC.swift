//
//  BiddingListVC.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 16/02/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

import SCLAlertView
import Alamofire
import PKHUD

class BiddingListVC: CommonVC, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tbl : UITableView!
    @IBOutlet weak var no_Jobs : UIStackView!
    
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var txtViewComments: UITextView!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAccept: UIButton!

    var arr = [[String :AnyObject]]()
    
    var dictSelected: [String :AnyObject] = [String :AnyObject]()

    let refreshControl = UIRefreshControl()
    
    var isFromNotification: Bool = false
    
    //----------------------------------------
    
    // MARK: Abstract Method
    
    //----------------------------------------

    class func viewController() -> BiddingListVC {
        return UIStoryboard(name: "Second", bundle: nil).instantiateViewController(withIdentifier: "BiddingListVC")  as! BiddingListVC
    }
    
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.get_biddingList(isRefresh: false)
        self.setUpData()
    }
    
    func setUpData(){
        
        tbl.tableFooterView = UIView(frame: .zero)
        
        self.refreshControl.addTarget(self, action: #selector(refreshdata), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tbl.refreshControl = refreshControl
        
        self.viewPopUp.layer.cornerRadius = 5.0
        self.viewPopUp.layer.borderWidth = 2
        self.viewPopUp.layer.borderColor = UIColor.black.cgColor
        self.viewPopUp.clipsToBounds = true
        
        self.txtViewComments.layer.cornerRadius = 5.0
        self.txtViewComments.layer.borderWidth = 2
        self.txtViewComments.layer.borderColor = UIColor.black.cgColor
        
        self.txtAmount.layer.cornerRadius = 5.0
        self.txtAmount.layer.borderWidth = 2
        self.txtAmount.layer.borderColor = UIColor.black.cgColor
        
        self.setupButtonUI(for: self.btnSubmit)
        self.setupButtonUI(for: self.btnCancel)
        self.setupButtonUI(for: self.btnAccept)

    }
    
    //----------------------------------------
    
    func addAnimationToView(to viewAmin: UIView, isShow: Bool, completion: @escaping(_ completed: Bool)-> Void) {
        
        viewAmin.isHidden = false
        
        viewAmin.transform = isShow ? CGAffineTransform.init(scaleX: 0.01, y: 0.01) : CGAffineTransform.identity
        
        UIView.animate(withDuration: 0.25, animations: {
            
            viewAmin.transform = isShow ? CGAffineTransform.identity : CGAffineTransform.init(scaleX: 0.01, y: 0.01)
            
        }) { (completed) in
            
            viewAmin.isHidden = !isShow
            
            self.viewTransparent.isHidden = !isShow
            
            self.view.layoutIfNeeded()
            completion(true)
        }
    }
    
    //----------------------------------------
    
    
    @IBAction func btnAcceptTapped(_ sender: Any) {
        
        let minPrice = self.dictSelected["min_bid_price"] as? String ?? ""
        
        self.applyBidding(job_id : self.dictSelected["job_id"] as? String ?? "", amount: minPrice)
        
        self.addAnimationToView(to: self.viewPopUp, isShow: false) { (comp) in
            self.txtAmount.text = ""
            self.txtViewComments.text = ""
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
        self.applyBidding(job_id: self.dictSelected["job_id"] as? String ?? "", amount: txtAmount.text ?? "")
        
        self.addAnimationToView(to: self.viewPopUp, isShow: false) { (comp) in
            self.txtAmount.text = ""
            self.txtViewComments.text = ""
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        
        self.addAnimationToView(to: self.viewPopUp, isShow: false) { (completed) in
            
        }
    }
    

    //----------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Refresh Data
    @objc func refreshdata(){
        self.get_biddingList(isRefresh: true)
    }
    
    @IBAction func retry(){
        self.get_biddingList(isRefresh: false)
    }
    
    //MARK:- Get Data
    func get_biddingList(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        if !isRefresh{
            HUD.show(.progress)
        }
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {return }
        
        let office_name = user_dict["office_name"] as? String ?? ""
        
        let driverId = user_dict["driver_id"] as? String ?? ""
        
        let parameter  = ["office_name": office_name,
                          "driver_id": driverId,
                          "type":"biddinglist" as AnyObject] as [String : AnyObject]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            self.refreshControl.endRefreshing()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as! String == "OK"{
                    if let arr = dict["DATA"] as? [[String:Any]]{
                        self.arr = arr as [[String : AnyObject]]
                        self.no_Jobs.isHidden = !self.arr.isEmpty
                        self.tbl.reloadData()
                        
                        if self.arr.count > 0 {
                            print(self.arr)
                            var arrJobIds : [String] = [String]()
                            self.arr.forEach({ (jobData) in
                                if let jobId = jobData["job_id"] as? String, !arrJobIds.contains(jobId)  {
                                    arrJobIds.append(jobId)
                                }
                            })
                            self.callWSToViewCheckBidding(withJobIds: arrJobIds.joined(separator: ","))
                        }
                        
                    }else{
                        self.arr.removeAll()
                        self.no_Jobs.isHidden = !self.arr.isEmpty
                        self.tbl.reloadData()
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
        
        
    }
    
    //----------------------------------------
    
    func callWSToViewCheckBidding(withJobIds jobIds: String) {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"BiddingViewCheck" as AnyObject,"driver_id": user_dict["driver_id"]!,"job_id": jobIds, "office_name":user_dict["office_name"]!] as [String : AnyObject]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL, method: .get, parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            print(response)
            
            switch response.result {
            case .success:
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as! String == "OK"{
                    
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
        
    }
    
    //----------------------------------------
    
    //MARK:- Tableview
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BiddingCell
        
        let dict = arr[indexPath.row]
        
        cell.lbl_id.text = "Ref.# " + "\(dict["job_id"] as? String ?? "")"
        
        var date_str = dict["job_date"] as? String ?? ""
        if let time = dict["job_time"] as? String{
            date_str = date_str + " " + time
        }
        cell.lbl_date.text = date_str
        cell.lbl_pickup.text = dict["pickup"] as? String
        cell.lbl_destination.text = dict["destination"] as? String
        
        cell.const_Stack_Height.constant = 30
        
        var isNeedToHideStack: Bool = false
        
        if let flightNo = dict["flight_no"] as? String, !flightNo.trimmingCharacters(in: .whitespaces).isEmpty {
            cell.lblFlightNo.isHidden = false
            cell.imgFlight.isHidden = false
            cell.lblFlightNo.text = dict["flight_no"] as? String
        }else{
            cell.lblFlightNo.text = ""
            cell.lblFlightNo.isHidden = true
            cell.imgFlight.isHidden = true
            isNeedToHideStack = true
        }
        
        if let vehicle_type = dict["vehicle_type"] as? String, !vehicle_type.trimmingCharacters(in: .whitespaces).isEmpty {
            cell.lblVehicleType.isHidden = false
            cell.imgVehicle.isHidden = false
            cell.lblVehicleType.text = vehicle_type
        }else{
            cell.lblVehicleType.isHidden = true
            cell.imgVehicle.isHidden = true
            cell.lblVehicleType.text = ""
            if isNeedToHideStack == true {
                cell.const_Stack_Height.constant = 0
            }
        }
        
        if let comments = dict["comments"] as? String, !comments.trimmingCharacters(in: .whitespaces).isEmpty {
            cell.lblComment.text = "Comment : " + comments
            cell.lblComment.isHidden = false
        }else{
            cell.lblComment.text = ""
            cell.lblComment.isHidden = true
        }
        
        cell.lblPrice.text = "Sugg. Min Price : £\(dict["min_bid_price"] as? String ?? "")"
        //cell.lbl_highest_bid.text = dict["highest_bid_till_now"] as? String
        
        if let enable_lock = dict["Enable Bidding"] as? String, enable_lock == "no"{
            cell.img_width.constant = 75
        }else{
            cell.img_width.constant = 0
        }
        cell.layoutIfNeeded()
        
        return cell
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = arr[indexPath.row]
        if let enable_lock = dict["Enable Bidding"] as? String, enable_lock == "no"{
            appDelegate.showerror(str: "Bidding is not unable for this job!")
            return
        }
        
        let minPrice = dict["min_bid_price"] as? String ?? ""
        
        if minPrice.trimmingCharacters(in: .whitespacesAndNewlines) == "" || minPrice == "0" {
            self.btnAccept.isHidden = true
        }else{
            self.btnAccept.isHidden = false
        }

        self.dictSelected = self.arr[indexPath.row]
        
        self.btnAccept.setTitle("Accept job @ \(minPrice)", for: .normal)
        
        self.addAnimationToView(to: self.viewPopUp, isShow: true, completion: { (comp) in
            
        })

        /*
       // self.get_Highestbidding(job_id: dict["job_id"] as! String) { (highest_bid) in
            
            
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alert = SCLAlertView(appearance: appearance)
                let txt = alert.addTextField("amount")
                txt.placeholder = "Enter you bid amount"
                txt.keyboardType = .decimalPad
                alert.addButton("Submit bid") {
                    print("Text value: \(String(describing: txt.text))")
//                    let min_bid = Double(dict["min_bid_price"] as? String ?? "0")
//                    let str = txt.text!.isEmpty ? "0" : txt.text!
//                    let place_bid = Double(str)
//                    if place_bid! < min_bid! {
//                        appDelegate.showerror(str: "Minimum bid should be \(min_bid!)")
//                    }else{
                    self.applyBidding(job_id : dict["job_id"] as! String, amount: txt.text!)
//                    }
                }
                alert.addButton("Cancel") {
                    print("Text value: \(String(describing: txt.text))")
                }
//            if let bid = highest_bid, !bid.isEmpty{
//            alert.showInfo("Place a bid", subTitle: "Current highest bid is: \(bid)")
//            }else{
                alert.showInfo("Place a bid", subTitle: "")
//            }
            
       // }
        */
        
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 205
    }

    //----------------------------------------
    
    //MARK: - Back Press
    @IBAction func backpress(sender:UIButton!){
        
        if self.isFromNotification {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Apply for bid
    
    func applyBidding(job_id : String!, amount:String, comments: String? = nil) {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]! as AnyObject,"type":"AcceptDriverBidEntry" as AnyObject,"driver_id": user_dict["driver_id"]!,"job_id": job_id,"bid":amount,"comment" : comments ?? ""] as [String : AnyObject]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as! String == "OK"{
                    if let message = dict["DATA"] as? String, message == "Your bid has been accepted and job allocated."{
                        //SCLAlertView().showSuccess(AppName, subTitle: "Congratulations! jobs is accepted and allocated to yourself.")
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alert = SCLAlertView(appearance: appearance)
                        
                        alert.addButton("Done", action: {
                            
                            self.Refresh_JobDetailsAPI(jobID: job_id)
                            
                        })
                        alert.showSuccess(AppName, subTitle: "Congratulations! jobs is accepted and allocated to yourself.")
                        
                    }else if let message = dict["DATA"] as? String, message == "Bid inserted" {
                        
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alert = SCLAlertView(appearance: appearance)
                        
                        alert.addButton("Ok", action: {
                            
                        })
                        alert.showSuccess(AppName, subTitle: "Thank you, your bid has been recorded, please bid the lowest amount you can do the job for to get it auto allocated.")
                        
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]! as! String)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
        
    }
    
    //MARk:- Get Current Highest Bid
    func get_Highestbidding(job_id : String!,completion:@escaping (_ highest_bid : String?)->Void){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]! as AnyObject,"type":"highest_bid" as AnyObject,"job_id": job_id] as [String : AnyObject]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as! String == "OK"{
                    if let dict2 = dict["DATA"] as? [String:Any], let bid = dict2["highest_bid_till_now"] as? String{
                        //completion(String(describing: dict2["highest_bid_till_now"]))
                        completion(bid)
                    }else{
                        completion(nil)
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                        completion(nil)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                    completion(nil)
                })
                
            }
        })
        
        
    }
    
    
    //MARK:- Refresh Job Details
    
    func Refresh_JobDetailsAPI(jobID:String!){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_loaded","job_id":jobID!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"device_type":"ios"] as [String : Any]
        print(parameter)
        print(jobID)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                 debugPrint(response.request?.url ?? "")
                debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                let arr = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    
                    DispatchQueue.main.async(execute: {
                        let jobDetails = arr[0]
                            DBHelper.sharedInstance.saveAllJobs(arr_jobs: [jobDetails], completion: { (finish) in
                            })
                        self.dismiss(animated: false, completion: {
                            
                            let jobProcessvc = JobProcessVC.viewController()
                            
                            jobProcessvc.jobDetails = jobDetails
                            
                            let nav  = appDelegate.window?.rootViewController as! UINavigationController
                            
                            nav.pushViewController(jobProcessvc, animated: false)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                        })
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: arr[0]["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:arr[0]["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    
                })
            }
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
