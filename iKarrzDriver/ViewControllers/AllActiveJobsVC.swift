//
//  AllActiveJobsVC.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 14/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import PKHUD
import SCLAlertView
import Alamofire

class AllActiveJobsVC: CommonVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbl : UITableView!
    
    @IBOutlet weak var no_jobs : UIStackView!
    //@IBOutlet weak var seg : UISegmentedControl!
    @IBOutlet weak var btn_new : UIButton!
    @IBOutlet weak var btn_accepted : UIButton!
    
    //var arr_jobs = [[String:AnyObject]]()
    let refreshControl = UIRefreshControl()
    
    var job_type = "New"
    
    var arr_new_jobs = [[String:AnyObject]]()
    var arr_accepted_jobs = [[String:AnyObject]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        
        if job_type == "New"{
            btn_new.backgroundColor = UIColor(hexString: "2EACE2")
            btn_accepted.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1)
        }else{
            btn_new.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1)
            btn_accepted.backgroundColor = UIColor(hexString: "2EACE2")
        }
        
        tbl.estimatedRowHeight = 44
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView(frame: CGRect.zero)
        
        self.refreshControl.addTarget(self, action: #selector(refreshdata), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tbl.refreshControl = refreshControl
        
        self.getJobs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getJobs), name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Load Jobs from DB
    func loadFromDB(){
        
        //let jobs = DBHelper.sharedInstance.featchFromEntity(Entity: "Jobs", predicate_str: "job_status == Accepted")
        
        //let predicate = NSPredicate(format: "job_status == %@", "Accepted")
        //let jobs = DBHelper.sharedInstance.featchFromEntityWithPredicate(Entity: "Jobs", predicate: predicate)
        let jobs = DBHelper.sharedInstance.featchFromEntityWithPredicate(Entity: "Jobs", predicate: nil)
        
        if jobs.Error == nil{
            //var arr_all = [[String:AnyObject]]()
            //self.arr_jobs.removeAll()
            self.arr_new_jobs.removeAll()
            self.arr_accepted_jobs.removeAll()
            for job_obj in jobs.arr!{
                if let job = job_obj as? Jobs {
                    let keys = Array(job.entity.attributesByName.keys)
                    let dict = job.dictionaryWithValues(forKeys: keys) as [String:AnyObject]
                    //self.arr_jobs.append(dict)
                    if dict["job_status"] as! String == "Accepted"{
                       self.arr_accepted_jobs.append(dict)
                    }else{
                       self.arr_new_jobs.append(dict)
                    }
                }
            }
            //print(self.arr_jobs)
//            if self.arr_jobs.isEmpty{
//                appDelegate.showerror(str: "No job found!")
//            }
            //if !self.arr_new_jobs.isEmpty{
                self.btn_new.setTitle("NEW (\(self.arr_new_jobs.count))", for: .normal)
            //}
            //if !self.arr_accepted_jobs.isEmpty{
                self.btn_accepted.setTitle("ACCEPTED (\(self.arr_accepted_jobs.count))", for: .normal)
            //}
            
            if job_type == "New" && self.arr_new_jobs.isEmpty{
               no_jobs.isHidden = false
            }else if job_type == "Accepted" && self.arr_accepted_jobs.isEmpty{
                no_jobs.isHidden = false
            }
            else{
                no_jobs.isHidden = true
            }
            
            self.tbl.reloadData()
        }
        else{
            SCLAlertView().showError(AppName, subTitle: (jobs.Error?.localizedDescription)!)
        }
    }
    
    //MARK: - Fetch Jobs
    
    @objc func getJobs(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            
            self.loadFromDB()
            
            if isRefresh{
                self.refreshControl.endRefreshing()
            }
            
            return
        }
        
        if !isRefresh{
            HUD.show(.progress)
        }
        
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_details", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            self.refreshControl.endRefreshing()
            
            switch response.result {
            case .success:
                // debugPrint(response.result.value!)
                 //print(response.request?.url)
                let dict = response.result.value as! [String:AnyObject]
                //self.arr_jobs  = dict["DATA"] as! [[String:AnyObject]]
                var arr  = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    //self.arr_jobs.remove(at: 0)
                    arr.remove(at: 0)
                    DBHelper.sharedInstance.deleteAll(str_entity: "Jobs")
//                    DBHelper.sharedInstance.saveAllJobs(arr_jobs: self.arr_jobs, completion: { (finish) in
//
//                    })
//                    self.arr_jobs = self.arr_jobs.filter({$0["job_status"] as! String == "Accepted"})
                    DBHelper.sharedInstance.saveAllJobs(arr_jobs: arr, completion: { (finish) in
                        
                    })
                    self.arr_accepted_jobs = arr.filter({$0["job_status"] as! String == "Accepted"})
                    self.arr_new_jobs = arr.filter({$0["job_status"] as! String == "New"})
                    
                    DispatchQueue.main.async(execute: {
                        //if !self.arr_new_jobs.isEmpty{
                        self.btn_new.setTitle("NEW (\(self.arr_new_jobs.count))", for: .normal)
                        //}
                        //if !self.arr_accepted_jobs.isEmpty{
                        self.btn_accepted.setTitle("ACCEPTED (\(self.arr_accepted_jobs.count))", for: .normal)
                        //}
                        
                        //self.tbl.isHidden = false
                        self.tbl.reloadData()
                        
                        if self.job_type == "New" && self.arr_new_jobs.isEmpty{
                            self.no_jobs.isHidden = false
                        }else if self.job_type == "Accepted" && self.arr_accepted_jobs.isEmpty{
                            self.no_jobs.isHidden = false
                        }
                        else{
                            self.no_jobs.isHidden = true
                        }
                        
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: arr[0]["msg"] as! String)
                        //self.tbl.isHidden = true
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
            
        })
        
    }
    
    //MARK: Refresh Data
    @objc func refreshdata(){
        self.getJobs(isRefresh: true)
    }

    //----------------------------------------
    
    //MARK:- Retry Jobs
    @IBAction func retry(){
         self.getJobs()
    }
    
    //MARK:- Segemnt
    @IBAction func seg_changed(sender:UIButton!){
        
        if sender.tag == 0{
            job_type = "New"
        }else{
            job_type = "Accepted"
        }
        
        
        if job_type == "New"{
            btn_new.backgroundColor = UIColor(hexString: "2EACE2")
            btn_accepted.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1)
        }else{
            btn_new.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1)
            btn_accepted.backgroundColor = UIColor(hexString: "2EACE2")
        }
        
        
        if job_type == "New" && self.arr_new_jobs.isEmpty{
            no_jobs.isHidden = false
        }else if job_type == "Accepted" && self.arr_accepted_jobs.isEmpty{
            no_jobs.isHidden = false
        }
        else{
            no_jobs.isHidden = true
        }
        
        UIView.transition(with: self.tbl, duration: 0.5, options: .transitionFlipFromRight, animations: {
            self.tbl.reloadData()
        }) { (finish) in
        }
    }
    
    //MARK:- TableView Delegate & Datasource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return arr_jobs.count
        if job_type == "New"{
            return arr_new_jobs.count
        }else{
            return arr_accepted_jobs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl.dequeueReusableCell(withIdentifier: "Cell") as! JobsInProgressCell
          
        //let dict = arr_jobs[indexPath.row]
        var dict = [String:AnyObject]()
        
        if job_type == "New"{
            dict = arr_new_jobs[indexPath.row]
        }else{
            dict = arr_accepted_jobs[indexPath.row]
        }
        
        cell.lbl_pickup.text = dict["pickup"] as? String
        cell.lbl_destination.text = dict["destination"] as? String
        
        cell.lbl_date.text = String(describing: dict["job_date"]!) +  "   "  + String(describing: dict["job_time"]!)
//        cell.lbl_payment.text = dict["payment_type"] as? String
        
        //cell.lbl_flightNo.text = dict["flight_no"] as? String
//        if let flight = dict["flight_no"] as? String, !flight.isEmpty{
//            cell.stack_flight.isHidden = false
//            cell.lbl_flightNo.text = flight
//        }else{
//            cell.stack_flight.isHidden = true
//        }
        
        
//        cell.lbl_fare.text = dict["fare"] as? String
        
        //cell.lbl_VehicleType.text = dict["vehicle_type"] as? String
//        if let vehicle = dict["vehicle_type"] as? String, !vehicle.isEmpty{
//            cell.stack_vehicle.isHidden = false
//            cell.lbl_VehicleType.text = vehicle
//        }else{
//            cell.stack_vehicle.isHidden = true
//        }
        
        let name = dict["name"] as? String ?? ""
        let mobile = dict["mobile"] as? String ?? ""
        let passenger = dict["caller"] as? String ?? ""
        let job_id = dict["job_id"] as? String ?? ""
        let payType = dict["payment_type"] as? String ?? ""
        let fare = dict["fare"] as? String ?? ""
        let job_status = dict["job_status"] as? String ?? ""
        
        cell.lblCustName.text  = name
        
        cell.lblMobile.text  = "mobile : " + mobile

        cell.lblPassenger.text = "Passenger : " + passenger
        
        cell.lblCustName.isHidden  = payType == "Cash"
        
        cell.lblFare.text = "Fare : £ \(fare)"
        
        cell.lblJobId.text = job_id
        
        cell.lblJobStatus.text = "Job Status : \(job_status)"
        
        cell.viewStatus.backgroundColor = self.getJobStatusColor(jobDetails: dict)
        
        let via1 = dict["via_address1"] as? String ?? ""
        let via2 = dict["via_address2"] as? String ?? ""
        let via3 = dict["via_address3"] as? String ?? ""
        
        cell.lblVia1.text = via1
        cell.lblVia2.text = via2
        cell.lblVia3.text = via3
        
        cell.stackVia1.isHidden = via1.isEmptyAfterTrim
        cell.stackVia2.isHidden = via2.isEmptyAfterTrim
        cell.stackVia3.isHidden = via3.isEmptyAfterTrim
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if job_type == "New" {
            
//            let nav = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
//            let storyboard = UIApplication.shared.keyWindow?.rootViewController?.storyboard
//            let NavjobOffervc = storyboard?.instantiateViewController(withIdentifier: "TimerNavJobOfferVC") as! UINavigationController
//            let jobOffervc = NavjobOffervc.topViewController as! TimerNotificationJobVC2
//            jobOffervc.jobID = 1022
//            NavjobOffervc.modalPresentationStyle = .fullScreen
//            nav.present(NavjobOffervc, animated: true, completion: nil)

            self.performSegue(withIdentifier: "GotoAccept", sender: indexPath)
        }else{
            
            let jobProcessVC = JobProcessVC.viewController()
            jobProcessVC.jobDetails = arr_accepted_jobs[indexPath.row]
            self.navigationController?.pushViewController(jobProcessVC, animated: true)
            
            //            self.performSegue(withIdentifier: "ProgresstoJobProcess", sender: indexPath)
        }
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "ProgresstoJobProcess" {
            let indexPath = sender as! IndexPath
            
            let jobprocess = segue.destination as! JobProcessVC
                jobprocess.jobDetails = arr_accepted_jobs[indexPath.row]
            
        }
        else if segue.identifier == "GotoAccept" {
            let indexPath = sender as! IndexPath
            
//            let NavjobOffervc = segue.destination as! UINavigationController
//
//            let jobOffervc = NavjobOffervc.topViewController as! NotificationJobVC2
//            jobOffervc.jobID = 134363

            let NavjobOffervc = segue.destination as! UINavigationController
            let jobOffervc = NavjobOffervc.topViewController as! NotificationJobVC2
                jobOffervc.isFromNotification = false
                jobOffervc.jobDetails = arr_new_jobs[indexPath.row]
        }
    }
    
    
}


extension UIImageView {
    func addshadow(top: Bool, left: Bool, bottom: Bool, right: Bool, shadowRadius: CGFloat = 1.0) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1.0
        
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        // selecting top most point
        path.move(to: CGPoint(x: x, y: y))
        // Move to the Bottom Left Corner, this will cover left edges
        /*
         |☐
         */
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        // Move to the Bottom Right Corner, this will cover bottom edge
        /*
         ☐
         -
         */
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        // Move to the Top Right Corner, this will cover right edge
        /*
         ☐|
         */
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        // Move back to the initial point, this will cover the top edge
        /*
         _
         ☐
         */
        path.close()
        self.layer.shadowPath = path.cgPath
    }
}
