//
//  Voice_msgVC.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 15/03/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

import AVKit

import Alamofire
import PKHUD
import SCLAlertView

class Voice_msgVC: CommonVC,AVAudioRecorderDelegate,AVAudioPlayerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tbl: UITableView!
    
    @IBOutlet weak var lbl_timer : UILabel!
    @IBOutlet weak var btn_record : UIButton!
    
    var progressCircle: CAShapeLayer!
    
    
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var settings         = [String : Int]()
    var meterTimer:Timer!
    var MaxAudioTimer:Timer!
    var audioPlayer : AVAudioPlayer!
    var audioPlayer2 : AVPlayer!
    var soundPlayIndexpath : IndexPath?
    
    var arr_msg_list = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.recordingSetup()
        
        self.get_msgList()
        
        tbl.tableFooterView = UIView(frame:.zero)
    }
    
    
    func recordingSetup(){
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            //try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try
                recordingSession.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
            //            recordingSession?.setCategory(convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord), with: .defaultToSpeaker)
            
            try recordingSession.setActive(true)
            
            recordingSession.requestRecordPermission { (allowed) in
                
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Get All Messages
    func get_msgList(){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]! as AnyObject,"type":"voice_msgs_list" as AnyObject,"driver_id": user_dict["driver_id"]!] as [String : AnyObject]
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as! String == "OK"{
                    if let arr = dict["DATA"] as? [[String:Any]]{
                        self.arr_msg_list = arr as [[String : AnyObject]]
                        self.tbl.reloadData()
                    }
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
        
    }
    
    
    //MARK: Draw Circle
    func drawCircle(){
        
        // var progressCircle = CAShapeLayer()
        progressCircle = CAShapeLayer()
        
        //let centerPoint = CGPoint (x: self.view.frame.size.width / 2, y: self.view.bounds.height / 2)
        let centerPoint = btn_record.center
        
        let circleRadius : CGFloat = 40
        
        let circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true    )
        
        progressCircle = CAShapeLayer ()
        progressCircle.path = circlePath.cgPath
        progressCircle.strokeColor = UIColor.white.cgColor
        progressCircle.fillColor = UIColor.clear.cgColor
        progressCircle.lineWidth = 8
        progressCircle.strokeStart = 0
        progressCircle.strokeEnd = 1.0
        self.view.layer.addSublayer(progressCircle)
        
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1.0
        //animation.duration = 5.0
        // animation.duration = CFTimeInterval(StateController.sharedInstance.durationOfAnalysis)
        animation.duration = CFTimeInterval(MaxAudioLength)
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        progressCircle.add(animation, forKey: "ani")
        
    }
    
    //MARK: Recording
    
    @IBAction func startRecord(){
        
        debugPrint("start record")
        
        //if let aplayer = audioPlayer, aplayer.isPlaying{
        if let player = audioPlayer2, player.rate != 0{
           // audioPlayer.stop()
           // audioPlayer = nil
            audioPlayer2.pause()
            if let tempIndex = soundPlayIndexpath{
                soundPlayIndexpath = nil
                self.tbl.reloadRows(at: [tempIndex], with: .fade)
            }else{
                soundPlayIndexpath = nil
            }
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try!
           audioSession.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)

        do {
            //            let dateformat = DateFormatter()
            //            dateformat.dateFormat = "ddMMyyyyHHmmsss"
            //            _ = dateformat.string(from: Date())
            //let name = Date().timeIntervalSince1970 * 1000.0
            //let souldUrl = documentsURL.appendingPathComponent("AUDIO_\(name)")?.appendingPathExtension("m4a")
            let souldUrl = String(documentsURL.appendingPathComponent("AUDIO.m4a"))
            audioRecorder = try AVAudioRecorder(url: URL(fileURLWithPath: souldUrl),
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record(forDuration: TimeInterval(MaxAudioLength))
            audioRecorder.isMeteringEnabled = true
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
            meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
            MaxAudioTimer = Timer.scheduledTimer(timeInterval: TimeInterval(MaxAudioLength), target:self, selector:#selector(self.stopRecord), userInfo:nil, repeats:false)
            self.drawCircle()
            
        } catch {
        }
    }
    
    
    //MARK:- Timer
    /*
     func stringFromTimeInterval(interval: TimeInterval) -> String {
     let interval = Int(interval)
     let seconds = interval % 60
     let minutes = (interval / 60) % 60
     //let hours = (interval / 3600)
     //return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
     return String(format: "%02d:%02d", minutes, seconds)
     }
     
     @objc func updateLabel(){
     seconds += 1
     lbl_timer.text = self.stringFromTimeInterval(interval: TimeInterval(seconds))
     
     if seconds == MaxAudioLength{
     lbl_timer.text = ""
     timer?.invalidate()
     timer = nil
     }
     }
     */
    
    @objc func updateAudioMeter(timer: Timer) {
        
        if audioRecorder.isRecording {
            //let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            //let totalTimeString = String(format: "%02d:%02d:%02d", hr, min, sec)
            let totalTimeString = String(format: "%02d:%02d", min, sec)
            lbl_timer.text = totalTimeString
            audioRecorder.updateMeters()
        }
    }
    
    
    @IBAction func stopRecord(){
        debugPrint("stop record")
        finishRecording(success: true)
    }
    
    func finishRecording(success: Bool) {
        
        audioRecorder.stop()
        meterTimer.invalidate()
        MaxAudioTimer.invalidate()
        lbl_timer.text = ""
        progressCircle.removeAllAnimations()
        progressCircle.removeFromSuperlayer()
        if success {
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    
    func playAudio(url:String!){
        
        if let aplayer = audioPlayer, aplayer.isPlaying{
            audioPlayer.stop()
            audioPlayer = nil
        }
        //self.audioPlayer = try! AVAudioPlayer(contentsOf: audioRecorder.url)
        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: url))
        self.audioPlayer.prepareToPlay()
        self.audioPlayer.delegate = self
        self.audioPlayer.play()
    }
    
    
    //MARK:- Delegates
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
        else{
            debugPrint(audioRecorder.url)
            self.sendfile_API(url: audioRecorder.url)
        }
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
        if let index = soundPlayIndexpath{
            //self.collectionview.reloadItems(at: [index])
            soundPlayIndexpath = nil
            self.tbl.reloadRows(at: [index], with: .fade)
        }
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
        if let index = soundPlayIndexpath{
            // self.collectionview.reloadItems(at: [index])
            soundPlayIndexpath = nil
            self.tbl.reloadRows(at: [index], with: .fade)
        }
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
        if let index = soundPlayIndexpath{
            // self.collectionview.reloadItems(at: [index])
            soundPlayIndexpath = nil
            self.tbl.reloadRows(at: [index], with: .fade)
        }
    }
    
    
    //MARK:- Send audio file
    
    func sendfile_API(url:URL!){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let data =  try! Data(contentsOf: url!)
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let param = ["office_name":user_dict["office_name"]!,"driver_id": user_dict["driver_id"]!,"file_name":url.lastPathComponent] as [String : AnyObject]
        
        
        
         Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            multipartFormData.append(data, withName: "file", fileName: url.lastPathComponent, mimeType: "audio/mpeg")
            debugPrint(multipartFormData)
        }, usingThreshold:  UInt64.init(), to: "http://aws.tbmslive.com/taxi_app/WebServices/WSv7.php?type=send_voice_msg", method: .post, headers: nil) { (result) in
 
        /*
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            multipartFormData.append(data, withName: "file", fileName: url.lastPathComponent, mimeType: "audio/mpeg")
            debugPrint(multipartFormData)
        }, usingThreshold:  UInt64.init(), to: "\(BASE_URL)?type=send_voice_msg", method: .post, headers: nil) { (result) in
         */
            //HUD.hide()
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseString(completionHandler: { (response) in
                    debugPrint(response)
                })
                
                upload.responseJSON { response in
                    debugPrint(response)
                    if let dict = response.result.value as? [String:AnyObject]{
                        if let data_dict = dict["DATA"] as? [String:AnyObject],!data_dict.isEmpty{
                            self.arr_msg_list.append(data_dict)
                            self.tbl.beginUpdates()
                            self.tbl.insertRows(at: [IndexPath(row: self.arr_msg_list.count - 1, section: 0)], with: .automatic)
                            HUD.hide()
                            self.tbl.endUpdates()
                        }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                HUD.hide()
            }
        }
        
    }
    
    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_msg_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Voice_cell") as! VoiceCell
        
        let dict = arr_msg_list[indexPath.row]
        
        cell.lbl_time.text = dict["msg_date"] as? String ?? ""
        
        let from = dict["from_msg"] as? String ?? ""
        
        if from == "Operator"{
            
            let image = #imageLiteral(resourceName: "bubble_received")
            cell.imag.image = image.resizableImage(withCapInsets: UIEdgeInsets.init(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch)
            cell.img_play.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.lbl_time.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            cell.cons_lead_imag.constant = 8.0
            cell.cons_trail_imag.constant = 70.0
            
            
            cell.cons_lead_content.constant = 30.0
            cell.cons_trail_content.constant = 100.0
            
        }else{
            let image = #imageLiteral(resourceName: "bubble_sent")
            cell.imag.image = image.resizableImage(withCapInsets: UIEdgeInsets.init(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch)
            cell.img_play.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.lbl_time.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            cell.cons_lead_imag.constant = 70.0
            cell.cons_trail_imag.constant = 8.0
            
            cell.cons_lead_content.constant = 90.0
            cell.cons_trail_content.constant = 30.0
        }
        
        if let ind = soundPlayIndexpath, ind == indexPath{
            cell.img_play.image = #imageLiteral(resourceName: "stop_audio")
        }else{
            cell.img_play.image = #imageLiteral(resourceName: "play")
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //if let aplayer = audioPlayer, aplayer.isPlaying , soundPlayIndexpath == indexPath{
        if let avplayer = audioPlayer2, avplayer.rate != 0{
            if soundPlayIndexpath == indexPath{
            audioPlayer2.pause()
            soundPlayIndexpath = nil
            }else{
                let temp_index = soundPlayIndexpath
                soundPlayIndexpath = indexPath
                let dict = self.arr_msg_list[indexPath.row]
                audioPlayer2 = AVPlayer.init(playerItem: AVPlayerItem(url: URL(fileURLWithPath: dict["msg_file"] as! String)))
                audioPlayer2.play()
                self.tbl.reloadRows(at: [temp_index!], with: .fade)
            }
            //self.tbl.reloadRows(at: [indexPath], with: .fade)
            
        }else{
            soundPlayIndexpath = indexPath
            let dict = self.arr_msg_list[indexPath.row]
            audioPlayer2 = AVPlayer.init(playerItem: AVPlayerItem(url: URL(fileURLWithPath: dict["msg_file"] as! String)))
            audioPlayer2.play()
        }
        
        self.tbl.reloadRows(at: [indexPath], with: .fade)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
