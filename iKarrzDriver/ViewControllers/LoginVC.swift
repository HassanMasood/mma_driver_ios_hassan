//
//  LoginVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 05/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import MessageUI

import SCLAlertView
import Alamofire
import PKHUD
import CoreLocation

//class LoginVC: UIViewController, UITextFieldDelegate {
class LoginVC: CommonVC, UITextFieldDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var txt_office_name:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    
    @IBOutlet weak var lblVersion: UILabel!
    
    @IBOutlet weak var btn_remember:UIButton!
    
    @IBOutlet weak var btnLogin:UIButton!
    
    @IBOutlet weak var txt_forgot_office_name:UITextField!
    @IBOutlet weak var txt_forgot_email:UITextField!
    @IBOutlet weak var view_forgotPassword : UIView!
    @IBOutlet weak var view_forgotPassword_inner : UIView!
    
    @IBOutlet weak var txt_register_email:UITextField!
    @IBOutlet weak var txt_register_phone:UITextField!
    @IBOutlet weak var view_register : UIView!
    @IBOutlet weak var view_register_inner : UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txt_email.text = appDelegate.deviceToken
        self.setupButtonUI(for: btnLogin)
        self.setupView(viewT: self.viewEmail)
        self.setupView(viewT: self.viewPassword)
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        if let dict = UserDefaults.standard.value(forKey: "Login_Info") as? [String:AnyObject]{
            txt_office_name.text = dict["office_name"] as? String
            txt_email.text = dict["user_name"] as? String
            txt_password.text = dict["password"] as? String
            
            btn_remember.isSelected = true
            //btn_remember.setImage(UIImage(), for: <#T##UIControlState#>)
        }
        
        self.txt_office_name.text = "ikarrz"
        
        view_forgotPassword_inner.backgroundColor = self.view.backgroundColor
        view_register_inner.backgroundColor = self.view.backgroundColor
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        self.lblVersion.text = "Version : \(appVersion)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        self.title = ""
        
        //BASE_URL = "http://tbmslive.com/taxi_app/WebServices/WSv7.php"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Validation
    
    func Validation() -> (msg:String,isvalid:Bool){
        
        if txt_office_name.text!.isEmpty{
            return ("Office name must not be empty.",false)
        }
        if txt_email.text!.isEmpty{
            return ("User name must not be empty.",false)
        }
        if txt_password.text!.isEmpty{
            return ("Password must not be empty.",false)
        }
        
        return("Valid",true)
    }
    
    //----------------------------------------
    
    func showAlertForLocationPrivacy() {
        
        let alert = UIAlertController.init(title: AppName, message: "Please set location permission to always.", preferredStyle: .alert)
        
        let actionSetting = UIAlertAction.init(title: "Settings", style: .default) { (setting) in
            
            if let url = URL(string:UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
        }
        
        let actionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (setting) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(actionSetting)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //----------------------------------------
    
    //MARK:- Login Button
    @IBAction func Login(){
        
        if btn_remember.isSelected{
            let dict = ["office_name":txt_office_name.text!,"user_name":txt_email.text!,"password":txt_password.text!]
            
            UserDefaults.standard.set(dict, forKey: "Login_Info")
        }
        
        self.view.endEditing(true)
        
        let check = Validation()
        
        if check.isvalid{
            
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                    
//                    SCLAlertView().showError(AppName, subTitle: "Location services are not enabled.")
                    SCLAlertView().showError(AppName, subTitle: locationDeniedMessage)
                    
                    return
                case .authorizedWhenInUse:
                    self.showAlertForLocationPrivacy()
                    print("When in use")
                    return
                case .authorizedAlways:
                    print("Access")
                }
            } else {
                print("Location services are not enabled")
                SCLAlertView().showError(AppName, subTitle: locationDeniedMessage)
                return
            }
            
            if appDelegate.reachability.connection == .unavailable{
                SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
                return
            }
            
            HUD.show(.progress)
            
            let parameter  = ["office_name":txt_office_name.text!,"type":"driver_login","username": txt_email.text!,"password": txt_password.text!]
            //let parameter  = ["office_name":txt_office_name.text!,"type":"driver_login_link","username": txt_email.text!,"password": txt_password.text!]
            debugPrint(parameter)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                
                switch response.result {
                case .success:
                    //debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["DATA"]?["msg"] as! String == "Login Success"{
                        
                        if dict["DATA"] !=  nil{
                            var dict_user = dict["DATA"] as! [String:AnyObject]
                            dict_user["user_name"] = self.txt_email.text! as AnyObject
                            dict_user["office_name"] = self.txt_office_name.text! as AnyObject
                            
                            
                            if let driverID = dict["DATA"]!["driver_id"] as? String {
                                
                                self.updateDeviceToken(DriverID: driverID)
                                dict_user["driver_id"] = driverID as AnyObject
                            }
                            if let phonenumber = dict["DATA"]!["phonenumber"] as? String{
                                dict_user["phonenumber"] = phonenumber as AnyObject
                            }
                            
                            if let driverName = dict["DATA"]!["name"] as? String{
                                dict_user["name"] = driverName as AnyObject
                            }
                            
                            if let loginId = dict["DATA"]!["loginid"] as? String{
                                dict_user["loginid"] = loginId as AnyObject
                            }
                            
                            if let driver_rating = dict["DATA"]!["driver_rating"] as? Double {
                                dict_user["driver_rating"] = driver_rating as AnyObject
                            }
                            
                            if let pay_type_change = dict["DATA"]!["pay_type_change"] as? String{
                                dict_user["pay_type_change"] = pay_type_change as AnyObject
                            }

                            let keysToRemove = dict_user.keys.filter({ (key) -> Bool in
                                if (dict_user[key] as? NSNull) != nil {
                                    return true
                                }
                                return false
                            })
                            
                            for key in keysToRemove {
                                dict_user[key] = "" as AnyObject
                            }

                            UserDefaults.standard.set(dict_user, forKey: "User_Info")

                            
                            
                            if let driverID = dict["DATA"]!["driver_id"] as? String {
                                
                                self.updateDeviceToken(DriverID: driverID)
                                dict_user["driver_id"] = driverID as AnyObject
                            }
                            if let driverID = dict["DATA"]!["phonenumber"] as? String{
                                dict_user["phonenumber"] = driverID as AnyObject
                            }
                            
                            UserDefaults.standard.set(dict_user, forKey: "User_Info")
                            //UserDefaults.standard.set("Yes", forKey: "isLoggedIn")
                        }
                        
                        
                        DispatchQueue.main.async(execute: {
                            
                            //self.performSegue(withIdentifier: "GotoDashboard", sender: self)
                            //appDelegate.changeRootViewController(with: "DashboardMenu")
                            self.performSegue(withIdentifier: "GotoConfirmation", sender: self)
                        })
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                        })
                    }
                    
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                    
                }
            })
        }
        else{
            SCLAlertView().showError(AppName, subTitle: check.msg)
        }
    }
    
    
    
    
    //MARK: Update Device Token
    func updateDeviceToken(DriverID:String!){
        
        if appDelegate.reachability.connection == .unavailable{
            //SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        print("Device token : ==>> \(appDelegate.deviceToken)")
        
        let parameter  = ["office_name":txt_office_name.text! as AnyObject,"type":"add_device_token_iphone" as AnyObject,"user_id":DriverID as AnyObject,"user_type": "Driver" as AnyObject,"device_token": appDelegate.deviceToken as AnyObject] as [String : AnyObject]
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["RESULT"] as! String == "OK"{
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    
    //MARK: Remember Me
    @IBAction func remember_me(sender:UIButton!){
        
        if sender.isSelected{
            
            sender.setImage(UIImage(named: "uncheck"), for: .normal)
            UserDefaults.standard.set("", forKey: "Login_Info")
            
        }else{
            sender.setImage(UIImage(named: "check"), for: .selected)
            
            let dict = ["office_name":txt_office_name.text!,"user_name":txt_email.text!,"password":txt_password.text!]
            
            UserDefaults.standard.set(dict, forKey: "Login_Info")
        }
        sender.isSelected = !sender.isSelected
        
    }
    
    //MARK:- TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    // MARK:- Forgot password view
    
    @IBAction func forgot_password (sender:UIButton) {
        
        self.view.endEditing(true)
        view_forgotPassword.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        view_forgotPassword.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_forgotPassword.alpha = 1.0
            self.view_forgotPassword.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
    }
    
    
    
    @IBAction func cancel_forgot_passwrod (sender:UIButton) {
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_forgotPassword.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_forgotPassword.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    @IBAction func send_forgot_passwrod (sender:UIButton) {
        
        self.view.endEditing(true)
        
        if txt_forgot_office_name.text!.isEmpty {
            SCLAlertView().showError(AppName, subTitle: "Office name must not be empty.")
        }
        else if txt_forgot_email.text!.isEmpty {
            SCLAlertView().showError(AppName, subTitle: "Email must not be empty.")
        }
        else if !txt_forgot_email.text!.isValidEmail {
            SCLAlertView().showError(AppName, subTitle: "Email seems invalid.")
        }
            
        else{
            
            if appDelegate.reachability.connection == .unavailable{
                SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
                return
            }
            
            HUD.show(.progress)
            
            let parameter  = ["type":"driver_forgotpass","office_name":txt_forgot_office_name.text! ,"email": txt_forgot_email.text!]
            // print(parameter)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                HUD.hide()
                
                switch response.result {
                case .success:
                    //debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    
                    if dict["DATA"]?["msg"] as! String == "Username and password sent on your email id."{
                        
                        if dict["DATA"] !=  nil{
                        }
                        
                        DispatchQueue.main.async(execute: {
                            UIView.animate(withDuration: 0.25, animations: {
                                self.view_forgotPassword.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                                self.view_forgotPassword.alpha = 0.0
                            }, completion:{(finished : Bool)  in
                                if (finished)
                                {
                                    SCLAlertView().showSuccess(AppName, subTitle: "Username and password sent on your email id.")
                                    self.txt_forgot_email.text = ""
                                    self.txt_forgot_office_name.text = ""
                                }
                            })
                            
                        })
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                        })
                    }
                    
                    
                case .failure(let error):
                    //print(error)
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: error.localizedDescription)
                    })
                    
                }
            })
            
            
            
        }
        
    }
    
    
    //MARK:- SignUp
    
    @IBAction func signUp(){
        self.view.endEditing(true)
        view_register.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        view_register.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_register.alpha = 1.0
            self.view_register.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @IBAction func cancel_register (sender:UIButton) {
        
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_register.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_register.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    @IBAction func send_register (sender:UIButton) {
        
        self.view.endEditing(true)
        
        if txt_register_email.text!.isEmpty {
            SCLAlertView().showError(AppName, subTitle: "Email must not be empty.")
        }
        else if !txt_register_email.text!.isValidEmail {
            SCLAlertView().showError(AppName, subTitle: "Email seems invalid.")
        }
        else if txt_register_phone.text!.isEmpty {
            SCLAlertView().showError(AppName, subTitle: "Phone number must not be empty.")
        }
            
        else if !MFMailComposeViewController.canSendMail(){
            SCLAlertView().showError(AppName, subTitle: "Email is not avaible,please confirm that Email Account is set up on this device.")
        }
        else{
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("Register TBMS Driver Trial")
            mail.setToRecipients([EmailID])
            let body = """
            Enquiry: Please provide me trial credentials to demo TBMS Driver
            Email : \(txt_register_email.text!)
            Phone:  \(txt_register_phone.text!)
            """
            mail.setMessageBody(body, isHTML: false)
            
            present(mail, animated: true)
        }
        
    }
    
    //MARK:- Mail Composer
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled:
            SCLAlertView().showError(AppName, subTitle: "Email is Cancelled")
        case .failed:
            SCLAlertView().showError(AppName, subTitle: "Email is Failed")
        case .saved:
            SCLAlertView().showInfo(AppName, subTitle: "Email is Saved")
        case .sent:
            SCLAlertView().showSuccess(AppName, subTitle: "Email is sent")
        }
        controller.dismiss(animated: true)
        
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
