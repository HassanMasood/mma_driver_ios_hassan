//
//  SignupVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 03/09/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit
import WebKit

import PKHUD
import SCLAlertView

class SignupVC: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var temp_view : UIView!
    
    var webView : WKWebView!
    
    var signupURL = ""
    var titleStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupData()
        //self.webView.automaticallyAdjustsScrollViewInsets = false
        self.title = titleStr
    }
    
    func setupData(){
        
        //let signupURL = "http://www.ecabbi.com/join-now.php"
        let url = URL(string: signupURL)
        let request = URLRequest(url: url! as URL)
        
        webView = WKWebView(frame: self.view.frame)
        print(temp_view.frame)
        print(webView.frame)
        webView.navigationDelegate = self
        webView.load(request)
        self.view.addSubview(webView)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // init and load request in webview.
        webView.frame = self.temp_view.frame
        
        //self.view.sendSubview(toBack: webView)
    }
    
    //MARK:- Done button
    
    @IBAction func done_press(){
        self.dismiss(animated: true) { 
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- WebView Delegate
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        HUD.hide()
        let alert = SCLAlertView()
        alert.addButton("Retry") { 
            webView.reload()
        }
        SCLAlertView().showError(AppName, subTitle: error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load \(String(describing: webView.url))")
        HUD.show(.progress)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        HUD.hide()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
