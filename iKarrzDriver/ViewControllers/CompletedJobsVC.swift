//
//  CompletedJobsVC.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 14/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import Alamofire
import PKHUD
import SCLAlertView

//import SimplePDF
import TPPDF

class CompletedJobsVC: CommonVC, UITextFieldDelegate, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var viewTransparent:UIView!
    @IBOutlet weak var viewEarnings:UIView!
    @IBOutlet weak var btnOk:UIButton!
    
    @IBOutlet weak var lblDateRange : UILabel!
    @IBOutlet weak var lblCash : UILabel!
    @IBOutlet weak var lblCard : UILabel!
    @IBOutlet weak var lblAccount : UILabel!
    @IBOutlet weak var lblTotalWork : UILabel!
    @IBOutlet weak var lblPercentage : UILabel!
    @IBOutlet weak var lblDriverWage : UILabel!

    @IBOutlet weak var txt_from : CustomTextField!
    @IBOutlet weak var txt_to : CustomTextField!
    
    var selectedTextField : UITextField?

    @IBOutlet weak var view_date:UIView!
    @IBOutlet weak var pic_date:UIDatePicker!
    
    @IBOutlet weak var tbl : UITableView!
    @IBOutlet weak var lbl_jobsCount : UILabel!
    @IBOutlet weak var lbl_TotalEarnings : UILabel!
    @IBOutlet weak var btnShowJobs : UIButton!
    
    var arr_jobs = [[String:AnyObject]]()
    let refreshControl = UIRefreshControl()
    
    
    var documentController : UIDocumentInteractionController!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.SetupData()
        
        self.getJobs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SetupData
    
    func  SetupData() {
        
        tbl.estimatedRowHeight = 44.0
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView(frame: CGRect.zero)
        
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tbl.refreshControl = refreshControl
        
        var dayComp = DateComponents()
        dayComp.year = -1
        let date = Calendar.current.date(byAdding: dayComp, to: Date())
        pic_date.minimumDate = date
        pic_date.maximumDate = Date() 
        
        //        txt_from.text = NSDate.toString(date: NSDate(), format: "dd MMM yyyy")
        //        txt_to.text = NSDate.toString(date: NSDate(), format: "dd MMM yyyy")
        txt_from.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        txt_to.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        
        self.btnShowJobs.layer.cornerRadius = 20
        self.btnShowJobs.layer.borderWidth = 2
        self.btnShowJobs.layer.borderColor = UIColor.white.cgColor
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(viewTransparentTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.viewTransparent.addGestureRecognizer(tapGesture)

    }

    //----------------------------------------
    
    func showEarningsData(fromDate: String, toDate: String, dict: [String: Any]) {
        
        let strDateRange = "Date range : \(fromDate) to \(toDate)"
        self.lblDateRange.text = strDateRange
        
        let total_cash = dict["total_cash"] as? String ?? ""
        self.lblCash.text = "Cash : \(self.roundedValue(str: total_cash))"
        
        let total_card = dict["total_card"] as? String ?? ""
        self.lblCard.text = "Card : \(self.roundedValue(str: total_card))"
        
        let total_account = dict["total_account"] as? String ?? ""
        self.lblAccount.text = "Account : \(self.roundedValue(str: total_account))"
        
        let percentage = dict["percentage"] as? String ?? ""
        self.lblPercentage.text = "Percentage : \(percentage) %"
        
        let total_work = dict["total_work"] as? Double ?? 0
        self.lblTotalWork.text = "Total work : \(total_work)"

        let driver_wage = dict["driver_wage"] as? Double ?? 0
        self.lblDriverWage.text = "Driver wage : \(driver_wage)"
    }
    
    //MARK: Today Reset Date
    
    func roundedValue(str : String)-> String {
        
        var strvalue = ""
        
        if str != "" {
            strvalue = String(format: "%.2f", Double(str) ?? 0)
        }
        return strvalue
    }
    
    //---------------------------------------------
    
    @objc func viewTransparentTapped(_ sender: UITapGestureRecognizer) {
        self.viewEarnings.isHidden = true
        self.viewTransparent.isHidden = true
        self.btnOk.isHidden = true
    }
    
    //------------------------------------------
    
    @IBAction func btnEarningTapped(_ sender: UIButton) {
        self.viewEarnings.isHidden = false
        self.viewTransparent.isHidden = false
        self.btnOk.isHidden = false
        self.callWSToGetEarningsData()
    }
    
    //------------------------------------------
    
    @IBAction func btnOkTapped(_ sender: UIButton) {
        self.viewEarnings.isHidden = true
        self.viewTransparent.isHidden = true
        self.btnOk.isHidden = true
    }
    
    @IBAction func todayResetDates(){
        
        txt_from.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        txt_to.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        
        self.getJobs()
    }
    
    //----------------------------------------
    
    func callWSToGetEarningsData() {
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        //let fromdate = NSDate.parse(dateString: txt_from.text!, format: "dd MMM yyyy")
        let fromdate = Date().datefromString(string: txt_from.text!, dateFormat: "dd MMM yyyy")
        let str_fromDate = fromdate.stringWithFormatter(dateFormat: "dd-MM-yyyy")
        
        //let todate = NSDate.parse(dateString: txt_to.text!, format: "dd MMM yyyy")
        let todate = Date().datefromString(string: txt_to.text!, dateFormat: "dd MMM yyyy")
        let str_toDate = todate.stringWithFormatter(dateFormat: "dd-MM-yyyy")
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"driver_earning_break_down",
                          "office_name":user_dict["office_name"] as? String ?? "",
                          "driver_id": user_dict["driver_id"] as? String ?? "",
                          "from_date":str_fromDate,
                          "to_date":str_toDate] as [String : Any]
        
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            HUD.hide()
            
            switch response.result {
            case .success:
                
                guard let responseData = response.result.value as? [String:AnyObject] else {
                    return
                }
                guard let dictData = responseData["DATA"] as? [String:AnyObject] else {
                    return
                }
                
                self.showEarningsData(fromDate: str_fromDate, toDate: str_toDate, dict: dictData)
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    //----------------------------------------
    
    //MARK:- TextField Delegate
    
    func  textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTextField = textField
        
        self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_date.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_date.alpha = 1.0
            self.view_date.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        return false
    }
    
    //----------------------------------------
    
    @IBAction func btnShowJobsTspped(_ sender: UIButton){
        self.getJobs()
    }
    
    //----------------------------------------
    
    //MARK:- Show Date Picker
    @IBAction func showDatePicker(sender:UIButton!){
        
        selectedTextField = (sender.tag == 0) ? txt_from : txt_to
        
        self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_date.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_date.alpha = 1.0
            self.view_date.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    //MARk:- Toolbar action
    
    @IBAction func cancel(sender:UIBarButtonItem!){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_date.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    @IBAction func Selected_datePicker(sender:UIBarButtonItem!){
        
        if selectedTextField == txt_from{
            let date = pic_date.date
           // txt_from.text = NSDate.toString(date: date as NSDate, format: "dd MMM yyyy")
            txt_from.text = date.stringWithFormatter(dateFormat: "dd MMM yyyy")
        }
        else if selectedTextField == txt_to{
            let date = pic_date.date
            txt_to.text = date.stringWithFormatter(dateFormat: "dd MMM yyyy")
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_date.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
        
        self.getJobs()
    }
    
    //MARK:-  Get Data
    
    
    func getJobs(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            if isRefresh{
                self.refreshControl.endRefreshing()
            }
            return
        }
        
        if !isRefresh{
        HUD.show(.progress)
        }
        
        //let fromdate = NSDate.parse(dateString: txt_from.text!, format: "dd MMM yyyy")
        let fromdate = Date().datefromString(string: txt_from.text!, dateFormat: "dd MMM yyyy")
        let str_fromDate = fromdate.stringWithFormatter(dateFormat: "dd-MM-yyyy")
        
        //let todate = NSDate.parse(dateString: txt_to.text!, format: "dd MMM yyyy")
        let todate = Date().datefromString(string: txt_to.text!, dateFormat: "dd MMM yyyy")
        let str_toDate = todate.stringWithFormatter(dateFormat: "dd-MM-yyyy")
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_completed_jobs", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"date_from":str_fromDate,"date_to":str_toDate] as [String : Any]
        debugPrint(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            self.refreshControl.endRefreshing()
            //print(response.result.value!)
            switch response.result {
            case .success:
               // debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                self.arr_jobs  = dict["DATA"] as! [[String:AnyObject]]
                if self.arr_jobs[0]["msg"] as! String ==  "Completed Jobs"{
                    self.arr_jobs.remove(at: 0)
                    
                    DispatchQueue.main.async(execute: {
                        //self.tbl.isHidden = false
                        self.tbl.reloadData()
                        self.lbl_jobsCount.text = String(self.arr_jobs.count) + " " + "Jobs"
                        //self.lbl_TotalEarnings.text = "£ " + String(describing: dict["totalearning"]!)
                        self.lbl_TotalEarnings.text = dict["totalearning"] as? String
                    })
                    
                }
                else{
                    
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: self.arr_jobs[0]["msg"] as! String)
                        self.lbl_jobsCount.text =  "0 " + "Jobs"
                        self.lbl_TotalEarnings.text = ""
                        //self.tbl.isHidden = true
                        self.arr_jobs.removeAll()
                        self.tbl.reloadData()
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })

    }
    
    //MARK:- Pull To Refresh
    
    @objc func refreshData(){
        self.getJobs(isRefresh: true)
    }
    
    /*
    @objc func refreshdata(sender: UIRefreshControl) {
        // Perform actions to refresh the content
        // ...
        // and then dismiss the control
        
        if appDelegate.reachability.connection == .none{
            sender.endRefreshing()
            return
        }
        
       // HUD.show(.progress)
        
        let fromdate = NSDate.parse(dateString: txt_from.text!, format: "dd MMM yyyy")
        let str_fromDate = NSDate.toString(date: fromdate, format: "dd-MM-yyyy")
        
        let todate = NSDate.parse(dateString: txt_to.text!, format: "dd MMM yyyy")
        let str_toDate = NSDate.toString(date: todate, format: "dd-MM-yyyy")

        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        //let parameter  = ["type":"driver_completed_jobs", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"date_from":txt_from.text!,"date_to":txt_to.text!] as [String : Any]
        let parameter  = ["type":"driver_completed_jobs", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"date_from":str_fromDate,"date_to":str_toDate] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
           // HUD.hide()
            sender.endRefreshing()
            //print(response.result.value!)
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                self.arr_jobs  = dict["DATA"] as! [[String:AnyObject]]
                if self.arr_jobs[0]["msg"] as! String ==  "Completed Jobs"{
                    self.arr_jobs.remove(at: 0)
                    
                    DispatchQueue.main.async(execute: {
                        //self.tbl.isHidden = false
                        self.tbl.reloadData()
                        self.lbl_jobsCount.text = String(self.arr_jobs.count) + " " + "Jobs"
                        self.lbl_TotalEarnings.text = "£ " + String(describing: dict["totalearning"]!)
                    })
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //self.tbl.isHidden = true
                        //SCLAlertView().showError(AppName, subTitle: self.arr_jobs[0]["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:self.arr_jobs[0]["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                })
            }
        })

    }
 */

    
    //MARK:- TableView Delegate & Datasource

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_jobs.count
    }
    
    //----------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl.dequeueReusableCell(withIdentifier: "Cell") as! JobsInProgressCell
                
        let dict = arr_jobs[indexPath.row]
        
        cell.lbl_pickup.text = dict["pickup"] as? String
        cell.lbl_destination.text = dict["destination"] as? String
        
        cell.lbl_date.text = String(describing: dict["job_date"]!) +  "   "  + String(describing: dict["job_time"]!)

//        if let flight = dict["flight_no"] as? String, !flight.isEmpty{
//            cell.stack_flight.isHidden = false
//            cell.lbl_flightNo.text = flight
//        }else{
//            cell.stack_flight.isHidden = true
//        }
        
        let name = dict["name"] as? String ?? ""
        let mobile = dict["mobile"] as? String ?? ""
        let passenger = dict["caller"] as? String ?? ""
        let job_id = dict["job_id"] as? String ?? ""
        let payType = dict["payment_type"] as? String ?? ""
        let fare = dict["fare"] as? String ?? ""
        let job_status = dict["job_status"] as? String ?? ""
        
        cell.lblCustName.text  = name
        
        cell.lblMobile.text  = "mobile : " + mobile
        
        cell.lblMobile.isHidden = mobile.isEmptyAfterTrim
        
        cell.lblPassenger.text = "Passenger : " + passenger
        
        cell.lblCustName.isHidden  = payType == "Cash"
        
        cell.lblFare.text = "Fare : \(fare)"
        
        cell.lblJobId.text = job_id
        
        cell.lblJobStatus.text = "Job Status : \(job_status)"
                
        cell.lblJobStatus.isHidden = job_status.isEmptyAfterTrim

        let via1 = dict["via_address1"] as? String ?? ""
        let via2 = dict["via_address2"] as? String ?? ""
        let via3 = dict["via_address3"] as? String ?? ""
        
        cell.lblVia1.text = via1
        cell.lblVia2.text = via2
        cell.lblVia3.text = via3
        
        cell.stackVia1.isHidden = via1.isEmptyAfterTrim
        cell.stackVia2.isHidden = via2.isEmptyAfterTrim
        cell.stackVia3.isHidden = via3.isEmptyAfterTrim
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "GotoDetails", sender: indexPath)
    }
    
    //MARK:- PDF 
    /*
    @IBAction func pdfGenerate(){
        
        let A4paperSize = CGSize(width: 595, height: 842)
        let pdf = SimplePDF(pageSize: A4paperSize)
        
        pdf.setContentAlignment(.center)
        
        pdf.addImage(#imageLiteral(resourceName: "logo"))
        pdf.addText("Ecabbi1")
        
        pdf.addLineSpace(30)
        pdf.addLineSeparator()
        pdf.addLineSpace(30)
        
        pdf.addText("Completed Jobs")
        
        pdf.addLineSpace(20)
        
        let dataArray = [["Job Reference", "Date","Time","Pickup","DropOff","Fare"],["1", "1","1", "abcdefghijklmnopqrstuvwxyz","1", "1"],["2", "2","2", "2","2", "2"],["3", "3","3", "3","3", "3"],["4", "4","4", "4","4", "4"]]
        pdf.addTable(dataArray.count, columnCount: 6, rowHeight: 50.0, columnWidth: 90.0, tableLineWidth: 1.0, font: UIFont.systemFont(ofSize: 10.0), dataArray: dataArray)
        
        let pdfData = pdf.generatePDFdata()
        
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("ecabbi.pdf")
        
        do {
            try pdfData.write(to: fileURL, options: .atomic)
        } catch {
            print(error)
        }
    }
    */
    
    @IBAction func pdfGenerate(sender:UIButton!){
        
        if arr_jobs.isEmpty{
            SCLAlertView().showError(AppName, subTitle: "No jobs to export data.")
            return
        }
        
        
        //let pdf = PDFGenerator(format: .a4)
        
        let pdf = PDFDocument(format: .a4)
        
       // let pdf = PDFGenerator(document: doc)
        
        pdf.addImage(.contentCenter, image: PDFImage(image: #imageLiteral(resourceName: "logo")))
        pdf.addText(.contentCenter, text: AppName, lineSpacing: 1.0)
        //pdf.addAttributedText(.contentCenter, text: NSAttributedString(string: "Ecabbi", attributes: [NSFontAttributeName:UIFont.boldSystemFont(ofSize: 20.0)]))
        
        pdf.addSpace(space: 20.0)
        pdf.addLineSeparator(style: .init(type: .dashed, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), width: 1.0))
        
        //pdf.addLineSeparator(.contentCenter, style: LineStyle(type: .dashed, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), width: 2.0))
        //pdf.addAttributedText(.contentCenter, text: NSAttributedString(string: "Completed Jobs", attributes: [NSFontAttributeName:UIFont.boldSystemFont(ofSize: 12.0)]))
        pdf.addSpace(space: 20.0)
        pdf.addText(.contentCenter, text: "Completed Jobs", lineSpacing: 1.0)
        
        pdf.addSpace(space: 20.0)
        
         var dataArray = [["Job ID", "Date","Time","Pickup","DropOff","Fare"]]
        
        for dict in arr_jobs{
             var job = [String]()
            job.append(String(describing: dict["job_id"]!))
            job.append(String(describing: dict["job_date"]!))
            job.append(String(describing: dict["job_time"]!))
            job.append(String(describing: dict["pickup"]!))
            job.append(String(describing: dict["destination"]!))
            job.append(String(describing: dict["fare"]!))
            
            dataArray.append(job)
        }
        
       // let tableAlignment: [[TableCellAlignment]] = [[.center, .center, .center, .center,.center,.center],[.center, .center, .center, .center, .center, .center],[.center, .center, .center, .center, .center, .center],[.center, .center, .center, .center, .center, .center],[.center, .center, .center, .center, .center, .center]]
        
        let tableAlignment: [[PDFTableCellAlignment]] = dataArray.map({ _ in [.center, .center, .center, .center,.center,.center]})
        
        let tableWidth: [CGFloat] = [
            0.08, 0.2,0.1, 0.25, 0.25,0.1
        ]
        
        let tableStyle = PDFTableStyleDefaults.simple
        //pdf.addTable(data: dataArray, alignment: tableAlignment, relativeColumnWidth: tableWidth, padding: 0, margin: 0, style: tableStyle)
        let pdftable = PDFTable()
        pdftable.widths = tableWidth
        pdftable.margin = 0
        pdftable.style = tableStyle
        pdftable.padding = 0
        do{
           try pdftable.generateCells(data: dataArray, alignments: tableAlignment)
            pdf.addTable(table: pdftable)
        }catch{
            //print("could not start reachability notifier")
        }
        
        
        pdf.addSpace(space: 20.0)
        if let str = self.lbl_TotalEarnings.text{
        pdf.addText(.contentRight, text: "TOTAL:  \(String(describing: str))", lineSpacing: 1.0)
    }
        
        
        //let url = pdf.generatePDFfile("Ecabbi")
//        do{
//           let url = try PDFGenerator.generateURL(document: pdf, filename: AppName)
//            print(url)
//            documentController = UIDocumentInteractionController(url: url)
//            documentController.presentOptionsMenu(from: sender.frame, in: self.view, animated: true)
//        }
//        catch{
//        }

        
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "GotoDetails"{
            let index = sender as! IndexPath
            let jobDetails = segue.destination as! JobDetailsVC2
            let dict = arr_jobs[index.row]
            jobDetails.jobDetails = dict
            
        }
    }
    

}
