//
//  ConfirmationVC.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 07/04/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

import SCLAlertView
import PKHUD

class ConfirmationVC: CommonVC {

    @IBOutlet weak var stack_expiry_lbl : UIStackView!
    @IBOutlet weak var stack_expiry : UIStackView!
    
    @IBOutlet weak var img_door : UIImageView!
    @IBOutlet weak var img_insurance : UIImageView!
    @IBOutlet weak var img_fire : UIImageView!
    @IBOutlet weak var img_wash : UIImageView!
    @IBOutlet weak var img_tyre : UIImageView!
    
    @IBOutlet weak var img_TabiPad : UIImageView!
    @IBOutlet weak var img_Water : UIImageView!
    @IBOutlet weak var img_Tissue : UIImageView!
    @IBOutlet weak var img_FitToDrive : UIImageView!
    
    @IBOutlet weak var btnConfirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupButtonUI(for: self.btnConfirm)
        // Do any additional setup after loading the view.
        self.setupData()
        self.getExpiryDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupData(){
        
        
        
    }
    
    //MARK:- Get Expiry
    func getExpiryDetails(){
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["office_name":user_dict["office_name"]!,"type":"expire_driver","driver_id": user_dict["driver_id"]!] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            //print(response.result.value!)
            switch response.result {
            case .success:
                let dict = response.result.value as! [String:AnyObject]
                
                if let msg  = dict["DATA"]?["msg"] as? String , msg == "Record Founded"{
                    
                    DispatchQueue.main.async(execute: {
                        self.stack_expiry_lbl.isHidden = false
                        self.stack_expiry.isHidden = false
                        self.setup_expiry_Data(dict: dict["DATA"] as! [String:String])
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                    })
                }
                
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
                
            }
        })
    }
    
    func setup_expiry_Data(dict:[String:String]!){
        
        var my_dict = dict
        my_dict!["msg"] = nil
        my_dict!["driver_id"] = nil
        
        for (str,value) in my_dict!{
           
            let sub_stack2 = UIStackView()
            sub_stack2.axis  = .horizontal
            sub_stack2.distribution  = .fill
            sub_stack2.alignment = .fill
            sub_stack2.spacing   = 15.0
            
            let lbl_address = UILabel()
            lbl_address.numberOfLines = 0
            lbl_address.lineBreakMode = .byWordWrapping
            lbl_address.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            lbl_address.backgroundColor = UIColor.clear
            
            switch str{
            case "insurance_expiry":
                lbl_address.text = "Insurance Expiry"
            case "license_expiry":
                lbl_address.text = "License Expiry"
            case "s_badge_expiry":
                lbl_address.text = "School Badge Expiry"
            case "badge_expiry":
                lbl_address.text = "Council Badge Expiry"
            case "driver_pco_licence_expiry":
                lbl_address.text = "Driver PCO License Expiry"
            default:
                lbl_address.text = ""
            }
            
            let lbl = UILabel()
            lbl.numberOfLines = 0
            lbl.lineBreakMode = .byWordWrapping
            lbl.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            lbl.backgroundColor = UIColor.clear
            lbl.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            lbl.text  = value
           // lbl.heightAnchor.constraint(equalTo: lbl_address.heightAnchor, multiplier: 1).isActive = true
            
            sub_stack2.addArrangedSubview(lbl_address)
            sub_stack2.addArrangedSubview(lbl)
            
            stack_expiry.addArrangedSubview(sub_stack2)
        }
        
    }
    
    @IBAction func service_selected(sender:UIButton!){
        
        switch sender.tag {
        case 0:
            img_door.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 1:
            img_insurance.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 2:
            img_fire.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 3:
            img_wash.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 4:
            img_tyre.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 5:
            img_TabiPad.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 6:
            img_Water.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 7:
            img_Tissue.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")
        case 8:
            img_FitToDrive.image = sender.isSelected ? #imageLiteral(resourceName: "uncheck") : #imageLiteral(resourceName: "check")

        default:
            debugPrint("h")
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func confirm_press(){
        
        if self.img_door.image == UIImage(named: "uncheck") || self.img_insurance.image == UIImage(named: "uncheck") || self.img_fire.image == UIImage(named: "uncheck") || self.img_wash.image == UIImage(named: "uncheck") || self.img_tyre.image == UIImage(named: "uncheck") || self.img_TabiPad.image == UIImage(named: "uncheck") || self.img_Water.image == UIImage(named: "uncheck") || self.img_Tissue.image == UIImage(named: "uncheck") || self.img_FitToDrive.image == UIImage(named: "uncheck") {
            appDelegate.showerror(str: "All comply are mandatory to continue!")
        }else{
            // UserDefaults.standard.set("Yes", forKey: "isLoggedIn")
            UserDefaults.standard.set(true, forKey: "isDriverLoggedIn")
            appDelegate.changeRootViewController(with: "DashboardMenu")
        }
        
    }
    
    @IBAction func cancel_press(){
        self.dismiss(animated: true) {
            
        }
    }

}
