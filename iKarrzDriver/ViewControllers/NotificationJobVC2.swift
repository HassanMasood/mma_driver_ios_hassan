//
//  NotificationJobVC2.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 11/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import SafariServices
import MapKit
import MessageUI
import Messages

import PKHUD
import SCLAlertView
import Alamofire
import Reachability

class NotificationJobVC2: CommonVC ,SFSafariViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var lblNName : UILabel!
    @IBOutlet weak var lblNJobId : UILabel!
    @IBOutlet weak var lblNPickup : UILabel!
    @IBOutlet weak var lblNDropoff : UILabel!
    @IBOutlet weak var lblNPayType : UILabel!
    @IBOutlet weak var lblNVehicleType : UILabel!
    
    @IBOutlet weak var lbl_date : UILabel!
    @IBOutlet weak var lbl_time : UILabel!
    @IBOutlet weak var lbl_ref : UILabel!
    @IBOutlet weak var lbl_name : UILabel!
    @IBOutlet weak var lbl_bookedBy : UILabel!
    // @IBOutlet weak var lbl_caller : UILabel!
    @IBOutlet weak var view_caller: UIView!
    
    @IBOutlet weak var lbl_otherRef : UILabel!
    //@IBOutlet weak var lbl_flight : UILabel!
    @IBOutlet weak var btn_flight : UIButton!
    @IBOutlet weak var stack_flight : UIStackView!
    @IBOutlet weak var stack_othersRef : UIStackView!
    
    @IBOutlet weak var lbl_fare : UILabel!
    @IBOutlet weak var lbl_paymentType : UILabel!
    
    @IBOutlet weak var stack_sms: UIStackView!
//    @IBOutlet weak var cons_bottom_sms : NSLayoutConstraint!
//    @IBOutlet weak var const_ViewMore_Bottom : NSLayoutConstraint!
    
    @IBOutlet weak var stack_address: UIStackView!
    @IBOutlet weak var stack_others : UIStackView!
    
    @IBOutlet weak var lbl_cust_group_name : UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
//    @IBOutlet weak var const_MapView_Height: NSLayoutConstraint!
    @IBOutlet weak var sclViewInfo : UIScrollView!

    @IBOutlet weak var view_ETA : UIView!
    @IBOutlet weak var txt_ETA : UITextField!
    
    
    var jobDetails = [String:AnyObject]()
    var jobID = 0
    
    var isFromNotification = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //self.setupData()
        if isFromNotification{
            self.Refresh_JobDetailsAPI { (fiinish) in
            }
        }else{
            self.jobID = Int(self.jobDetails["job_id"] as! String)!
            print(self.jobID)
            self.setupData()
            self.setupNewData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:- Setup Data
    func setupData()  {
        
        lbl_date.text = jobDetails["job_date"] as? String
        lbl_time.text = jobDetails["job_time"] as? String
        lbl_otherRef.text = jobDetails["telephone"] as? String
        
        //lbl_fare.text = "£ " + String(describing: jobDetails["fare"]!)
        lbl_fare.text = jobDetails["fare"] as? String
        lbl_paymentType.text = (jobDetails["payment_type"] as? String)?.uppercased()
        
        //lbl_mobile.text = String(describing: jobDetails["mobile"]!)
        if let flight = jobDetails["flight_no"] as? String, !flight.isEmpty{
            let str_attr = NSAttributedString(string: flight, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor : UIColor.white])
            btn_flight.setAttributedTitle(str_attr, for: .normal)
        }else{
            stack_flight.isHidden = true
        }
        
        let name = jobDetails["name"] as? String ?? ""
        self.lbl_name.text = name
        
        if let caller = jobDetails["caller"] as? String, caller.isEmpty{
            view_caller.isHidden = true
        }
        
        if let flight = jobDetails["flight_no"] as? String, flight.isEmpty, let otherref = jobDetails["telephone"] as? String, otherref.isEmpty{
            stack_othersRef.isHidden = true
        }
        
        if let mobile = jobDetails["mobile"] as? String, mobile.isEmpty{
            stack_sms.heightAnchor.constraint(equalToConstant: 0).isActive = true
            //        cons_bottom_sms.constant = 0
        }
        
//        self.configAddressStack()
        self.configOthersStack()
        self.drawRouteBetweenLocations()
        
        lbl_cust_group_name.text = jobDetails["cust_group_name"] as? String
    }
    
    //----------------------------------------
    
    func setupNewData() {
        
        let jobId = String(describing: jobDetails["job_id"]!)
        self.lblNJobId.text = "JobId : \(jobId)"
        
        let passenger = jobDetails["caller"] as? String ?? ""
        self.lblNName.text = passenger
        
        let pickup = jobDetails["pickup"] as? String ?? ""
        self.lblNPickup.text = pickup
        
        let drop = jobDetails["destination"] as? String ?? ""
        self.lblNDropoff.text = drop
        
        let vehicleType = jobDetails["vehicle_type"] as? String ?? ""
        self.lblNVehicleType.text = vehicleType
        
        let pay = jobDetails["payment_type"] as? String ?? ""
        self.lblNPayType.text = "Payment Type : \(pay)"
    }
    
    //----------------------------------------
    
    func getPickupLocation()-> CLLocationCoordinate2D? {
        
        if let sourceLat = self.jobDetails["pickup_lat"] as? String,let sourceLong = self.jobDetails["pickup_long"] as? String {
            
            if let latDegree = CLLocationDegrees(sourceLat),let longDegree = CLLocationDegrees(sourceLong) {
                return CLLocationCoordinate2D(latitude: latDegree, longitude: longDegree)
            }
        }
        return nil
    }
    
    //----------------------------------------
    
    func getDropOffLocation()-> CLLocationCoordinate2D? {
        
        if let dropLat = self.jobDetails["dropoff_lat"] as? String,let dropLong = self.jobDetails["dropoff_long"] as? String {
            
            if let latDegree = CLLocationDegrees(dropLat),let longDegree = CLLocationDegrees(dropLong) {
                return CLLocationCoordinate2D(latitude: latDegree, longitude: longDegree)
            }
        }
        return nil
        
    }
    
    //----------------------------------------
    
    func drawRouteBetweenLocations() {
        
        if let sourceLocation = self.getPickupLocation(), let destinationLocation = self.getDropOffLocation() {
            
//            self.const_MapView_Height.constant = 150
            
            self.mapView.delegate = self
            
            let sourcePlacemark = MKPlacemark.init(coordinate: sourceLocation, addressDictionary: nil)
            
            let destPlacemark = MKPlacemark.init(coordinate: destinationLocation, addressDictionary: nil)
            
            let sourceAnotation = MKPointAnnotation()
            
            sourceAnotation.title = self.jobDetails["pickup"] as? String ?? ""
            
            if let source = sourcePlacemark.location {
                sourceAnotation.coordinate = source.coordinate
            }
            
            let destAnnotation = MKPointAnnotation()
            destAnnotation.title = self.jobDetails["destination"] as? String ?? ""
            
            if let dest = destPlacemark.location {
                destAnnotation.coordinate = dest.coordinate
            }
            
            self.mapView.showAnnotations([sourceAnotation, destAnnotation], animated: true)
            
            let sourceMapItem = MKMapItem.init(placemark: sourcePlacemark)
            
            let destMapItem = MKMapItem.init(placemark: destPlacemark)
            
            if appDelegate.reachability.connection == .unavailable{
                SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
                return
            }
            
            HUD.show(.progress)
            
            let directionRequest = MKDirections.Request()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destMapItem
            directionRequest.transportType = .automobile
            
            let directions = MKDirections.init(request: directionRequest)
            
            directions.calculate { (response, error) in
                
                HUD.hide()
                
                guard let response = response else {
                    if let error = error {
                        print(error)
                    }
                    return
                }
                
                let route = response.routes[0]
                self.mapView.addOverlay(route.polyline, level: MKOverlayLevel.aboveRoads)
                
                let rect = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegion.init(rect), animated: true)
            }
        }else{
//            self.const_MapView_Height.constant = 0
        }
    }
    
    func configAddressStack()  {
        
        stack_address.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let stackHeader = UILabel()
        stackHeader.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        stackHeader.backgroundColor = UIColor.darkGray
        stackHeader.text  = "Route Details"
        stackHeader.textAlignment = .center
        stackHeader.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        //stack_address.addArrangedSubview(stackHeader)
        
        let arr_address = ["pickup","via_address","stop1","stop2","stop3","destination","note"]
        let arr_addressTxt = ["Pickup","Via","Stop1","Stop2","Stop3","Dropoff","Notes"]
        let arr_address_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "road"),#imageLiteral(resourceName: "stop") ,#imageLiteral(resourceName: "stop"),#imageLiteral(resourceName: "stop"), #imageLiteral(resourceName: "destination"),#imageLiteral(resourceName: "note")]
        
        for (index,str) in arr_address.enumerated() {
            
            if let add = jobDetails[str] as?  String , !add.isEmptyAfterTrim{
                
                let sub_stack = UIStackView()
                sub_stack.axis  = .vertical
                sub_stack.distribution  = .fill
                sub_stack.alignment = .fill
                sub_stack.spacing   = 8.0
                
                
                let img_line = UIImageView()
                img_line.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                img_line.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
                sub_stack.addArrangedSubview(img_line)
                
                
                let sub_stack2 = UIStackView()
                sub_stack2.axis  = .horizontal
                sub_stack2.distribution  = .fill
                sub_stack2.alignment = .fill
                sub_stack2.spacing   = 8.0
                sub_stack2.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                sub_stack2.isLayoutMarginsRelativeArrangement = true
                
                let view = UIView()
                view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                
                
                let img_add = UIImageView()
                //img_add.backgroundColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
                img_add.contentMode = .scaleAspectFit
                img_add.image = arr_address_icon[index]
                view.addSubview(img_add)
                img_add.widthAnchor.constraint(equalToConstant: 30).isActive = true
                img_add.heightAnchor.constraint(equalToConstant: 30).isActive = true
                img_add.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
                img_add.translatesAutoresizingMaskIntoConstraints = false
                //sub_stack2.addArrangedSubview(img_add)
                
                
                let lbl_title = UILabel()
                lbl_title.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_title.backgroundColor = UIColor.clear
                lbl_title.text  = arr_addressTxt[index]
                lbl_title.textAlignment = .left
                lbl_title.widthAnchor.constraint(equalToConstant: 80).isActive = true
                lbl_title.setContentHuggingPriority(UILayoutPriority(rawValue: 750), for: .horizontal)
                //sub_stack2.addArrangedSubview(lbl_title)
                
                let lbl_add = UILabel()
                lbl_add.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                lbl_add.backgroundColor = UIColor.clear
                lbl_add.text  = add
                lbl_add.numberOfLines = 0
                lbl_add.textAlignment = .left
                view.addSubview(lbl_add)
                lbl_add.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
                lbl_add.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
                lbl_add.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
                lbl_add.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
                lbl_add.leadingAnchor.constraint(equalTo: img_add.trailingAnchor, constant: 8).isActive = true
                lbl_add.translatesAutoresizingMaskIntoConstraints = false
                //sub_stack2.addArrangedSubview(lbl_add)
                
                
                img_add.centerYAnchor.constraint(equalTo: lbl_add.centerYAnchor).isActive = true
                
                
                let btn_waze = CustomButton(type: .custom)
                btn_waze.imageView!.contentMode = .scaleAspectFit
                //btn_waze.setImage(#imageLiteral(resourceName: "waze"), for: .normal)
                btn_waze.setImage(#imageLiteral(resourceName: "map_loc"), for: .normal)
                btn_waze.widthAnchor.constraint(equalToConstant: 30).isActive = true
                btn_waze.heightAnchor.constraint(equalToConstant: 30).isActive = true
                btn_waze.valueText = add
                //btn_waze.addTarget(self, action: #selector(navtoWaze(sender:)), for: .touchUpInside)
                btn_waze.addTarget(self, action: #selector(locate_map(sender:)), for: .touchUpInside)
                view.addSubview(btn_waze)
                btn_waze.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
                btn_waze.centerYAnchor.constraint(equalTo: lbl_add.centerYAnchor).isActive = true
                btn_waze.leadingAnchor.constraint(equalTo: lbl_add.trailingAnchor, constant: 8).isActive = true
                btn_waze.translatesAutoresizingMaskIntoConstraints = false
                if lbl_title.text == "Notes"{
                    //btn_waze.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
                    //sub_stack2.addArrangedSubview(btn_waze)
                    btn_waze.widthAnchor.constraint(equalToConstant: 0).isActive = true
                    btn_waze.heightAnchor.constraint(equalToConstant: 0).isActive = true
                }
                
                //sub_stack.addArrangedSubview(sub_stack2)
                sub_stack.addArrangedSubview(view)
                
                stack_address.addArrangedSubview(sub_stack)
                
            }
        }
    }
    
    
    func configOthersStack()  {
        
        stack_others.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let stackHeader = UILabel()
        stackHeader.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        stackHeader.backgroundColor = UIColor.darkGray
        stackHeader.text  = "OTHER DETAILS"
        stackHeader.textAlignment = .center
        stackHeader.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        //stack_others.addArrangedSubview(stackHeader)
        
        let img_line = UIImageView()
        img_line.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        img_line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //stack_others.addArrangedSubview(img_line)
        
        let arr_others = ["vehicle_type","luggage","hand_luggage","car_park","wttime","wttime_val","extras","gratuity","vehicle_chair","child_seat","bookingfees","num_of_people","payment_r","no_of_cars","calc_value","incentive_val","toll_charge", "no_of_hours"]
        
        let arr_others_icon = [#imageLiteral(resourceName: "car1") ,#imageLiteral(resourceName: "luggage") , #imageLiteral(resourceName: "hand_lugagge")  ,#imageLiteral(resourceName: "car_parking") ,#imageLiteral(resourceName: "sand_clock") ,#imageLiteral(resourceName: "waiting_time") , #imageLiteral(resourceName: "plus") ,#imageLiteral(resourceName: "medal") , #imageLiteral(resourceName: "wheelChair") , #imageLiteral(resourceName: "child_seat") ,#imageLiteral(resourceName: "booking_fees") ,#imageLiteral(resourceName: "people") , #imageLiteral(resourceName: "payment_received") , #imageLiteral(resourceName: "no_of_cars") , #imageLiteral(resourceName: "handshake"), #imageLiteral(resourceName: "gift"), #imageLiteral(resourceName: "tallcharge"), #imageLiteral(resourceName: "clock")]
        
        let arrOthersName = ["Vehicle type", "Hand bag", "Luggage", "Parking", "Waiting Charge","Waiting Time", "Extras", "Gratuity", "Wheel Chair", "Child seat", "Book Fees", "No. of people", "Payment", "No. of Cars", "Driver Percentage", "Driver incentive", "Toll charge", "No. of hours"]
        
        for i in stride(from: 0, to: arr_others.count, by: 1) {
            
            let str = arr_others[i]
            let icon = arr_others_icon[i]
            
            if let strValue = self.jobDetails[str] as? NSNull {
                continue
            }
            
            if let strValue = self.jobDetails[str] as? String, (strValue.isEmptyAfterTrim || strValue == "0" || strValue == "0.00") {
                continue
            }
            
            if let intValue = self.jobDetails[str] as? Int, intValue == 0 {
                continue
            }
            
            
            let sub_stack = UIStackView()
            sub_stack.axis  = .horizontal
            sub_stack.distribution  = .fillEqually
            sub_stack.alignment = .fill
            sub_stack.spacing   = 15.0   //25.0
            
            //            for j in 0...1{
            
            //                let str  =    (j == 0) ?  arr_others[i] : arr_others[i+1]
            //                let icon =    (j == 0) ?  arr_others_icon[i] : arr_others_icon[i+1]
            
            //                if (i+j) > arr_others.count - 1{
            //                    break
            //                }
            
            //if let other = jobDetails[str] as?  String , !other.isEmptyAfterTrim{
            //if let other = jobDetails[str] as?  String {
            
            let sub_stack2 = UIStackView()
            sub_stack2.axis  = .horizontal
            sub_stack2.distribution  = .fill
            sub_stack2.alignment = .fill
            sub_stack2.spacing   = 15.0
            
            let img_addressIcon = UIImageView()
            img_addressIcon.contentMode = .scaleAspectFit
            img_addressIcon.image = icon.withRenderingMode(.alwaysTemplate)
            img_addressIcon.tintColor = .white

            img_addressIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
            img_addressIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
            sub_stack2.addArrangedSubview(img_addressIcon)
            
            
            let lbl_address = UILabel()
            lbl_address.numberOfLines = 0
            lbl_address.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            lbl_address.backgroundColor = UIColor.clear
            lbl_address.text  = arrOthersName[i] + " : " + (jobDetails[str] as?  String ?? "")
            
            if str == "payment_r" || str == "vehicle_chair" || str == "child_seat"{
                if lbl_address.text == arrOthersName[i] + " : " + "yes" {
                    lbl_address.text = "\(arrOthersName[i]) : ✅"
                }else{
                    lbl_address.text = "\(arrOthersName[i]) : ❌"
                }
            } else if str == "calc_value"{
                lbl_address.text = arrOthersName[i] + " : " + (jobDetails[str] as!  String) + " %"
            }
            lbl_address.adjustsFontSizeToFitWidth = true
            lbl_address.minimumScaleFactor = 0.5
            lbl_address.textAlignment = .left
            sub_stack2.addArrangedSubview(lbl_address)
            
            img_addressIcon.centerYAnchor.constraint(equalTo: lbl_address.centerYAnchor).isActive = true
            
            sub_stack.addArrangedSubview(sub_stack2)
            //}
            //            }
            
            if sub_stack.arrangedSubviews.count != 0 {
                stack_others.addArrangedSubview(sub_stack)
            }
        }
        
    }
    
    //MARK: - Flight
    @IBAction func flight_no(btn:UIButton!){
        //print("flight is \(String(describing: btn.titleLabel!.text))")
        
        if let str = btn.titleLabel?.text{
            
            let actionsheet = UIAlertController(title: AppName, message: "check your flight info", preferredStyle: .actionSheet)
            
            let google = UIAlertAction(title: "Google", style: .default, handler: { (action) in
                //let url = URL(string: "https://www.google.co.uk/search?q=\(str)")
                let url = URL(string: "https://www.google.co.uk/search?q=\(str)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
                let sf = SFSafariViewController(url: url!)
                sf.delegate = self
                self.present(sf, animated: true, completion: {
                    
                })
            })
            
            let flightAware = UIAlertAction(title: "FlightAware", style: .default, handler: { (action) in
                //let url = URL(string: "http://uk.flightaware.com/live/flight/\(str)")
                let url = URL(string: "http://uk.flightaware.com/live/flight/\(str)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
                let sf = SFSafariViewController(url: url!)
                sf.delegate = self
                self.present(sf, animated: true, completion: {
                })
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
                
            })
            
            actionsheet.addAction(google)
            actionsheet.addAction(flightAware)
            actionsheet.addAction(cancel)
            
            self.present(actionsheet, animated: true, completion: {
                
            })
        }
    }
    
    @IBAction func locate_map(sender: CustomButton!){
        
        let alert = UIAlertController(title: AppName, message: "choose map source", preferredStyle: .actionSheet)
        
        let waze_map = UIAlertAction(title: "via Waze", style: .default) { (action) in
            self.navtoWaze(location: sender.valueText)
        }
        
        let google_map = UIAlertAction(title: "via Google maps", style: .default) { (action) in
            self.navtoGoogleMaps(location: sender.valueText)
        }
        
        let apple_map = UIAlertAction(title: "via Apple maps", style: .default) { (action) in
            
            let url = URL(string: "http://maps.apple.com//?q=\(sender.valueText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            if let wurl = url , UIApplication.shared.canOpenURL(wurl){
                UIApplication.shared.open(wurl, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
                })
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(waze_map)
        alert.addAction(google_map)
        alert.addAction(apple_map)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    
    //MARK: - Back Press
    @IBAction func backpress(sender:UIButton!){
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnNavTapped(_ sender: UIButton) {
        
        print("Nav button tapped...")
        let pickup = jobDetails["pickup"] as? String ?? ""
        
        self.shareLocation(location: pickup)
    }
    
    //----------------------------------------
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
//        self.const_ViewMore_Bottom.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnMoreCloseTapped(_ sender: UIButton) {
//        self.const_ViewMore_Bottom.constant = -1500
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
        self.sclViewInfo.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    @IBAction func btnCloseInfoTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.sclViewInfo.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    //----------------------------------------
    
    func shareLocation(location: String) {
        
        let alert = UIAlertController(title: AppName, message: "choose map source", preferredStyle: .actionSheet)
        
        let waze_map = UIAlertAction(title: "via Waze", style: .default) { (action) in
            self.navtoWaze(location: location)
        }
        
        let google_map = UIAlertAction(title: "via Google maps", style: .default) { (action) in
            self.navtoGoogleMaps(location: location)
        }
        
        let apple_map = UIAlertAction(title: "via Apple maps", style: .default) { (action) in
            
            let url = URL(string: "http://maps.apple.com//?q=\(location)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            if let wurl = url , UIApplication.shared.canOpenURL(wurl){
                UIApplication.shared.open(wurl, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (finish) in
                })
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(waze_map)
        alert.addAction(google_map)
        alert.addAction(apple_map)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    //----------------------------------------
    
    //MARK:- Phone Call
    @IBAction func makePhonecall(){
        
        let mobile = jobDetails["mobile"] as! String
        
        guard let number = URL(string: "telprompt://" + mobile) else { return }
        
        if UIApplication.shared.canOpenURL(number){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(number)
            }
        }
        else{
            SCLAlertView().showError(AppName, subTitle: "Phone call is not available on this device.")
        }
    }
    
    @IBAction func makeSMS(sender:UIButton!){
        
        var type = "thank_sms"
        switch sender.tag {
        case 1:
            type = "inform_sms"
        case 2:
            type = "thank_sms"
        case 3:
            type = "enquire_sms"
        default:
            type = "inform_sms"
        }
        
        var bodyStr = ""
        
        if appDelegate.reachability.connection != .unavailable {
            
            let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
            
            let parameter  = ["type":"default_custom_sms", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"job_id":jobDetails["job_id"]!,"id":type] as [String : Any]
            // print(parameter)
            HUD.show(.progress)
            
            APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
                
                HUD.hide()
                print(response)
                switch response.result {
                case .success:
                    // debugPrint(response.result.value!)
                    let dict = response.result.value as! [String:AnyObject]
                    let arr  = dict["DATA"] as! [[String:AnyObject]]
                    if let msg = arr[0]["msg"] as? String{
                        bodyStr = msg
                        
                        DispatchQueue.main.async(execute: {
                            self.sendMessage(bodyStr: bodyStr)
                        })
                        
                    }
                    
                case .failure(let error):
                    print(error)
                    //                    DispatchQueue.main.async(execute: {
                    //                        //self.tbl.isHidden = true
                    //                        appDelegate.showerror(str: error.localizedDescription)
                    //                    })
                }
            })
            
        }
        else{
            self.sendMessage(bodyStr: bodyStr)
        }
        
    }
    
    func sendMessage(bodyStr : String!){
        
        if MFMessageComposeViewController.canSendText(){
            
            let message = MFMessageComposeViewController()
            message.messageComposeDelegate = self
            //message.recipients = [sender.titleLabel!.text!]
            message.recipients =  [jobDetails["mobile"] as! String]
            message.subject = AppName
            message.body = bodyStr
            self.present(message, animated: true, completion: {
                
            })
        }
        else{
            appDelegate.showerror(str: "Can not able to send message.")
        }
    }
    
    //MARK:- Message Delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result {
        case .sent:
            SCLAlertView().showSuccess(AppName, subTitle: "Message Sent successfully.")
        case .cancelled:
            appDelegate.showerror(str: "Message Cancelled.")
            
        case .failed:
            appDelegate.showerror(str: "Message failed.")
        }
        controller.dismiss(animated: true) {
            
        }
    }
    
    
    //MARK:-  DB Operations
    
    func deleteJobFromDb(){
        
        let predicate = NSPredicate(format: "job_id == %@", jobDetails["job_id"] as! String)
        let jobs = DBHelper.sharedInstance.featchFromEntityWithPredicate(Entity: "Jobs", predicate: predicate)
        
        if let arr = jobs.arr as? [Jobs] , !arr.isEmpty{
            let job = arr.last
            appDelegate.persistentContainer.viewContext.delete(job!)
            appDelegate.saveContext()
        }
    }
    
    func UpdateJobDb(){
        
        let job = DBHelper.sharedInstance.checkDuplication(Entity: "Jobs", predicate_str: "job_id", ID: jobDetails["job_id"] as! String)
        
        if job.Error == nil{
            if job.isExists{
                let latestJob = job.arr?.last as! Jobs
                let dispatch_Str = String(describing: jobDetails["dispatch"])
                latestJob.dispatch = Int64(dispatch_Str) ?? 0
                appDelegate.saveContext()
            }
            else{
                DBHelper.sharedInstance.saveAllJobs(arr_jobs: [jobDetails], completion: { (finish) in
                    
                })
            }
            
        }
        
    }
    
    
    //MARK:- ETA
    
    @IBAction func acceptWithETA (sender:UIButton) {
        
        self.view.endEditing(true)
        view_ETA.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        view_ETA.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_ETA.alpha = 1.0
            self.view_ETA.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
    }
    
    @IBAction func ok_ETA(){
        
        if (txt_ETA.text?.isEmpty)!{
            appDelegate.showerror(str: "ETA should not be empty!")
            return
        }
        self.Accept_Reject_JobAPI(jobType: "Accept_ETA")
    }
    
    @IBAction func cancel_eta(){
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_ETA.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_ETA.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    
    
    //MARK:- Refresh Job Details
    
    func Refresh_JobDetailsAPI(isJobAccepted:Bool = false,completion:@escaping (_ isFinish:Bool) -> Void){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_loaded","job_id":jobID, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"device_type":"ios"] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            
            switch response.result {
            case .success:
                // debugPrint(response.request?.url ?? "")
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                let arr = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    
                    DispatchQueue.main.async(execute: {
                        
                        if !arr.isEmpty{
                            self.jobDetails = arr[0]
                        }
                        self.setupData()
                        self.setupNewData()
                        if isJobAccepted {
                            DBHelper.sharedInstance.saveAllJobs(arr_jobs: [self.jobDetails], completion: { (finish) in
                            })
                        }
                        completion(true)
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        //SCLAlertView().showError(AppName, subTitle: arr[0]["msg"] as! String, duration: 4)
                        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                            // action here
                        }
                        SCLAlertView().showError(AppName, subTitle:arr[0]["msg"] as! String,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                        completion(true)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //SCLAlertView().showError(AppName, subTitle: error.localizedDescription, duration: 4)
                    let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                        // action here
                    }
                    SCLAlertView().showError(AppName, subTitle:error.localizedDescription,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
                    completion(true)
                })
            }
        })
    }
    
    //MARK:- Action for Accept & Reject Job
    
    @IBAction func jobaction(sender:UIButton!){
        
        let jobtype =  sender.tag == 1 ? "driver_job_accept" : "driver_job_reject"
        self.Accept_Reject_JobAPI(jobType: jobtype)
    }
    
    
    func Accept_Reject_JobAPI(jobType:String){
        
        //HUD.show(.progress)
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        var job_type = jobType
        if jobType == "Accept_ETA"{
            job_type = "driver_job_accept"
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        var parameter  = ["type":job_type ,"job_id":jobID, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        if jobType == "Accept_ETA"{
            parameter["eta"] = txt_ETA.text! + " M"
        }
        
        if let loc = appDelegate.latest_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            //HUD.hide()
            //print(response.request?.url ?? "")
            //print(response.result.value ?? "")
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                guard let dict = response.result.value as? [String:AnyObject] else { return }
                
                let msg = dict["DATA"]!["msg"] as? String ?? ""
                
                if msg ==  "Job Accepted"{
                    
                    if jobType == "driver_job_accept"{
                        
                        DispatchQueue.main.async {
                            self.Refresh_JobDetailsAPI(isJobAccepted: true, completion: { (finish) in
                                self.dismiss(animated: false, completion: {
                                    let jobProcessVC = JobProcessVC.viewController()
                                    jobProcessVC.jobDetails = self.jobDetails
                                    
                                    let nav  = appDelegate.window?.rootViewController as! UINavigationController
                                    
                                    nav.pushViewController(jobProcessVC, animated: false)
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                                })
                            })
                            
                        }
                    }
                    else{
                        
                        self.deleteJobFromDb()
                        self.performSegue(withIdentifier: "GoToDashBoard2", sender: self)
                        //self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                        self.performSegue(withIdentifier: "GoToDashBoard2", sender: self)
                        //self.navigationController?.popViewController(animated: true)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NotificationJobVC2 : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .red
        renderer.lineWidth = 5.0
        return renderer
    }
    
    //----------------------------------------
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}
