//
//  ExpenseListVC.swift
//  TBMSDriver
//
//  Created by Piyush Agrawal on 12/05/18.
//  Copyright © 2018 Piyush Agrawal. All rights reserved.
//

import UIKit

import Alamofire
import PKHUD
import SCLAlertView

class ExpenseListVC: CommonVC, UITextFieldDelegate, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var txt_from : CustomTextField!
    @IBOutlet weak var txt_to : CustomTextField!
    
    var selectedTextField : UITextField?
    
    @IBOutlet weak var view_date:UIView!
    @IBOutlet weak var pic_date:UIDatePicker!
    
    @IBOutlet weak var tbl : UITableView!
    @IBOutlet weak var lbl_expenseListCount : UILabel!
    @IBOutlet weak var lbl_TotalEarnings : UILabel!
    
    var arr_expenses = [[String:AnyObject]]()
    let refreshControl = UIRefreshControl()
    
    var documentController : UIDocumentInteractionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.SetupData()
        
        self.getJobs()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getJobs(isRefresh:)), name: NSNotification.Name(rawValue: "updateData"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SetupData
    
    func  SetupData() {
        
        tbl.estimatedRowHeight = 44.0
        tbl.rowHeight = UITableView.automaticDimension
        tbl.tableFooterView = UIView(frame: CGRect.zero)
        
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tbl.refreshControl = refreshControl
        
        var dayComp = DateComponents()
        dayComp.year = -1
        let date = Calendar.current.date(byAdding: dayComp, to: Date())
        pic_date.minimumDate = date
        pic_date.maximumDate = Date()
        
        txt_from.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        txt_to.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
    }
    
    
    //MARK: Today Reset Date
    @IBAction func todayResetDates(){
        
        txt_from.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        txt_to.text = Date().stringWithFormatter(dateFormat: "dd MMM yyyy")
        
        self.getJobs()
    }
    
    //MARK:- TextField Delegate
    
    func  textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTextField = textField
        
        self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_date.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_date.alpha = 1.0
            self.view_date.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        return false
    }
    
    //MARK:- Show Date Picker
    @IBAction func showDatePicker(sender:UIButton!){
        
        selectedTextField = (sender.tag == 0) ? txt_from : txt_to
        
        self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_date.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_date.alpha = 1.0
            self.view_date.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    //MARk:- Toolbar action
    
    @IBAction func cancel(sender:UIBarButtonItem!){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_date.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
    }
    
    @IBAction func Selected_datePicker(sender:UIBarButtonItem!){
        
        if selectedTextField == txt_from{
            let date = pic_date.date
            // txt_from.text = NSDate.toString(date: date as NSDate, format: "dd MMM yyyy")
            txt_from.text = date.stringWithFormatter(dateFormat: "dd MMM yyyy")
        }
        else if selectedTextField == txt_to{
            let date = pic_date.date
            txt_to.text = date.stringWithFormatter(dateFormat: "dd MMM yyyy")
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_date.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_date.alpha = 0.0
        }, completion:{(finished : Bool)  in
            if (finished)
            {
            }
        })
        
        self.getJobs()
    }
    
    //MARK:-  Get Data
    
    @objc func getJobs(isRefresh : Bool = false){
        
        if appDelegate.reachability.connection == .unavailable{
            SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            if isRefresh{
                self.refreshControl.endRefreshing()
            }
            return
        }
        
        if !isRefresh{
            HUD.show(.progress)
        }
        
        //let fromdate = NSDate.parse(dateString: txt_from.text!, format: "dd MMM yyyy")
        let fromdate = Date().datefromString(string: txt_from.text!, dateFormat: "dd MMM yyyy")
        let str_fromDate = fromdate.stringWithFormatter(dateFormat: "dd-MMM-yyyy")
        //let str_fromDate = "1-MAY-2018"
        
        //let todate = NSDate.parse(dateString: txt_to.text!, format: "dd MMM yyyy")
        let todate = Date().datefromString(string: txt_to.text!, dateFormat: "dd MMM yyyy")
        let str_toDate = todate.stringWithFormatter(dateFormat: "dd-MMM-yyyy")
        //let str_toDate = "15-May-2018"
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_expense_list", "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"from_date":str_fromDate,"to_date":str_toDate] as [String : Any]
        // print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            HUD.hide()
            self.refreshControl.endRefreshing()
            print(response.request?.url ?? "")
            switch response.result {
            case .success:
                // debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                //self.arr_expenses  = dict["DATA"] as! [[String:AnyObject]]
                if let arr = dict["DATA"] as? [[String:AnyObject]]{
                    self.arr_expenses = arr
                    DispatchQueue.main.async(execute: {
                        //self.tbl.isHidden = false
                        self.tbl.reloadData()
                        self.lbl_expenseListCount.text = String(self.arr_expenses.count) + " " + "Expenses"
                        //self.lbl_TotalEarnings.text = "£ " + String(describing: dict["totalearning"]!)
                        //self.lbl_TotalEarnings.text = dict["totalearning"] as? String
                        let sum = self.arr_expenses.map({Double($0["expense_amount"] as? String ?? "0") ?? 0 }).reduce(0, +)
                        //debugPrint(sum)
                        self.lbl_TotalEarnings.text = String(format:"%.2f",sum)

                    })
                    
                }
                else{
                    
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: "Server Error")
                        self.lbl_expenseListCount.text =  "0 " + "Expenses"
                        self.lbl_TotalEarnings.text = "0"
                        //self.tbl.isHidden = true
                        self.arr_expenses.removeAll()
                        self.tbl.reloadData()
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    //self.tbl.isHidden = true
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    //MARK:- Pull To Refresh
    
    @objc func refreshData(){
        self.getJobs(isRefresh: true)
    }
    
    
    //MARK:- TableView Delegate & Datasource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_expenses.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl.dequeueReusableCell(withIdentifier: "Cell") as! ExpenseCell
        
        let dict = arr_expenses[indexPath.row]
        
        var str_date_time = dict["expense_date"] as? String ?? ""
        if !str_date_time.isEmpty{
             str_date_time += "  "
        }
        str_date_time += dict["expense_time"] as? String ?? ""
        cell.lbl_date_time.text = str_date_time
        
        cell.lbl_expense_name.text = dict["expense_name"] as? String ?? ""
        
        cell.lbl_cost.text = dict["expense_amount"] as? String ?? ""
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // self.performSegue(withIdentifier: "GotoDetails", sender: indexPath)
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
