//
//  AppDelegate.swift
//  Ecabbi
//
//  Created by Piyush Agrawal on 05/05/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import IQKeyboardManagerSwift
import SCLAlertView
import Reachability
import Fabric
import Crashlytics
import ChameleonFramework
import GooglePlaces
import GoogleMaps
import CoreLocation
import Braintree
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate,CLLocationManagerDelegate {
    
    var window: UIWindow?
    
    static let sharedInstance = AppDelegate()
    
    var reachability : Reachability! = nil
    
    var deviceToken = ""

    let locationManager = CLLocationManager()
    var latest_location : CLLocation?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        application.applicationIconBadgeNumber = 0
        
        application.isIdleTimerDisabled = true //prevent phone off while app is running
        
        IQKeyboardManager.shared.enable = true
        
        Fabric.with([Crashlytics.self,STPAPIClient.self])
                
        if let googleKey = UserDefaults.standard.value(forKey: "Google_key") as? String , !googleKey.isEmptyAfterTrim {
            kGoogleAPIKey = googleKey
            
            GMSPlacesClient.provideAPIKey(kGoogleAPIKey)
            GMSServices.provideAPIKey(kGoogleAPIKey)
        }
        
            
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
        do{
            reachability = try Reachability()
            try reachability.startNotifier()
        }catch{
            //print("could not start reachability notifier")
        }
        
        
        //        if let str = UserDefaults.standard.value(forKey: "isLoggedIn") as? String,  str == "Yes"{
        //            appDelegate.changeRootViewController(with: "DashboardMenu")
        //        }
        let loggedIn = UserDefaults.standard.bool(forKey: "isDriverLoggedIn")
        if loggedIn{
            appDelegate.changeRootViewController(with: "DashboardMenu")
        }
        
        //Remote Notification
        self.configureUserNotifications()
        //self.registerForRemoteNotification()
        
        let docsurl = try! FileManager.default.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        print("File is: \(docsurl)")
    BTAppSwitch.setReturnURLScheme("com.taxibasesoftware.tbmsdriver.payments")
        
        // Stripe theme configuration
//        STPTheme.default().primaryBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        STPTheme.default().primaryForegroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        STPTheme.default().secondaryForegroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        STPTheme.default().accentColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
     //   let bbApearance = UIBarButtonItem.appearance()
   // bbApearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)

//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.backgroundColor : UIColor.black]
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.backgroundColor : UIColor.clear]
        
        self.setupLocation()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.taxibasesoftware.tbmsdriver.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return false
    }
    
    //MARK: User Notifications
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func configureUserNotifications() {
        
        let center  = UNUserNotificationCenter.current()
        
        let acceptAction = UNNotificationAction(identifier:
            "accept", title: "✅ Accept Job", options: [])
        let rejectAction = UNNotificationAction(identifier:
            "reject", title: "❌ Reject Job", options: [])
        let category =
            UNNotificationCategory(identifier: "newDriverJob",
                                   actions: [acceptAction,rejectAction],
                                   intentIdentifiers: [],
                                   options: [])
        
        center.setNotificationCategories([category])
        center.delegate = self
        center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
            if error == nil{
                DispatchQueue.main.async { 
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
    }
    
    func callWSToGetGoogleKey() {
        
        let parameter  = ["office_name":OfficeName,"type":"get_google_key"]
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success:
                
                debugPrint(response.result.value ?? "")
                
                guard let data = response.result.value as? [String:Any] else {
                    return
                }
                
                guard let dict = data["DATA"] as? [String:Any] else {
                    return
                }
                
                if let key = dict["link"] as? String, !key.isEmpty{
                    UserDefaults.standard.set(key, forKey: "Google_key")
                    kGoogleAPIKey = key
                } else if let key2 = UserDefaults.standard.value(forKey: "Google_key") as? String , !key2.isEmpty{
                    kGoogleAPIKey = key2
                }
                
                if kGoogleAPIKey.isEmptyAfterTrim == false {
                    GMSPlacesClient.provideAPIKey(kGoogleAPIKey)
                    GMSServices.provideAPIKey(kGoogleAPIKey)
                }
                
            case .failure(let error):
                print("Error signup: \(error)")
                DispatchQueue.main.async(execute: {
                    //appDelegate.showerror(str: error.localizedDescription)
                })
                if let key2 = UserDefaults.standard.value(forKey: "Google_key") as? String , !key2.isEmpty{
                    kGoogleAPIKey = key2
                }
                GMSPlacesClient.provideAPIKey(kGoogleAPIKey)
                GMSServices.provideAPIKey(kGoogleAPIKey)
            }
        })
        
    }
    
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration for remote notifications failed")
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("Registered with device token: \(deviceToken.hexString)")
        self.deviceToken = deviceToken.hexString

    }
    
    
    //Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        
        let dict = notification.request.content.userInfo["aps"] as! [String:AnyObject]
        print(dict)
        
        let category = dict["category"] as! String
        
        let nav = self.window?.rootViewController as! UINavigationController
        _ = nav.topViewController!
        _ = self.window?.rootViewController?.storyboard
        
        
        if category == "newDriverJob"{
            //        let JobID = dict["job_ID"] as! NSInteger
            //
            //        let jobOffervc = storyboard?.instantiateViewController(withIdentifier: "JobOfferVC") as! NotificationJobVC
            //        jobOffervc.jobID = JobID
            //nav.pushViewController(jobOffervc, animated: true)
            //completionHandler([.alert, .badge, .sound])
            completionHandler([.alert,.sound])
        }
        else if category == "getDriverMsg"{
            if let topVC = nav.topViewController as? MessagesVC{
                topVC.getAllMessages()
            }else{
                completionHandler([.alert,.sound])
            }
        }
        else if category == "updateDriverJob"{
            completionHandler([.alert,.sound])
        }
        else if category == "newTimeoutJob"{
            completionHandler([.alert,.sound])
        }else if category == "newBiddingJob"{
            completionHandler([.alert,.sound])
        }
        
    }
    
    //Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.setupLocation()
        print("User Info = ",response.notification.request.content.userInfo)
        print("Response received for \(response.actionIdentifier)")
        let dict = response.notification.request.content.userInfo["aps"] as! [String:AnyObject]
        //print(dict)
        let category = dict["category"] as! String
        
        if category == "newDriverJob"{
            
            let JobID = dict["job_ID"] as! NSInteger
            if response.actionIdentifier == "accept" {
                
                self.Accept_Reject_JobAPI(jobType: "driver_job_accept", jobID: JobID)
            }
            else if response.actionIdentifier == "reject" {
                
                self.Accept_Reject_JobAPI(jobType: "driver_job_reject", jobID: JobID)
            }
            else{
                
                let nav = self.window?.rootViewController as! UINavigationController
                //let topvc = nav.topViewController!
                let storyboard = self.window?.rootViewController?.storyboard
                let NavjobOffervc = storyboard?.instantiateViewController(withIdentifier: "NavJobOfferVC") as! UINavigationController
                //let jobOffervc = storyboard?.instantiateViewController(withIdentifier: "JobOfferVC") as! NotificationJobVC
                let jobOffervc = NavjobOffervc.topViewController as! NotificationJobVC2
                jobOffervc.jobID = JobID
                //nav.pushViewController(jobOffervc, animated: true)
                nav.present(NavjobOffervc, animated: true, completion: {
                    
                })
            }
        } else if category == "newTimeoutJob"{
            let JobID = dict["job_ID"] as! NSInteger
            let nav = self.window?.rootViewController as! UINavigationController
            //let topvc = nav.topViewController!
            let storyboard = self.window?.rootViewController?.storyboard
            let NavjobOffervc = storyboard?.instantiateViewController(withIdentifier: "TimerNavJobOfferVC") as! UINavigationController
            //let jobOffervc = storyboard?.instantiateViewController(withIdentifier: "JobOfferVC") as! NotificationJobVC
            let jobOffervc = NavjobOffervc.topViewController as! TimerNotificationJobVC2
            jobOffervc.jobID = JobID
            NavjobOffervc.modalPresentationStyle = .fullScreen
            //nav.pushViewController(jobOffervc, animated: true)
            nav.present(NavjobOffervc, animated: true, completion: {
                
            })
        } else if category == "updateDriverJob"{
            
            let JobID = dict["job_ID"] as! NSInteger
            
            let nav = self.window?.rootViewController as! UINavigationController
            
            self.Refresh_JobDetailsAPI(jobID: JobID, completion: { (finish, details) in
                if details != nil{
                    let jobProcessvc = JobProcessVC.viewController()
                    jobProcessvc.jobDetails = details
                    nav.pushViewController(jobProcessvc, animated: true)
                }
            })
            
            
        } else if category == "getDriverMsg"{
            
            let nav = self.window?.rootViewController as! UINavigationController
            
            let Msgvc = MessagesVC.viewController()
            
            let state = UIApplication.shared.applicationState
            
            if state == .background || state == .active {
                
                if let topVC = nav.topViewController as? MessagesVC {
                    topVC.getAllMessages()
                }
                else{
                    nav.pushViewController(Msgvc, animated: true)
                }
            }
            else if state == .inactive {
                nav.pushViewController(Msgvc, animated: true)
            }
            
        }else if category == "newBiddingJob" {
            
            let nav = self.window?.rootViewController as! UINavigationController
            
            let biddingListVC = BiddingListVC.viewController()
            
            biddingListVC.isFromNotification = true
            let state = UIApplication.shared.applicationState
            
            if state == .background || state == .active {
                
                if let topVC = nav.topViewController as? BiddingListVC {
                    topVC.get_biddingList()
                }
                else{
                    nav.pushViewController(biddingListVC, animated: true)
                }
            }
            else if state == .inactive {
                nav.pushViewController(biddingListVC, animated: true)
            }
        }
        
        //completionHandler()
    }
    
    func Accept_Reject_JobAPI(jobType:String,jobID:NSInteger){
        self.setupLocation()
        //HUD.show(.progress)
        if appDelegate.reachability.connection == .unavailable{
            //SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        var parameter  = ["type":jobType,"job_id":jobID, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!] as [String : Any]
        //print(parameter)
        
        if let loc = latest_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            //HUD.hide()
            //print(response.result.value!)
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                if let dict = response.result.value as? [String:AnyObject] {
                    
                    if let msg = dict["DATA"]!["msg"] as? String, msg ==  "Job Accepted"{
                        self.ChangeJob_statusAPI(jobID: jobID)
                        
                    }
                    else{
                        DispatchQueue.main.async(execute: {
                            if let msg = dict["DATA"]?["msg"] as? String {
                                appDelegate.showerror(str: msg)
                            }
                        })
                    }
                }
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    func ChangeJob_statusAPI(jobID:NSInteger){
        
        //HUD.show(.progress)
        if appDelegate.reachability.connection == .unavailable{
            //SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        //let parameter  = ["type":"driver_job_complete","job_id":jobID, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":4] as [String : Any]
        var parameter  = ["type":"driver_job_complete","job_id":jobID, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"dispatch":4] as [String : Any]
        // print(parameter)
        
        if let loc = latest_location{
            parameter["latitude"] = String(Double(loc.coordinate.latitude))
            parameter["longitude"] = String(Double(loc.coordinate.longitude))
        }
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            // HUD.hide()
            //print(response.result.value!)
            
            switch response.result {
            case .success:
                // debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                
                if dict["DATA"]!["msg"] as! String ==  "Job Completed"{
                    
                    /*
                     DispatchQueue.main.async(execute: {
                     if (self.navigationController?.viewControllers[1] as? JobsInProgressVC) != nil {
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_List_Jobs"), object: nil)
                     }else{
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_NewList_Jobs"), object: nil)
                     }
                     
                     self.Refresh_JobDetailsAPI()
                     
                     })
                     */
                    
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: dict["DATA"]!["msg"] as! String)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                })
            }
        })
        
    }
    
    func Refresh_JobDetailsAPI(jobID:NSInteger!,completion:@escaping (_ isFinish:Bool,_ details:[String:AnyObject]?) -> Void){
        
        if appDelegate.reachability.connection == .unavailable{
            //SCLAlertView().showError(AppName, subTitle: "Please check your internet connection.")
            return
        }
        
        //  HUD.show(.progress)
        
        let user_dict = UserDefaults.standard.object(forKey: "User_Info") as! [String:AnyObject]
        
        let parameter  = ["type":"driver_job_loaded","job_id":jobID!, "office_name":user_dict["office_name"]!, "driver_id": user_dict["driver_id"]!,"device_type":"ios"] as [String : Any]
        //print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            //  HUD.hide()
            
            switch response.result {
            case .success:
                //debugPrint(response.result.value!)
                let dict = response.result.value as! [String:AnyObject]
                let arr = dict["DATA"] as! [[String:AnyObject]]
                if dict["RESULT"] as! String ==  "OK"{
                    
                    DispatchQueue.main.async(execute: {
                        var details = [String:AnyObject]()
                        
                        if !arr.isEmpty{
                            details = arr[0]
                        }
                        
                        completion(true, details)
                    })
                }
                else{
                    DispatchQueue.main.async(execute: {
                        appDelegate.showerror(str: arr[0]["msg"] as! String)
                        
                        completion(true, nil)
                    })
                }
                
            case .failure(let error):
                //print(error)
                DispatchQueue.main.async(execute: {
                    appDelegate.showerror(str: error.localizedDescription)
                    completion(true, nil)
                })
            }
        })
    }
    
    //----------------------------------------
    
    func callWSToLocationServicesTurnedOff() {
        
        if appDelegate.reachability.connection == .unavailable{
            return
        }
        
        guard let user_dict = UserDefaults.standard.object(forKey: "User_Info") as? [String:AnyObject] else {
            return
        }
        
        let parameter  = ["type":"Locationservicewarning","driver_id": user_dict["driver_id"]!] as [String : Any]
        
        print(parameter)
        
        APIManager.sharedInstance.sessionManager.request(BASE_URL,  parameters: parameter).responseJSON(completionHandler: { (response) in
            
            switch response.result {
                
            case .success:
                
                let dict = response.result.value as! [String:AnyObject]
                if dict["RESULT"] as! String ==  "OK"{
                    print(dict)
                }
                
            case .failure(let error):
                print(error)
            }
        })
    }
    
    //MARK:- Change RootView
    
//    func changeRootViewController(with identifier:String) {
//        let storyboard = self.window?.rootViewController?.storyboard
//        let desiredViewController = storyboard?.instantiateViewController(withIdentifier: identifier)
//
//        let snapshot:UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
//        desiredViewController?.view.addSubview(snapshot);
//
//        self.window?.rootViewController = desiredViewController;
//
//        UIView.animate(withDuration: 0.3, animations: {() in
//            snapshot.layer.opacity = 0;
//            snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
//        }, completion: {
//            (value: Bool) in
//            snapshot.removeFromSuperview();
//        });
//    }

    //----------------------------------------
    
    func changeRootViewController(with identifier:String, storyBoard: String? = nil) {
        
        let storyboard = UIStoryboard.init(name: storyBoard ?? "Main", bundle: nil)
        
        let desiredViewController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        let snapshot:UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
        
        desiredViewController.view.addSubview(snapshot);
        
        self.window?.rootViewController = desiredViewController;
        
        UIView.animate(withDuration: 0.3, animations: {() in
            snapshot.layer.opacity = 0;
            snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
        }, completion: {
            (value: Bool) in
            snapshot.removeFromSuperview();
        });
    }
    
    //----------------------------------------
    
    //MARK:- Rechability
    @objc func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.connection != .unavailable {
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            
            if kGoogleAPIKey.isEmpty{
                self.callWSToGetGoogleKey()
            }
            
        } else {
            print("Network not reachable")
            DispatchQueue.main.async(execute: {
                appDelegate.showerror(str: "Cannot connect to internet.Some of app functions will not work without internet.")
            })
            
        }
    }
    
    
    
    //MARK:- Show ALert
    func showerror(str:String!){
        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
            // action here
        }
        SCLAlertView().showError(AppName, subTitle:str,timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 4.0, timeoutAction:timeoutAction))
    }
    
    //----------------------------------------
    
    func showeSuccess(str:String) {
        SCLAlertView().showSuccess(AppName, subTitle: str)
    }
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "TBMSDriver")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK:- Setup Current Location
    func setupLocation(){
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if CLLocationManager.authorizationStatus() == .notDetermined{
            self.locationManager.requestAlwaysAuthorization()
            //self.locationManager.requestAlwaysAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    
    //MARK:- Current Lcoation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        latest_location = locations[locations.count - 1]
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //appDelegate.showerror(str: error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
                self.callWSToLocationServicesTurnedOff()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
            self.locationManager.startUpdatingLocation()
        }else{
            self.callWSToLocationServicesTurnedOff()
        }
    }
    
    
}

