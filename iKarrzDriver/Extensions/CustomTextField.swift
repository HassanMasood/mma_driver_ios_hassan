//
//  CustomTextField.swift
//  AlfredMann
//
//  Created by Piyush on 04/04/17.
//  Copyright © 2017 TBMS. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBInspectable
    public var LeftPadding: CGFloat = 0.0 {
        didSet {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: LeftPadding, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
    }
    
    @IBInspectable
    public var RightPadding: CGFloat = 0.0 {
        didSet {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: RightPadding, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}
