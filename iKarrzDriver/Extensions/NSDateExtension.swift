import Foundation


//https://gist.github.com/icoxfog417/1ce24f4672a72b54dbb0

extension NSDate {
    var calendar: NSCalendar {
        return NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
    }
    
    func after(value: Int, calendarUnit:NSCalendar.Unit) -> NSDate{
        return calendar.date(byAdding: calendarUnit, value: value, to: self as Date, options: [NSCalendar.Options(rawValue: 0)])! as NSDate
        //return calendar.dateByAddingUnit(calendarUnit, value: value, toDate: self as Date, options: NSCalendar.Options(0))!
    }

//    func minus(date: NSDate) -> NSDateComponents{
//        return calendar.components(NSCalendar.Unit.MinuteCalendarUnit, fromDate: self, toDate: date, options: NSCalendar.Options(0))
//    }
    
    func equalsTo(date: NSDate) -> Bool {
        return self.compare(date as Date) == ComparisonResult.orderedSame
    }

    func greaterThan(date: NSDate) -> Bool {
        return self.compare(date as Date) == ComparisonResult.orderedDescending
    }

    func lessThan(date: NSDate) -> Bool {
        return self.compare(date as Date) == ComparisonResult.orderedAscending
    }

    
    class func parse(dateString: String, format: String = "yyyy-MM-dd HH:mm:ss") -> NSDate{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        return formatter.date(from: dateString)! as NSDate
    }

     func toString(format: String = "yyyy-MM-dd HH:mm:ss") -> String{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        return formatter.string(from: self as Date)
    }
    
    class func toString(date: NSDate, format: String = "yyyy-MM-dd HH:mm:ss") -> String{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        return formatter.string(from: date as Date)
    }
    
    class func toUTCString(date: NSDate, format: String = "yyyy-MM-dd HH:mm:ss") -> String{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        formatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        return formatter.string(from: date as Date)
    }
    
    
    //MARK: - From another
    //https://gist.github.com/lukewakeford/4e6cda958c252017e112
    
    class func stringFromDate(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "dd/MM/yyyy"
        let date_string = date_formatter.string(from: date as Date)
        return date_string
    }
    
    class func timeAndDateStringFromDate(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "dd/MM/yyyy  HH:mm"
        let date_string = date_formatter.string(from: date as Date)
        return date_string
    }
    
    class func readableDateStringFromDate(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "EEE d%@ MMM YYYY"
        let date_string = date_formatter.string(from: date as Date)
        return String(format: date_string, date.daySuffix())
    }
    
    class func readableDateTimeStringFromDate(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "HH:mm, EEE d%@ MMM YYYY"
        let date_string = date_formatter.string(from: date as Date)
        return String(format: date_string, date.daySuffix())
    }
    
    class func timeStringFromDate(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "HH:mm"
        let date_string = date_formatter.string(from: date as Date)
        return date_string
    }
    
    class func hoursAndMinutesFromDate(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "HH:mm"
        let date_string = date_formatter.string(from: date as Date)
        return date_string
    }
    
    class func yearMonthDayDateString(date:NSDate) -> String {
        let date_formatter = DateFormatter()
        date_formatter.locale = Locale(identifier: "en_US_POSIX")
        date_formatter.dateFormat = "yyyy-MM-dd"
        let date_string = date_formatter.string(from: date as Date)
        return date_string
    }
    
    func daySuffix() -> String {
        let calendar = NSCalendar.current
        let dayOfMonth = calendar.component(.day, from: self as Date)
        switch dayOfMonth {
        case 1: fallthrough
        case 21: fallthrough
        case 31: return "st"
        case 2: fallthrough
        case 22: return "nd"
        case 3: fallthrough
        case 23: return "rd"
        default: return "th"
        }
    }
    
    class func daysFrom(date:NSDate,endDate:NSDate) -> Int {
        
//        let com = NSCalendar.current.dateComponents([.day], from: date, to: endDate)
//        return com.day
//        
//        return NSCalendar.currentCalendar.components(.Day, fromDate: date, toDate: endDate, options: []).day
        
        let calendar: Calendar = Calendar.current
        let date1 = calendar.startOfDay(for: date as Date)
        let date2 = calendar.startOfDay(for: endDate as Date)
        return calendar.dateComponents([.day], from: date1, to: date2).day!
    }
    
    class func hoursFrom(date:NSDate,endDate:NSDate) -> Int {
        
        let calendar: Calendar = Calendar.current
        let date1 = calendar.startOfDay(for: date as Date)
        let date2 = calendar.startOfDay(for: endDate as Date)
        return calendar.dateComponents([.day], from: date1, to: date2).hour!
    }
    
    class func minutesFrom(date:NSDate,endDate:NSDate) -> Int {
        
        let calendar: Calendar = Calendar.current
        let date1 = calendar.startOfDay(for: date as Date)
        let date2 = calendar.startOfDay(for: endDate as Date)
        return calendar.dateComponents([.day], from: date1, to: date2).minute!
    }
    
}
