//
//  DateExtension.swift
//  ChauffeurCustomer
//
//  Created by Piyush Agrawal on 13/07/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import Foundation

extension Date{
    
    func stringWithFormatter(dateFormat: String) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = dateFormat
        return formatter.string(from: self)
    }
    
    
    
    func datefromString(string:String,dateFormat: String) -> Date {
        let formatter: DateFormatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = dateFormat
        return formatter.date(from: string)!
    }
    
    
    
    
    
    
}
