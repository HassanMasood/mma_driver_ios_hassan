//
//  CustomImageView.swift
//  AlfredMann
//
//  Created by Piyush on 08/04/17.
//  Copyright © 2017 TBMS. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func prepareForInterfaceBuilder() {
       // self.configure()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.configure()
    }
    
    @IBInspectable  var tintimageColor: UIColor! {
        didSet {
            //self.configure()
            self.tintColor = tintimageColor
        }
    }
    
    private func configure() {
        //self.image = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }

}
