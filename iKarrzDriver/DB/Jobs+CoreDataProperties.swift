//
//  Jobs+CoreDataProperties.swift
//  
//
//  Created by Piyush Agrawal on 08/12/17.
//
//

import Foundation
import CoreData


extension Jobs {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Jobs> {
        return NSFetchRequest<Jobs>(entityName: "Jobs")
    }

    @NSManaged public var bookingfees: String?
    @NSManaged public var calc_value: String?
    @NSManaged public var caller: String?
    @NSManaged public var car_park: String?
    @NSManaged public var child_seat: String?
    @NSManaged public var commission_f: String?
    @NSManaged public var cust_group_name: String?
    @NSManaged public var destination: String?
    @NSManaged public var dispatch: Int64
    @NSManaged public var driver_id: String?
    @NSManaged public var extras: String?
    @NSManaged public var fare: String?
    @NSManaged public var flight_no: String?
    @NSManaged public var gratuity: String?
    @NSManaged public var hand_luggage: String?
    @NSManaged public var incentive_val: String?
    @NSManaged public var job_date: String?
    @NSManaged public var job_id: String?
    @NSManaged public var job_status: String?
    @NSManaged public var job_time: String?
    @NSManaged public var luggage: String?
    @NSManaged public var meter: String?
    @NSManaged public var mobile: String?
    @NSManaged public var name: String?
    @NSManaged public var no_of_cars: String?
    @NSManaged public var note: String?
    @NSManaged public var num_of_people: String?
    @NSManaged public var otherref: String?
    @NSManaged public var payment_r: String?
    @NSManaged public var payment_type: String?
    @NSManaged public var pickup: String?
    @NSManaged public var status: String?
    @NSManaged public var stop1: String?
    @NSManaged public var stop2: String?
    @NSManaged public var stop3: String?
    @NSManaged public var telephone: String?
    @NSManaged public var urgent_job: String?
    @NSManaged public var vehicle_chair: String?
    @NSManaged public var vehicle_type: String?
    @NSManaged public var via_address: String?
    @NSManaged public var wttime: String?
    @NSManaged public var wttime_val: String?
    @NSManaged public var discount: String?

}
