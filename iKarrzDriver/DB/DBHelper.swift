//
//  DBHelper.swift
//  TBMSDriver
//
//  Created by Kishor Lodhia on 10/11/17.
//  Copyright © 2017 Piyush Agrawal. All rights reserved.
//

import UIKit

import CoreData

class DBHelper: NSObject {

    static let sharedInstance = DBHelper()
    
    func deleteAll(str_entity:String!){
        
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: str_entity)
        
        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try appDelegate.persistentContainer.viewContext.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
    func featchFromEntity(Entity : String,predicate_str:String?) -> (arr:Array<AnyObject>?,Error:NSError?){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity)
        
        if let predic = predicate_str{
            let predicate = NSPredicate(format: predic)
            fetchRequest.predicate=predicate
        }
        
        do {
            let arr = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>,nil)
            }
            else{
                return(arr as Array<AnyObject>,nil)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil,error)
        }
    }
    
    func featchFromEntityWithPredicate(Entity : String,predicate:NSPredicate?) -> (arr:Array<AnyObject>?,Error:NSError?){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity)
        
        if let predic = predicate{
            fetchRequest.predicate=predic
        }
        
        do {
            let arr = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>,nil)
            }
            else{
                return(arr as Array<AnyObject>,nil)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil,error)
        }
    }
    
    func checkDuplication(Entity:String,predicate_str:String,ID:String) -> (arr:Array<AnyObject>?,isExists:Bool,Error:NSError?) {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity)
        let predicate = NSPredicate(format: "\(predicate_str)=%@", ID)
        
        fetchRequest.predicate=predicate
        
        do {
            let arr = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>,true,nil)
            }
            else{
                return(arr as Array<AnyObject>,false,nil)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil,true,error)
        }
    }
    
    
    func saveAllJobs(arr_jobs:[[String:AnyObject]],completion:(_ isfinished:Bool)->Void)  {

        for dict in arr_jobs{
        
        let job = NSEntityDescription.insertNewObject(forEntityName: "Jobs",
                                                  into: appDelegate.persistentContainer.viewContext) as! Jobs
            
            job.job_id = dict["job_id"] as? String
            job.name = dict["name"] as? String
            job.mobile = dict["mobile"] as? String
            job.telephone = dict["telephone"] as? String
            job.job_date = dict["job_date"] as? String
            job.job_time = dict["job_time"] as? String
            job.pickup = dict["pickup"] as? String
            job.via_address = dict["via_address"] as? String
            job.destination = dict["destination"] as? String
            job.vehicle_type = dict["vehicle_type"] as? String
            job.payment_type = dict["payment_type"] as? String
            job.fare = dict["fare"] as? String
            job.note = dict["note"] as? String
            job.flight_no = dict["flight_no"] as? String
            job.caller = dict["caller"] as? String
            job.gratuity = dict["gratuity"] as? String
            job.car_park = dict["car_park"] as? String
            job.wttime = dict["wttime"] as? String
            job.wttime_val = dict["wttime_val"] as? String
            job.luggage = dict["luggage"] as? String
            job.hand_luggage = dict["hand_luggage"] as? String
            //job.otherref = dict["otherref"] as? String
            job.otherref = dict["telephone"] as? String
            job.incentive_val = dict["incentive_val"] as? String
            job.calc_value = dict["calc_value"] as? String
            job.num_of_people = dict["num_of_people"] as? String
            job.extras = dict["extras"] as? String
            job.meter = dict["meter"] as? String
            job.driver_id = dict["driver_id"] as? String
            job.status = dict["status"] as? String
            job.job_status = dict["job_status"] as? String
            job.stop1 = dict["stop1"] as? String
            job.stop2 = dict["stop2"] as? String
            job.stop3 = dict["stop3"] as? String
            job.payment_r = dict["payment_r"] as? String
            job.commission_f = dict["commission_f"] as? String
            job.child_seat = dict["child_seat"] as? String
            job.vehicle_chair = dict["vehicle_chair"] as? String
            job.no_of_cars = dict["no_of_cars"] as? String
            job.urgent_job = dict["urgent_job"] as? String
            job.cust_group_name = dict["cust_group_name"] as? String
            job.bookingfees = dict["bookingfees"] as? String
            job.discount = dict["discount"] as? String
            
            if let dispatch = dict["dispatch"] as? NSNumber {
                let dispatchstr = String(describing: dispatch)
                job.dispatch = Int64(dispatchstr)!
            }
        }

        
        appDelegate.saveContext()
        completion(true)
        
    }
    
    
    
    
    
}
